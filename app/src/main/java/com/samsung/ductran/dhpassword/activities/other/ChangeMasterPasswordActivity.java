package com.samsung.ductran.dhpassword.activities.other;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.EditText;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseAuthInvalidCredentialsException;
import com.google.firebase.auth.FirebaseAuthInvalidUserException;
import com.google.firebase.auth.FirebaseAuthRecentLoginRequiredException;
import com.google.firebase.auth.FirebaseAuthWeakPasswordException;
import com.google.firebase.auth.FirebaseUser;
import com.samsung.ductran.dhpassword.R;
import com.samsung.ductran.dhpassword.activities.login.SplashActivity;
import com.samsung.ductran.dhpassword.model.UserInfo;
import com.samsung.ductran.dhpassword.utils.Encryptor;
import com.samsung.ductran.dhpassword.utils.HashHelper;
import com.samsung.ductran.dhpassword.utils.SettingUtil;

import java.io.File;

import cn.pedant.SweetAlert.ProgressHelper;
import cn.pedant.SweetAlert.SweetAlertDialog;
import io.realm.Realm;
import io.realm.RealmConfiguration;

public class ChangeMasterPasswordActivity extends AppCompatActivity {

    EditText currentPass, newPass, repeatNewPass;
    String username;
    String oldPassword;

    SweetAlertDialog changePasswordProgressDialog;

    Realm realm;

    FirebaseAuth mAuth;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_change_master_password);

        ActionBar actionBar = getSupportActionBar();
        actionBar.setDisplayOptions(ActionBar.DISPLAY_HOME_AS_UP | ActionBar.DISPLAY_SHOW_TITLE);
        actionBar.setTitle("Change master password");

        Intent intent = getIntent();

        username = intent.getStringExtra("username");
        oldPassword = intent.getStringExtra("password");

        RealmConfiguration configuration = new RealmConfiguration.Builder()
                .name("UserInformation.realm")
                .build();
        realm = Realm.getInstance(configuration);

        mAuth = FirebaseAuth.getInstance();

        initView();

        initChangePassProgress();
    }

    public void initView(){
        currentPass = findViewById(R.id.ed_current_password);
        newPass = findViewById(R.id.ed_new_password);
        repeatNewPass = findViewById(R.id.ed_repeat_new_password);
    }

    public void initChangePassProgress(){
        changePasswordProgressDialog = new SweetAlertDialog(this, SweetAlertDialog.PROGRESS_TYPE)
                .setTitleText("Please wait")
                .setContentText("");
        ProgressHelper helper = changePasswordProgressDialog.getProgressHelper();
        helper.setBarColor(Color.parseColor("#3F51B5"));
        changePasswordProgressDialog.setCancelable(false);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.edit_done, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return super.onSupportNavigateUp();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        switch (id){
            case R.id.option_menu_done:
                if(checkEmptyField()){
                    showEmptyError();
                }else checkCurrentPassword(currentPass.getText().toString());
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    public boolean checkEmptyField(){
        return currentPass.getText().toString().equals("")
                || newPass.getText().toString().equals("")
                || repeatNewPass.getText().toString().equals("");
    }

    public boolean checkPasswordSecure(String pass){
        boolean checkUpper = false, checkLower = false, checkNumber = false, checkLenght = false;
        if (pass.length() >= 8)checkLenght = true;
        for (char c = 'a'; c <= 'z'; c++){
            if(pass.contains(String.valueOf(c))){
                checkLower = true;
                break;
            }
        }

        for (char c = 'A'; c <= 'Z'; c++){
            if(pass.contains(String.valueOf(c))){
                checkUpper = true;
                break;
            }
        }

        for (int c = 0; c <= 9; c++){
            if(pass.contains(String.valueOf(c))){
                checkNumber = true;
                break;
            }
        }

        return checkLower && checkUpper && checkNumber && checkLenght;
    }

    public void checkCurrentPassword(String password){
        SettingUtil.setContex(this);
        boolean isOfflineMode = SettingUtil.getSetting(username + getString(R.string.use_offline_setting));
        if(isOfflineMode){
            String newP = newPass.getText().toString()
                    , repeatNewP = repeatNewPass.getText().toString();
            if(checkPasswordSecure(newP)){
                if(newP.equals(repeatNewP)){
                    SweetAlertDialog dialog = new SweetAlertDialog(this, SweetAlertDialog.WARNING_TYPE)
                            .setTitleText(getString(R.string.warning_title))
                            .setContentText(getString(R.string.change_password_offline_relauch_warning))
                            .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                                @Override
                                public void onClick(SweetAlertDialog sweetAlertDialog) {
                                    sweetAlertDialog.dismiss();
                                    changePasswordProgressDialog.show();
                                    changeRealmPassword(newP);
                                }
                            })
                            .setCancelClickListener(new SweetAlertDialog.OnSweetClickListener() {
                                @Override
                                public void onClick(SweetAlertDialog sweetAlertDialog) {
                                    sweetAlertDialog.dismiss();
                                }
                            })
                            .setConfirmText(getString(R.string.dialog_confirm_text))
                            .setCancelText(getString(R.string.dialog_cancel_text))
                            .showCancelButton(true);
                    dialog.setCancelable(true);
                    dialog.setCanceledOnTouchOutside(true);
                    dialog.show();
                }else {
                    showPassNotEqualError();
                }
            }else {
                showPasswordNotSecureError();
            }
        }else {
            if(isOnline()){
                changePasswordProgressDialog.show();
                mAuth.signInWithEmailAndPassword(username + "@dhpassword.com", password)
                        .addOnCompleteListener(new OnCompleteListener<AuthResult>() {
                            @Override
                            public void onComplete(@NonNull Task<AuthResult> task) {
                                changePasswordProgressDialog.dismiss();
                                if(task.isSuccessful()){
                                    oldPassword = password;
                                    showRelaunchWarningDialog();
                                }else {
                                    try {
                                        throw task.getException();
                                    } catch (FirebaseAuthInvalidUserException e) {
                                        e.printStackTrace();
                                    } catch (FirebaseAuthInvalidCredentialsException e) {
                                        showWrongPasswordError();
                                        e.printStackTrace();
                                    } catch (Exception e) {
                                        e.printStackTrace();
                                    }
                                }
                            }
                        });
            }else {
                showNotOnlineError();
            }
        }
    }

    public boolean isOnline() {
        ConnectivityManager cm =
                (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo netInfo = cm.getActiveNetworkInfo();
        return netInfo != null && netInfo.isConnectedOrConnecting();
    }

    public void changeMasterPassword(){
        String newP = newPass.getText().toString()
                , repeatNewP = repeatNewPass.getText().toString();
        if(checkPasswordSecure(newP)){
            if(newP.equals(repeatNewP)){
                boolean isOfflineMode = SettingUtil.getSetting(username + getString(R.string.use_offline_setting));
                FirebaseUser user = mAuth.getCurrentUser();
                user.updatePassword(newP).addOnCompleteListener(new OnCompleteListener<Void>() {
                    @Override
                    public void onComplete(@NonNull Task<Void> task) {
                        if(task.isSuccessful()){
                            changeRealmPassword(newP);
                        }else {
                            try {
                                throw task.getException();
                            } catch(FirebaseAuthWeakPasswordException e) {
                                showPasswordNotSecureError();
                                Log.e("Exception change password", e.getMessage());
                            } catch(FirebaseAuthInvalidCredentialsException e) {
                                Log.e("Exception change password", e.getMessage());
                            } catch(FirebaseAuthRecentLoginRequiredException e) {
                                Log.e("Exception change password", e.getMessage());
                            } catch(Exception e) {
                                Log.e("Exception change password", e.getMessage());
                            }
                        }
                    }
                });
            }else {
                showPassNotEqualError();
            }
        }else {
            showPasswordNotSecureError();
        }
    }

    public void showWrongPasswordError(){
        new SweetAlertDialog(this, SweetAlertDialog.ERROR_TYPE)
                .setContentText(getString(R.string.wrong_current_master_password_error))
                .setTitleText(getString(R.string.error_dialog_title))
                .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                    @Override
                    public void onClick(SweetAlertDialog sweetAlertDialog) {
                        sweetAlertDialog.dismiss();
                    }
                })
                .show();
    }

    public void showEmptyError(){
        new SweetAlertDialog(this, SweetAlertDialog.ERROR_TYPE)
                .setContentText(getString(R.string.all_fields_empty_error))
                .setTitleText(getString(R.string.error_dialog_title))
                .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                    @Override
                    public void onClick(SweetAlertDialog sweetAlertDialog) {
                        sweetAlertDialog.dismiss();
                    }
                })
                .show();
    }

    public void showPasswordNotSecureError(){
        new SweetAlertDialog(this, SweetAlertDialog.ERROR_TYPE)
                .setContentText(getString(R.string.unsecure_new_master_password_error_content))
                .setTitleText(getString(R.string.error_dialog_title))
                .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                    @Override
                    public void onClick(SweetAlertDialog sweetAlertDialog) {
                        sweetAlertDialog.dismiss();
                    }
                })
                .show();
    }

    public void showPassNotEqualError(){
        new SweetAlertDialog(this, SweetAlertDialog.ERROR_TYPE)
                .setContentText(getString(R.string.check_two_password_error_content))
                .setTitleText(getString(R.string.error_dialog_title))
                .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                    @Override
                    public void onClick(SweetAlertDialog sweetAlertDialog) {
                        sweetAlertDialog.dismiss();
                    }
                })
                .show();
    }

    public void showNotOnlineError(){
        new SweetAlertDialog(this, SweetAlertDialog.ERROR_TYPE)
                .setContentText("Cannot connect to server. Please check your connection and try again")
                .setTitleText(getString(R.string.warning_title))
                .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                    @Override
                    public void onClick(SweetAlertDialog sweetAlertDialog) {
                        sweetAlertDialog.dismiss();
                    }
                })
                .show();
    }

    public void changeRealmPassword(String password){
        RealmConfiguration oldConfig = new RealmConfiguration.Builder()
                .name(username + ".realm")
                .encryptionKey(HashHelper.hexStringToByteArray(HashHelper.get_SHA_512(oldPassword, username)))
                .build();
        Realm realm = Realm.getInstance(oldConfig);

        realm.writeEncryptedCopyTo(new File(oldConfig.getRealmDirectory(), username + "-new.realm")
                , HashHelper.hexStringToByteArray(HashHelper.get_SHA_512(password, username)));
        realm.close();

        File dir = oldConfig.getRealmDirectory();
        File deleteFile = new File(dir, username + ".realm");
        deleteFile.delete();
        File from = new File(dir,username + "-new.realm");
        File to = new File(dir,username + ".realm");
        from.renameTo(to);

//        RealmConfiguration newConfig = new RealmConfiguration.Builder()
//                .name(username + ".realm")
//                .encryptionKey(HashHelper.hexStringToByteArray(HashHelper.get_SHA_512(oldPassword, username)))
//                .build();
//        Realm.setDefaultConfiguration(newConfig);
//        Realm newRealm = Realm.getDefaultInstance();
//        newRealm.close();

        UserInfo userInfo = this.realm.where(UserInfo.class).equalTo("username", username).findFirst();
        UserInfo updatedUser = new UserInfo(userInfo.getId(), userInfo.getName(), userInfo.getGender(), userInfo.getAge(),
                                            userInfo.getBirthday(), userInfo.getMobile(), userInfo.getEmail(), userInfo.getAddress(), userInfo.getUsername());
        try {
            Encryptor encryptor = new Encryptor(username);
            updatedUser.setIv(encryptor.getIv());
            updatedUser.setEncryptedPassword(encryptor.encryptText(username, password));
            this.realm.executeTransaction(new Realm.Transaction() {
                @Override
                public void execute(Realm realm) {
                    realm.copyToRealmOrUpdate(updatedUser);
                }
            });
//            DHpassword.getInstance().clearApplicationData();
            changePasswordProgressDialog.dismiss();
            showSuccessDialog();
        } catch (Exception e){
            Log.d("Exception signup", "execute: " + e.getMessage());
        }
    }

    public void showSuccessDialog(){
        new SweetAlertDialog(this, SweetAlertDialog.SUCCESS_TYPE)
                .setTitleText(getString(R.string.success_dialog_title))
                .setContentText(getString(R.string.change_master_password_successfully))
                .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                    @Override
                    public void onClick(SweetAlertDialog sweetAlertDialog) {
                        sweetAlertDialog.dismiss();
//                        finishAffinity();
//                        exit();
                        finishAffinity();
                        System.exit(0);
                    }
                })
                .show();
    }

    public void showRelaunchWarningDialog(){
        SweetAlertDialog dialog = new SweetAlertDialog(this, SweetAlertDialog.WARNING_TYPE)
                .setTitleText(getString(R.string.warning_title))
                .setContentText(getString(R.string.change_password_relauch_warning))
                .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                    @Override
                    public void onClick(SweetAlertDialog sweetAlertDialog) {
                        sweetAlertDialog.dismiss();
                        changePasswordProgressDialog.show();
                        changeMasterPassword();
                    }
                })
                .setCancelClickListener(new SweetAlertDialog.OnSweetClickListener() {
                    @Override
                    public void onClick(SweetAlertDialog sweetAlertDialog) {
                        sweetAlertDialog.dismiss();
                    }
                })
                .setConfirmText(getString(R.string.dialog_confirm_text))
                .setCancelText(getString(R.string.dialog_cancel_text))
                .showCancelButton(true);
        dialog.setCancelable(true);
        dialog.setCanceledOnTouchOutside(true);
        dialog.show();
    }

    private void restartApp() {
        Intent intent = new Intent(getApplicationContext(), SplashActivity.class);
        int mPendingIntentId = 123456;
        PendingIntent mPendingIntent = PendingIntent.getActivity(getApplicationContext(), mPendingIntentId, intent, PendingIntent.FLAG_CANCEL_CURRENT);
        AlarmManager mgr = (AlarmManager) getApplicationContext().getSystemService(Context.ALARM_SERVICE);
        mgr.set(AlarmManager.RTC, System.currentTimeMillis() + 100, mPendingIntent);
        System.exit(0);
    }

    public void exit(){
//        int pid = android.os.Process.myPid();
//        android.os.Process.killProcess(pid);
//        Intent intent = new Intent(this, SplashActivity.class);
//        intent.putExtra("crash", true);
//        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP
//                | Intent.FLAG_ACTIVITY_CLEAR_TASK
//                | Intent.FLAG_ACTIVITY_NEW_TASK);
//        PendingIntent pendingIntent = PendingIntent.getActivity(getBaseContext(), 0, intent, PendingIntent.FLAG_ONE_SHOT);
//        AlarmManager mgr = (AlarmManager) getBaseContext().getSystemService(Context.ALARM_SERVICE);
//        mgr.set(AlarmManager.RTC, System.currentTimeMillis() + 100, pendingIntent);
        finishAndRemoveTask();
        System.exit(2);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        realm.close();
    }
}
