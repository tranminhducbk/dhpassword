package com.samsung.ductran.dhpassword.model;

import java.io.Serializable;
import java.util.Date;

import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;

public class Login extends RealmObject implements Serializable {

    @PrimaryKey
    int id;

    String url;
    String autofillHint;
    String username;
    String password;
    String title;
    Date dateCreated;

    public void setId(int id) {
        this.id = id;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public void setDateCreated(Date dateCreated) {
        this.dateCreated = dateCreated;
    }

    public int getId() {
        return id;
    }

    public String getUrl() {
        return url;
    }

    public String getUsername() {
        return username;
    }

    public String getPassword() {
        return password;
    }

    public String getTitle() {
        return title;
    }

    public Date getDateCreated() {
        return dateCreated;
    }

    public String getAutofillHint() {
        return autofillHint;
    }

    public void setAutofillHint(String autofillHint) {
        this.autofillHint = autofillHint;
    }

    public Login(int id, String url, String username, String password, String title, Date dateCreated) {
        this.id = id;
        this.url = url;
        this.username = username;
        this.password = password;
        this.title = title;
        this.dateCreated = dateCreated;
    }

    public Login(){

    }

    public Login(int id, String url, String autofillHint, String username, String password, String title, Date dateCreated) {
        this.id = id;
        this.url = url;
        this.autofillHint = autofillHint;
        this.username = username;
        this.password = password;
        this.title = title;
        this.dateCreated = dateCreated;
    }
}
