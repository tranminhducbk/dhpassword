package com.samsung.ductran.dhpassword.activities.password;

import android.content.Intent;
import android.support.design.widget.BottomSheetDialogFragment;
import android.support.v7.app.ActionBar;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;

import com.bumptech.glide.Glide;
import com.samsung.ductran.dhpassword.R;
import com.samsung.ductran.dhpassword.activities.main.BaseActivity;
import com.samsung.ductran.dhpassword.utils.URLHelper;
import com.samsung.ductran.dhpassword.model.Login;
import com.samsung.ductran.dhpassword.model.URL;
import com.samsung.ductran.dhpassword.ui.GridViewBottomSheet;

import java.util.ArrayList;
import java.util.Date;
import java.util.UUID;

import cn.pedant.SweetAlert.SweetAlertDialog;
import io.realm.Realm;


public class AddOrUpdatePasswordActivity extends BaseActivity {
    Button btn_creat_account;
    EditText ed_title,ed_username,ed_password,ed_url;
    ImageView siteLogo;

    Login login;
    ArrayList<URL> urls;

    public ImageView getSiteLogo() {
        return siteLogo;
    }

    public void setSiteLogo(ImageView siteLogo) {
        this.siteLogo = siteLogo;
    }

    public EditText getEd_title() {
        return ed_title;
    }

    public void setEd_title(EditText ed_title) {
        this.ed_title = ed_title;
    }

    public EditText getEd_username() {
        return ed_username;
    }

    public void setEd_username(EditText ed_username) {
        this.ed_username = ed_username;
    }

    public EditText getEd_password() {
        return ed_password;
    }

    public void setEd_password(EditText ed_password) {
        this.ed_password = ed_password;
    }

    public EditText getEd_url() {
        return ed_url;
    }

    public void setEd_url(EditText ed_url) {
        this.ed_url = ed_url;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_password);

        ActionBar actionBar = getSupportActionBar();
        actionBar.setDisplayOptions(ActionBar.DISPLAY_HOME_AS_UP | ActionBar.DISPLAY_SHOW_TITLE);
        actionBar.setTitle(getString(R.string.add_password_title));

        initView();

        Intent intent = getIntent();
        login = (Login) intent.getSerializableExtra("account");

        if(login!=null){
            showLogin();
        }
    }

    public void showLogin(){
        ed_title.setText(login.getTitle());
        ed_url.setText(login.getUrl());
        ed_password.setText(login.getPassword());
        ed_username.setText(login.getUsername());

        URLHelper helper = new URLHelper(this);
        urls = helper.initURL();

        boolean in = false;
        if(login.getUrl() != null){
            String dataURL = login.getUrl();
            for (URL url : urls){
                if( (!dataURL.equals(""))
                        && (dataURL.equals(url.getUrl())
                        || dataURL.contains(url.getTitle().toLowerCase())
                        || url.getUrl().contains(dataURL.toLowerCase()))){
                    Glide.with(this).load(helper.getLogo(url.getName())).asBitmap().into(siteLogo);
                    in = true;
                    break;
                }
            }
        }
        if (!in)Glide.with(this).load(helper.getLogo("browser")).into((ImageView) siteLogo);
    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return super.onSupportNavigateUp();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.edit_done, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        switch (id){
            case R.id.option_menu_done:
                addOrUpdatePassword();
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    public void initView(){
       // btn_creat_account = (Button) findViewById(R.id.btn_creat_account);
        ed_title = (EditText)findViewById(R.id.ed_login_title);
        ed_username = (EditText)findViewById(R.id.ed_login_account);
        ed_password = (EditText)findViewById(R.id.ed_login_password);
        ed_url = (EditText)findViewById(R.id.ed_login_url);
        siteLogo = findViewById(R.id.site_logo);
    }

    public void addOrUpdatePassword(){
        if(checkEmptyField()){
            int id;
            Date dateCreated;
            if(login!=null){
                id = login.getId();
                dateCreated = login.getDateCreated();
            }else {
                id = (int) UUID.randomUUID().getMostSignificantBits();
                dateCreated = new Date();
            }
            String title = ed_title.getText().toString();
            if(title.equals(""))title = getResources().getString(R.string.untitled);

            String autofillHint = title;

            String url = ed_url.getText().toString();
            if(url != null){
                String[] items = url.split("\\.");

                for (int i = 0; i < items.length; i++){
                    if(items[i].equals("com") || items[i].equals("org") || items[i].equals("org")){
                        if(i > 0){
                            autofillHint = items[i-1];
                            break;
                        }
                    }
                }
            }

            Login login = new Login(id, url, autofillHint, ed_username.getText().toString(),
                    ed_password.getText().toString(), title, dateCreated);

            Realm realm = Realm.getDefaultInstance();
            realm.executeTransaction(new Realm.Transaction() {
                @Override
                public void execute(Realm realm) {
                    realm.copyToRealmOrUpdate(login);
                }
            });
            realm.close();
            finishAfterTransition();
        }else {
            showEmptyFieldError();
        }
    }

    public boolean checkEmptyField(){
        boolean check = true;
        if((ed_username.getText().toString().equals("")
                &&ed_password.getText().toString().equals("")
                &&ed_url.getText().toString().equals(""))){
            check = false;
        }
        return check;
    }

    public void showEmptyFieldError(){
        new SweetAlertDialog(this, SweetAlertDialog.ERROR_TYPE)
                .setContentText(getString(R.string.add_login_empty_field_error))
                .setTitleText(getString(R.string.error_dialog_title))
                .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                    @Override
                    public void onClick(SweetAlertDialog sweetAlertDialog) {
                        sweetAlertDialog.dismiss();
                    }
                })
                .show();
    }

    public void choosePopularSitesClick(View view){
        BottomSheetDialogFragment bottomSheetDialogFragment = new GridViewBottomSheet();
        bottomSheetDialogFragment.show(getSupportFragmentManager(), bottomSheetDialogFragment.getTag());
    }
}
