package com.samsung.ductran.dhpassword.activities.other;

import android.support.v7.app.ActionBar;
import android.os.Bundle;
import android.widget.Adapter;
import android.widget.ListAdapter;
import android.widget.ListView;


import com.samsung.ductran.dhpassword.R;
import com.samsung.ductran.dhpassword.activities.main.BaseActivity;
import com.samsung.ductran.dhpassword.adapters.PasswordGeneratedAdapter;
import com.samsung.ductran.dhpassword.model.PasswordGenerated;

import io.realm.Realm;
import io.realm.RealmConfiguration;
import io.realm.RealmResults;

public class ViewPasswordGeneratedActivity extends BaseActivity {
    Adapter adapter;
    ListView listView;

    Realm myRealm;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_view_password_generate);

        ActionBar actionBar = getSupportActionBar();
        actionBar.setDisplayOptions(ActionBar.DISPLAY_HOME_AS_UP | ActionBar.DISPLAY_SHOW_TITLE);
        actionBar.setTitle(getString(R.string.generated_password_title));

        listView =(ListView)findViewById(R.id.listview_passwordgenerate);

        RealmConfiguration configuration = new RealmConfiguration.Builder()
                .name("realmgenerated.realm")
                .build();
        myRealm = Realm.getInstance(configuration);
        RealmResults<PasswordGenerated> results = myRealm.where(PasswordGenerated.class).sort("date").findAll();
        if(results.size() > 50){
            myRealm.executeTransaction(new Realm.Transaction() {
                @Override
                public void execute(Realm realm) {
                    results.deleteFirstFromRealm();
                }
            });
        }
        adapter = new PasswordGeneratedAdapter(ViewPasswordGeneratedActivity.this,R.layout.item_view_password_generate,results);

        listView.setAdapter((ListAdapter) adapter);

    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return super.onSupportNavigateUp();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        myRealm.close();
    }
}
