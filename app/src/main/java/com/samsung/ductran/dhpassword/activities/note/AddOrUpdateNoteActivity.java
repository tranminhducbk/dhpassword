package com.samsung.ductran.dhpassword.activities.note;

import android.content.Intent;
import android.support.v7.app.ActionBar;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.EditText;

import com.samsung.ductran.dhpassword.R;
import com.samsung.ductran.dhpassword.activities.main.BaseActivity;
import com.samsung.ductran.dhpassword.model.Note;

import java.util.Date;
import java.util.UUID;

import cn.pedant.SweetAlert.SweetAlertDialog;
import io.realm.Realm;

public class AddOrUpdateNoteActivity extends BaseActivity {

    EditText title;
    EditText content;
    Note note;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_note);

        ActionBar actionBar = getSupportActionBar();
        actionBar.setDisplayOptions(ActionBar.DISPLAY_HOME_AS_UP | ActionBar.DISPLAY_SHOW_TITLE);
        actionBar.setTitle(R.string.add_secure_note_title);

        initView();

        Intent intent = getIntent();
        note = (Note) intent.getSerializableExtra("note");
        if(note!=null){
            showNote();
        }
    }

    public void showNote(){
        title.setText(note.getTitle());
        content.setText(note.getData());
    }

    public void initView(){
        title = findViewById(R.id.ed_note_title);
        content = findViewById(R.id.ed_note_content);
    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return super.onSupportNavigateUp();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.edit_done, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        switch (id){
            case R.id.option_menu_done:
                if(content.getText().toString().equals("")){
                    showEmptyError();
                }else {
                    addOrUpdateNote();
                    finishAfterTransition();
                }
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    public void showEmptyError(){
        new SweetAlertDialog(this, SweetAlertDialog.ERROR_TYPE)
                .setContentText(getString(R.string.note_content_empty_error_content))
                .setTitleText(getString(R.string.error_dialog_title))
                .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                    @Override
                    public void onClick(SweetAlertDialog sweetAlertDialog) {
                        sweetAlertDialog.dismiss();
                    }
                })
                .show();
    }

    public void addOrUpdateNote(){
        Realm realm = Realm.getDefaultInstance();
        Date dateCreated;
        int id;
        if(note==null){
            id = (int) UUID.randomUUID().getMostSignificantBits();
            dateCreated = new Date();
        }else {
            id = note.getId();
            dateCreated = note.getDateCreated();
        }

        String title = this.title.getText().toString();
        if (title.equals(""))title = getResources().getString(R.string.untitled);
        String content = this.content.getText().toString();
        Note note = new Note(id, title, content, dateCreated);

        realm.executeTransaction(new Realm.Transaction() {
            @Override
            public void execute(Realm realm) {
                realm.copyToRealmOrUpdate(note);
            }
        });

        realm.close();
    }
}
