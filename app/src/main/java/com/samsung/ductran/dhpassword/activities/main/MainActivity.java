package com.samsung.ductran.dhpassword.activities.main;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.Typeface;
import android.icu.text.SimpleDateFormat;
import android.icu.util.Calendar;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityOptionsCompat;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.view.MenuItemCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.CardView;
import android.view.Gravity;
import android.view.View;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.animation.Animation;
import android.view.animation.TranslateAnimation;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.getbase.floatingactionbutton.FloatingActionsMenu;
import com.github.siyamed.shapeimageview.CircularImageView;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.storage.FirebaseStorage;
import com.miguelcatalan.materialsearchview.MaterialSearchView;
import com.samsung.ductran.dhpassword.R;
import com.samsung.ductran.dhpassword.activities.card.AddOrUpdateCreditCardActivity;
import com.samsung.ductran.dhpassword.activities.login.SplashActivity;
import com.samsung.ductran.dhpassword.activities.note.AddOrUpdateNoteActivity;
import com.samsung.ductran.dhpassword.activities.password.AddOrUpdatePasswordActivity;
import com.samsung.ductran.dhpassword.activities.userinfo.UserInfoActivity;
import com.samsung.ductran.dhpassword.fragments.BackupRestoreFragment;
import com.samsung.ductran.dhpassword.fragments.FeedbackFragment;
import com.samsung.ductran.dhpassword.fragments.ListViewFragment;
import com.samsung.ductran.dhpassword.fragments.PasswordGeneratorFragment;
import com.samsung.ductran.dhpassword.fragments.SettingFragment;
import com.samsung.ductran.dhpassword.model.Card;
import com.samsung.ductran.dhpassword.model.Login;
import com.samsung.ductran.dhpassword.model.Note;
import com.samsung.ductran.dhpassword.model.UserInfo;
import com.samsung.ductran.dhpassword.utils.BitmapHelper;
import com.samsung.ductran.dhpassword.utils.Decryptor;
import com.samsung.ductran.dhpassword.utils.Encryptor;
import com.samsung.ductran.dhpassword.utils.HashHelper;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.security.InvalidAlgorithmParameterException;
import java.security.InvalidKeyException;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.NoSuchProviderException;
import java.security.UnrecoverableEntryException;
import java.security.cert.CertificateException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.crypto.BadPaddingException;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;

import cn.pedant.SweetAlert.ProgressHelper;
import cn.pedant.SweetAlert.SweetAlertDialog;
import io.realm.Realm;
import io.realm.RealmConfiguration;
import io.realm.RealmResults;
import ru.noties.scrollable.ScrollableLayout;

public class MainActivity extends BaseActivity
        implements NavigationView.OnNavigationItemSelectedListener, ListViewFragment.OnFragmentInteractionListener, BackupRestoreFragment.OnListFragmentInteractionListener {

    private static final int PASSWORD_TYPE = 0;
    private static final int NOTE_TYPE = 1;
    private static final int CARD_TYPE = 2;
    private static final int All_TYPE = 3;

    private MaterialSearchView searchView;
    private Fragment listFragment;
    ScrollableLayout scrollableLayout;

    View fabAddSecureNote;
    View fabAddpassword;
    View fabAddpayment;
    FloatingActionsMenu menuMultipleActions;

    CardView fab_cancelSearch;
    FrameLayout fab_cancel_search_container;

    NavigationView navigationView;

    TextView numPassword, numNote, numCard, tvUsername;
    CircularImageView user_avatar;

    boolean isTypeSearching;
    boolean isSearching;
    boolean ishideCancel = true;
    boolean ishideAdd;
    boolean ongenerated = false;


    private String masterPassword;
    private String username;
    private byte[] avatar;

    private Realm realm;
    private Handler mHandler;

    private FirebaseDatabase database;
    private FirebaseAuth firebaseAuth;
    FirebaseStorage storage;

    SweetAlertDialog backUpProgressDialog;
    SweetAlertDialog restoreProgressDialog;
    SweetAlertDialog signingOut;
    SweetAlertDialog wrongRestorePasswordDialog;

    private Menu menu;

    public boolean isSearching() {
        return isSearching;
    }

    public boolean isTypeSearching() {
        return isTypeSearching;
    }

    public void setMasterPassword(String masterPassword) {
        this.masterPassword = masterPassword;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getMasterPassword() {
        return masterPassword;
    }

    public String getUsername() {
        return username;
    }

    public byte[] getAvatar() {
        return avatar;
    }

    public void setAvatar(byte[] avatar) {
        this.avatar = avatar;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        mHandler = new Handler();
        database = FirebaseDatabase.getInstance();
        firebaseAuth = FirebaseAuth.getInstance();
        storage = FirebaseStorage.getInstance();

        initSearchView();
        initFabAdd();
        initFabCancelSearch();
        initBackUpRestoreProgress();

        Bundle bundle = getIntent().getBundleExtra("userInfo");
        setUsername(bundle.getString("username"));
        setMasterPassword(bundle.getString("masterPassword"));
        setAvatar(bundle.getByteArray("avatar"));
        BaseActivity.setBaseUsername(bundle.getString("username"));
        BaseActivity.setHandlerUsername();

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
            this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.setDrawerListener(toggle);
        toggle.syncState();

        scrollableLayout = findViewById(R.id.scrollable_layout);

        navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);

        tvUsername = navigationView.getHeaderView(0).findViewById(R.id.tv_user_name);
        user_avatar = navigationView.getHeaderView(0).findViewById(R.id.user_image);
        tvUsername.setText(username);
        if(avatar!=null)user_avatar.setImageBitmap(BitmapHelper.getBitmapFromBytes(avatar));

        //init realm object
        RealmConfiguration configuration = new RealmConfiguration.Builder()
                .name(username+".realm")
                .encryptionKey(HashHelper.hexStringToByteArray(HashHelper.get_SHA_512(masterPassword, username)))
                .build();
        Realm.setDefaultConfiguration(configuration);
        String path = configuration.getPath();
        realm = Realm.getDefaultInstance();

        //load first fragment
        FragmentManager manager = getSupportFragmentManager();
        FragmentTransaction transaction = manager.beginTransaction();
        listFragment = new ListViewFragment();
        ((ListViewFragment)listFragment).setItemType(PASSWORD_TYPE);
        ((ListViewFragment)listFragment).setFloatingActionsMenu(menuMultipleActions);
        transaction.add(R.id.fragment_container, listFragment, "Home Fragment");
        transaction.commit();

        initDrawerNumber();
    }

    public void initDrawerNumber(){
        numPassword = (TextView) MenuItemCompat.getActionView(navigationView.getMenu().findItem(R.id.nav_password));
        numNote = (TextView) MenuItemCompat.getActionView(navigationView.getMenu().findItem(R.id.nav_secure_note));
        numCard = (TextView) MenuItemCompat.getActionView(navigationView.getMenu().findItem(R.id.nav_payment));
        loadDrawerCount();
    }

    public void loadDrawerCount(){
        numPassword.setGravity(Gravity.CENTER_VERTICAL);
        numNote.setGravity(Gravity.CENTER_VERTICAL);
        numCard.setGravity(Gravity.CENTER_VERTICAL);

        numPassword.setTypeface(null, Typeface.BOLD);
        numNote.setTypeface(null,Typeface.BOLD);
        numCard.setTypeface(null,Typeface.BOLD);

        int nPass = realm.where(Login.class).findAll().size();
        int nNote = realm.where(Note.class).findAll().size();
        int nCard = realm.where(Card.class).findAll().size();

        numPassword.setText(String.valueOf(nPass));
        numCard.setText(String.valueOf(nCard));
        numNote.setText(String .valueOf(nNote));
    }

    public void initFabAdd(){
        menuMultipleActions = (FloatingActionsMenu) findViewById(R.id.multiple_actions);

        fabAddSecureNote = findViewById(R.id.fab_add_securenote);
        fabAddSecureNote.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                addSecureNote();
            }
        });

        fabAddpassword = findViewById(R.id.fab_add_password);
        fabAddpassword.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                addPassword();
            }
        });

        fabAddpayment = findViewById(R.id.fab_add_payment);
        fabAddpayment.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                addPayment();
            }
        });

        FrameLayout frameLayout = (FrameLayout)findViewById(R.id.fragment_overlap);
        frameLayout.setVisibility(View.GONE);
        frameLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                menuMultipleActions.collapse();
            }
        });
        menuMultipleActions.setFrameLayout_overlap(frameLayout);
    }

    public void initSearchView(){
        searchView = (MaterialSearchView) findViewById(R.id.search_view);
        searchView.setVoiceSearch(false);
        searchView.setCursorDrawable(R.drawable.color_cursor_white);
//        searchView.setSuggestions(getResources().getStringArray(R.array.query_suggestions));
        searchView.setOnQueryTextListener(new MaterialSearchView.OnQueryTextListener() {

            @Override
            public boolean onQueryTextSubmit(String s) {
                ListViewFragment fragment = (ListViewFragment) getSupportFragmentManager().findFragmentByTag("Home Fragment");
                fragment.fillData();
                fragment.searchData(s);
                isSearching = true;
                showfabCancelSearch();
                return false;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                ListViewFragment fragment = (ListViewFragment) getSupportFragmentManager().findFragmentByTag("Home Fragment");
                if(!isSearching){
                    if(newText.equals("")){
                        fragment.setupData();
                    }else {
                        fragment.fillData();
                        fragment.searchData(newText);
                    }
                }
                return false;
            }
        });
        searchView.setOnSearchViewListener(new MaterialSearchView.SearchViewListener() {
            @Override
            public void onSearchViewShown() {
                //Do some magic
                menuMultipleActions.hideFAB();
                isTypeSearching = true;
                if(isSearching){
                    hidefabCancelSearch();
                    isSearching = false;
                }
            }

            @Override
            public void onSearchViewClosed() {
                //Do some magic
                if(!isSearching) {
                    menuMultipleActions.showFAB();
//                    hidefabCancelSearch();
                    setTitle(getString(R.string.app_name));
                }else {
                    setTitle(getString(R.string.window_title_search));
                    showfabCancelSearch();
                }
                isTypeSearching = false;
            }
        });
    }

    public void initFabCancelSearch(){
        fab_cancelSearch = (CardView) findViewById(R.id.fab_cancel_search);
        fab_cancel_search_container = (FrameLayout)findViewById(R.id.fab_cancel_search_container);
        fab_cancelSearch.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                isSearching = false;
                hidefabCancelSearch();
                menuMultipleActions.showFAB();
                setTitle(getString(R.string.app_name));
                ListViewFragment fragment = (ListViewFragment)getSupportFragmentManager().findFragmentByTag("Home Fragment");
                fragment.setupData();
            }
        });
    }

    public void initBackUpRestoreProgress(){
        backUpProgressDialog = new SweetAlertDialog(this, SweetAlertDialog.PROGRESS_TYPE)
                .setTitleText(getString(R.string.backing_up_text))
                .setContentText("");
        ProgressHelper helper = backUpProgressDialog.getProgressHelper();
        helper.setBarColor(Color.parseColor("#3F51B5"));
        backUpProgressDialog.setCancelable(false);

        restoreProgressDialog = new SweetAlertDialog(this, SweetAlertDialog.PROGRESS_TYPE)
                .setTitleText(getString(R.string.restore_data_text))
                .setContentText("");
        helper = restoreProgressDialog.getProgressHelper();
        helper.setBarColor(Color.parseColor("#3F51B5"));
        restoreProgressDialog.setCancelable(false);

        signingOut = new SweetAlertDialog(this, SweetAlertDialog.PROGRESS_TYPE)
                .setTitleText("Signing out")
                .setContentText("");
        helper = signingOut.getProgressHelper();
        helper.setBarColor(Color.parseColor("#3F51B5"));
        signingOut.setCancelable(false);

        wrongRestorePasswordDialog = new SweetAlertDialog(this, SweetAlertDialog.ERROR_TYPE)
                .setContentText(getString(R.string.wrong_restore_password))
                .setTitleText(getString(R.string.warning_title))
                .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                    @Override
                    public void onClick(SweetAlertDialog sweetAlertDialog) {
                        sweetAlertDialog.dismiss();
                    }
                });
    }

    public void addPassword(){
        Intent intent = new Intent(MainActivity.this, AddOrUpdatePasswordActivity.class);
        startActivity(intent);
        Runnable mPendingRunnable = new Runnable() {
            @Override
            public void run() {
                menuMultipleActions.collapse();
            }
        };
        mHandler.postDelayed(mPendingRunnable, 200);
    }

    public void addPayment(){
        Intent intent = new Intent(MainActivity.this, AddOrUpdateCreditCardActivity.class);
        startActivity(intent);
        Runnable mPendingRunnable = new Runnable() {
            @Override
            public void run() {
                menuMultipleActions.collapse();
            }
        };
        mHandler.postDelayed(mPendingRunnable, 200);
    }

    public void addSecureNote(){
        Intent intent = new Intent(MainActivity.this, AddOrUpdateNoteActivity.class);
        startActivity(intent);
        Runnable mPendingRunnable = new Runnable() {
            @Override
            public void run() {
                menuMultipleActions.collapse();
            }
        };
        mHandler.postDelayed(mPendingRunnable, 200);
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if(searchView.isSearchOpen()){
            searchView.closeSearch();
            ListViewFragment fragment = (ListViewFragment)getSupportFragmentManager().findFragmentByTag("Home Fragment");
            fragment.setupData();
        }
        else if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            new SweetAlertDialog(this, SweetAlertDialog.NORMAL_TYPE)
                    .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                        @Override
                        public void onClick(SweetAlertDialog sweetAlertDialog) {
                            sweetAlertDialog.dismiss();
                            MainActivity.this.finishAffinity();
                            realm.close();
                            System.exit(0);
                        }
                    })
                    .setCancelClickListener(new SweetAlertDialog.OnSweetClickListener() {
                        @Override
                        public void onClick(SweetAlertDialog sweetAlertDialog) {
                            sweetAlertDialog.dismiss();
                        }
                    })
                    .setTitleText(getString(R.string.exit_warning_content_text))
                    .setContentText("")
                    .setConfirmText(getString(R.string.dialog_confirm_text))
                    .setCancelText(getString(R.string.dialog_cancel_text))
                    .show();
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        loadDrawerCount();
        setUserAvatar();
    }

    public void setUserAvatar(){
        RealmConfiguration configuration = new RealmConfiguration.Builder()
                .name("UserInformation.realm")
                .build();
        Realm realm = Realm.getInstance(configuration);
        UserInfo userInfo = realm.where(UserInfo.class).equalTo("username", username).findFirst();
        avatar = userInfo.getAvatar();
        if(avatar!= null)user_avatar.setImageBitmap(BitmapHelper.getBitmapFromBytes(avatar));
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        MenuItem item = menu.findItem(R.id.menu_search);
        searchView.setMenuItem(item);
        this.menu = menu;
        return true;
    }

    public void hideOptionMenu(){
        MenuItem item = menu.findItem(R.id.menu_search);
        item.setVisible(false);
    }

    public void showOptionMenu(){
        MenuItem item = menu.findItem(R.id.menu_search);
        item.setVisible(true);
    }

    public void backupCloudStorage(){
        backUpProgressDialog.show();
        if(isOnline()){
            firebaseAuth.signInWithEmailAndPassword(username + "@dhpassword.com", masterPassword)
                    .addOnCompleteListener(this, new OnCompleteListener<AuthResult>() {
                        @Override
                        public void onComplete(@NonNull Task<AuthResult> task) {
                            if(task.isSuccessful()){

                            }else {
                                backUpProgressDialog.dismiss();
                                Toast.makeText(MainActivity.this, "Backup failed: " +  task.getException().getMessage(), Toast.LENGTH_LONG).show();
                            }
                        }
                    });
        }else {
            backUpProgressDialog.dismiss();
            showNotOnlineError();
        }
    }

    public void backUpData(){
        backUpProgressDialog.show();
        if(isOnline()){
            firebaseAuth.signInWithEmailAndPassword(username + "@dhpassword.com", masterPassword)
                    .addOnCompleteListener(this, new OnCompleteListener<AuthResult>() {
                        @Override
                        public void onComplete(@NonNull Task<AuthResult> task) {
                            if(task.isSuccessful()){
                                String key = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new Date()) + " " + Build.MANUFACTURER.toUpperCase() +" "+ Build.MODEL;
                                DatabaseReference reference = database.getReference().child(HashHelper.get_SHA_512(username, "")).child(key);
                                backUpLogin(reference);
                                backUpNote(reference);
                                backUpCard(reference);
                                backUpUser(reference);
                                backUpProgressDialog.dismiss();
                                showBackUpSuccessDialog();
                            }else {
                                backUpProgressDialog.dismiss();
                                Toast.makeText(MainActivity.this, "Backup failed: " +  task.getException().getMessage(), Toast.LENGTH_LONG).show();
                            }
                        }


                    });
        }else {
            backUpProgressDialog.dismiss();
            showNotOnlineError();
        }
    }

    public void backUpLogin(DatabaseReference reference){
        try {
            Encryptor encryptor = new Encryptor(username);

            RealmResults<Login> loginResult = realm.where(Login.class).findAll();
            for (Login login : loginResult){
                DatabaseReference loginRef = reference.child("login").push();

                DatabaseReference idRef = loginRef.child("id");
                idRef.setValue(login.getId());

                DatabaseReference urlRef = loginRef.child("url");
                urlRef.setValue(login.getUrl());

                DatabaseReference autofillRef = loginRef.child("autofill_hint");
                autofillRef.setValue(login.getAutofillHint());

                DatabaseReference usernameRef = loginRef.child("username");
                String encryptedUsername = encryptor.encryptTextWithKey(masterPassword, login.getUsername());
                usernameRef.setValue(encryptedUsername);

                DatabaseReference passwordRef = loginRef.child("password");
                String encryptedPassword = encryptor.encryptTextWithKey(masterPassword, login.getPassword());
                passwordRef.setValue(encryptedPassword);

                DatabaseReference titleRef = loginRef.child("title");
                titleRef.setValue(login.getTitle());

                DatabaseReference dateCreatedRef = loginRef.child("date_created");
                dateCreatedRef.setValue(login.getDateCreated());
            }
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        } catch (KeyStoreException e) {
            e.printStackTrace();
        } catch (NoSuchProviderException e) {
            e.printStackTrace();
        } catch (NoSuchPaddingException e) {
            e.printStackTrace();
        } catch (InvalidKeyException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (InvalidAlgorithmParameterException e) {
            e.printStackTrace();
        } catch (CertificateException e) {
            e.printStackTrace();
        } catch (UnrecoverableEntryException e) {
            e.printStackTrace();
        } catch (BadPaddingException e) {
            e.printStackTrace();
        } catch (IllegalBlockSizeException e) {
            e.printStackTrace();
        }
    }

    public void backUpNote(DatabaseReference reference){
        try {
            Encryptor encryptor = new Encryptor(username);

            RealmResults<Note> noteResult = realm.where(Note.class).findAll();
            for (Note note : noteResult){
                DatabaseReference noteRef = reference.child("note").push();

                DatabaseReference idRef = noteRef.child("id");
                idRef.setValue(note.getId());

                DatabaseReference titleRef = noteRef.child("title");
                titleRef.setValue(note.getTitle());

                DatabaseReference dataRef = noteRef.child("data");
                String encryptedData = encryptor.encryptTextWithKey(masterPassword, note.getData());
                dataRef.setValue(encryptedData);

                DatabaseReference dateCreatedRef = noteRef.child("date_created");
                dateCreatedRef.setValue(note.getDateCreated());
            }
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        } catch (KeyStoreException e) {
            e.printStackTrace();
        } catch (NoSuchProviderException e) {
            e.printStackTrace();
        } catch (NoSuchPaddingException e) {
            e.printStackTrace();
        } catch (InvalidKeyException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (InvalidAlgorithmParameterException e) {
            e.printStackTrace();
        } catch (CertificateException e) {
            e.printStackTrace();
        } catch (UnrecoverableEntryException e) {
            e.printStackTrace();
        } catch (BadPaddingException e) {
            e.printStackTrace();
        } catch (IllegalBlockSizeException e) {
            e.printStackTrace();
        }
    }

    public void backUpCard(DatabaseReference reference){
        try {
            Encryptor encryptor = new Encryptor(username);

            RealmResults<Card> cardResults = realm.where(Card.class).findAll();
            for (Card card : cardResults){
                DatabaseReference cardRef = reference.child("card").push();

                DatabaseReference idRef = cardRef.child("id");
                idRef.setValue(card.getId());

                DatabaseReference titleRef = cardRef.child("title");
                titleRef.setValue(card.getTitle());

                DatabaseReference holderRef = cardRef.child("card_holder");
                holderRef.setValue(card.getCardHolder());

                DatabaseReference typeRef = cardRef.child("type");
                typeRef.setValue(card.getType());

                DatabaseReference numberRef = cardRef.child("number");
                numberRef.setValue(card.getCardNumber());

                DatabaseReference expirationRef = cardRef.child("expiration");
                expirationRef.setValue(card.getExpirationDate());

                DatabaseReference valid_fromRef = cardRef.child("valid_from");
                valid_fromRef.setValue(card.getValidFrom());

                DatabaseReference dateCreatedRef = cardRef.child("date_created");
                dateCreatedRef.setValue(card.getDateCreated());

                DatabaseReference pinRef = cardRef.child("PIN");
                String encryptedPIN = encryptor.encryptTextWithKey(masterPassword, card.getPin());
                pinRef.setValue(encryptedPIN);

                DatabaseReference cvvRef = cardRef.child("cvv");
                String encryptedCVV = encryptor.encryptTextWithKey(masterPassword, card.getCvv());
                cvvRef.setValue(encryptedCVV);
            }
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        } catch (KeyStoreException e) {
            e.printStackTrace();
        } catch (NoSuchProviderException e) {
            e.printStackTrace();
        } catch (NoSuchPaddingException e) {
            e.printStackTrace();
        } catch (InvalidKeyException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (InvalidAlgorithmParameterException e) {
            e.printStackTrace();
        } catch (CertificateException e) {
            e.printStackTrace();
        } catch (UnrecoverableEntryException e) {
            e.printStackTrace();
        } catch (BadPaddingException e) {
            e.printStackTrace();
        } catch (IllegalBlockSizeException e) {
            e.printStackTrace();
        }
    }

    public void backUpUser(DatabaseReference reference){
        RealmConfiguration configuration = new RealmConfiguration.Builder()
                .name("UserInformation.realm")
                .build();
        Realm realm = Realm.getInstance(configuration);
        UserInfo userInfo = realm.where(UserInfo.class).equalTo("username", username).findFirst();
        DatabaseReference userRef = reference.child("user");

        DatabaseReference nameRef = userRef.child("name");
        nameRef.setValue(userInfo.getName());

        DatabaseReference genderRef = userRef.child("gender");
        genderRef.setValue(userInfo.getGender());

        DatabaseReference birthdayRef = userRef.child("birthday");
        birthdayRef.setValue(userInfo.getBirthday());

        DatabaseReference mobileRef = userRef.child("mobile");
        mobileRef.setValue(userInfo.getMobile());

        DatabaseReference emailRef = userRef.child("email");
        emailRef.setValue(userInfo.getEmail());

        DatabaseReference addressRef = userRef.child("address");
        addressRef.setValue(userInfo.getAddress());

        DatabaseReference usernameRef = userRef.child("username");
        usernameRef.setValue(userInfo.getUsername());

        DatabaseReference ivRef = userRef.child("iv");
        List<Integer> list = new ArrayList<>();
        byte[] iv = userInfo.getIv();
        for (int i = 0; i < iv.length; i++)list.add((int)iv[i]);
        ivRef.setValue(list);

        DatabaseReference encryptedPassRef = userRef.child("encryptedPassword");
        encryptedPassRef.setValue(userInfo.getEncryptedPassword());

        realm.close();
    }

    public void showNotOnlineError(){
        new SweetAlertDialog(this, SweetAlertDialog.ERROR_TYPE)
                .setContentText(getString(R.string.not_online_error))
                .setTitleText(getString(R.string.warning_title))
                .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                    @Override
                    public void onClick(SweetAlertDialog sweetAlertDialog) {
                        sweetAlertDialog.dismiss();
                    }
                })
                .show();
    }

    public void showCancelError(){
        new SweetAlertDialog(this, SweetAlertDialog.ERROR_TYPE)
                .setContentText(getString(R.string.restpre_request_cancelled))
                .setTitleText(getString(R.string.warning_title))
                .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                    @Override
                    public void onClick(SweetAlertDialog sweetAlertDialog) {
                        sweetAlertDialog.dismiss();
                    }
                })
                .show();
    }

    public void showWrongRestorePasswordError(){
        if (!wrongRestorePasswordDialog.isShowing())wrongRestorePasswordDialog.show();
    }

    public void showBackUpSuccessDialog(){
        new SweetAlertDialog(this,  SweetAlertDialog.SUCCESS_TYPE).setTitleText(getString(R.string.success_dialog_title))
                .setContentText(getString(R.string.backup_success_message))
                .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                    @Override
                    public void onClick(SweetAlertDialog sweetAlertDialog) {
                        sweetAlertDialog.dismiss();
                    }
                })
                .show();
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        if (id == R.id.nav_password) {
            setTitle(R.string.drawer_item_password);
            showOptionMenu();
            menuMultipleActions.showFAB();
            changeFragment(PASSWORD_TYPE);
        } else if (id == R.id.nav_secure_note) {
            setTitle(R.string.drawer_item_secure_note);
            showOptionMenu();
            menuMultipleActions.showFAB();
            changeFragment(NOTE_TYPE);
        } else if (id == R.id.nav_generate_password) {
            setTitle(R.string.drawer_item_generate_password);
            changeFragment(PasswordGeneratorFragment.class);
            ongenerated = true;
            hideOptionMenu();
            menuMultipleActions.hideFAB();
        } else if (id == R.id.nav_payment) {
            setTitle(R.string.drawer_item_payment);
            showOptionMenu();
            menuMultipleActions.showFAB();
            changeFragment(CARD_TYPE);
        }else if(id == R.id.nav_feedback){
            setTitle(getString(R.string.title_feedback));
            changeFragment(FeedbackFragment.class);
            hideOptionMenu();
            menuMultipleActions.hideFAB();
        }else if(id == R.id.nav_setting){
            setTitle(getString(R.string.window_title_setting));
            changeFragment(SettingFragment.class);
            hideOptionMenu();
            menuMultipleActions.hideFAB();
        }else if (id == R.id.nav_backup){
            setTitle(getString(R.string.nav_backup_and_restore));
            changeFragment(BackupRestoreFragment.class);
            hideOptionMenu();
            menuMultipleActions.hideFAB();
        }else if (id == R.id.nav_signout){
//            Intent intent = new Intent(MainActivity.this, LoginActivity.class);
//            startActivity(intent);
//            finish();
            firebaseAuth.signOut();
            signingOut.show();
            Intent intent = new Intent(this, SplashActivity.class);
            intent.putExtra("crash", true);
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP
                    | Intent.FLAG_ACTIVITY_CLEAR_TASK
                    | Intent.FLAG_ACTIVITY_NEW_TASK);
            PendingIntent pendingIntent = PendingIntent.getActivity(getBaseContext(), 0, intent, PendingIntent.FLAG_ONE_SHOT);
            AlarmManager mgr = (AlarmManager) getBaseContext().getSystemService(Context.ALARM_SERVICE);
            mgr.set(AlarmManager.RTC, System.currentTimeMillis() + 200, pendingIntent);
            finishAffinity();
            System.exit(0);
        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    public void changeFragment(int type){
        Runnable mPendingRunnable = new Runnable() {
            @Override
            public void run() {
                FragmentManager manager = getSupportFragmentManager();
                FragmentTransaction transaction = manager.beginTransaction();
                List<Fragment> fragments = manager.getFragments();

                for (Fragment f : fragments){
                    if (f instanceof ListViewFragment && ((ListViewFragment)f).getItemType() == type)return;
                }

                ListViewFragment fragment = new ListViewFragment();
                ((ListViewFragment)fragment).setFloatingActionsMenu(menuMultipleActions);
                Bundle bundle = new Bundle();
                bundle.putInt("itemType", type);
                fragment.setArguments(bundle);

                transaction.replace(R.id.fragment_container, fragment, "Home Fragment");
                transaction.commit();
                ongenerated = false;
            }
        };
        mHandler.postDelayed(mPendingRunnable, 300);
    }

    @Override
    public void onFragmentInteraction(Uri uri) {

    }

    public  void onavatarclick(View v ){
        Intent intent_user = new Intent(MainActivity.this, UserInfoActivity.class);
        intent_user.putExtra("username", username);
        ActivityOptionsCompat options = ActivityOptionsCompat.
                makeSceneTransitionAnimation(this, v, getString(R.string.nav_header_main_transition_name));
        startActivity(intent_user, options.toBundle());
    }

    public void hidefabCancelSearch(){
        if(!ishideCancel){

            TranslateAnimation animate = new TranslateAnimation(
                    0,                 // fromXDelta
                    0,                 // toXDelta
                    0,                 // fromYDelta
                    fab_cancel_search_container.getHeight()); // toYDelta
            animate.setDuration(250);
            animate.setFillAfter(true);
            fab_cancel_search_container.startAnimation(animate);

            animate.setAnimationListener(new Animation.AnimationListener() {
                @Override
                public void onAnimationStart(Animation animation) {

                }

                @Override
                public void onAnimationEnd(Animation animation) {
                    fab_cancel_search_container.clearAnimation();
                    fab_cancel_search_container.setVisibility(View.INVISIBLE);
                    ishideCancel = true;
                }

                @Override
                public void onAnimationRepeat(Animation animation) {

                }
            });
        }
    }

    public void showfabCancelSearch(){
        if(ishideCancel){

            fab_cancel_search_container.setVisibility(View.VISIBLE);

            TranslateAnimation animate = new TranslateAnimation(
                    0,                 // fromXDelta
                    0,                 // toXDelta
                    fab_cancel_search_container.getHeight(),  // fromYDelta
                    0);                // toYDelta
            animate.setDuration(250);
            animate.setFillAfter(true);
            fab_cancel_search_container.startAnimation(animate);

            animate.setAnimationListener(new Animation.AnimationListener() {
                @Override
                public void onAnimationStart(Animation animation) {

                }

                @Override
                public void onAnimationEnd(Animation animation) {
                    fab_cancel_search_container.clearAnimation();
                    ishideCancel = false;
                }

                @Override
                public void onAnimationRepeat(Animation animation) {

                }
            });
        }
    }

    private void changeFragment(final Class fragmentClass) {

        Runnable mPendingRunnable = new Runnable() {
            @Override
            public void run() {
                Fragment fragment = null;

                try {
                    fragment = (Fragment) fragmentClass.newInstance();
                    Bundle args = new Bundle();
                    args.putString("username", username);
                    fragment.setArguments(args);
                } catch (Exception e) {
                    e.printStackTrace();
                }

                // Insert the fragment by replacing any existing fragment
                FragmentManager fragmentManager = getSupportFragmentManager();
                fragmentManager.beginTransaction().replace(R.id.fragment_container, fragment).commit();

            }
        };
        mHandler.postDelayed(mPendingRunnable, 300);
    }

    @Override
    public void onListFragmentInteraction(String item) {
        View customView = getLayoutInflater().inflate(R.layout.layout_input_restore_password, null);
        EditText edt_pass_to_unlock = customView.findViewById(R.id.restore_edt_password);
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle("Input your master password when you backup data");
        builder.setView(customView);
        builder.setCancelable(true);
        builder.setPositiveButton(getString(R.string.dialog_ok), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                String passwordToUnlock = edt_pass_to_unlock.getText().toString();
                if (passwordToUnlock == null || passwordToUnlock.equals(""))edt_pass_to_unlock.setError(getString(R.string.empty));
                else {
                    restoreData(item, passwordToUnlock);
                    dialog.dismiss();
                }
            }
        });
        builder.setNegativeButton(getString(R.string.cancel_label), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        });

        AlertDialog dialog = builder.create();
        if (!dialog.isShowing())dialog.show();
    }

    public void restoreData(String item, String passwordToUnlock){
        restoreProgressDialog.show();
        if (isOnline()){
            firebaseAuth.signInWithEmailAndPassword(username  + "@dhpassword.com", masterPassword)
                    .addOnCompleteListener(new OnCompleteListener<AuthResult>() {
                @Override
                public void onComplete(@NonNull Task<AuthResult> task) {
                    if(task.isSuccessful()){
                        loginDone = false; cardDone = false; noteDone = false; userDone = false;
                        DatabaseReference rootRef = database.getReference().child(HashHelper.get_SHA_512(username, "")).child(item);
                        restoreLoginData(rootRef, passwordToUnlock);
                        restoreNoteData(rootRef, passwordToUnlock);
                        restoreCardData(rootRef, passwordToUnlock);
                        restoreUserData(rootRef);
                    }else {
                        restoreProgressDialog.dismiss();
                        Toast.makeText(MainActivity.this, "Cannot retrieve data", Toast.LENGTH_SHORT).show();
                    }
                }
            });
        }else {
            restoreProgressDialog.dismiss();
            showNotOnlineError();
        }
    }

    boolean loginDone, cardDone, noteDone, userDone;

    public void restoreLoginData(DatabaseReference rootRef, String passwordToUnlock){
        Decryptor decryptor = new Decryptor();

        DatabaseReference loginRef = rootRef.child("login");
        loginRef.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                for (DataSnapshot itemSnapShot : dataSnapshot.getChildren()){
                    try {
                        int id = itemSnapShot.child("id").getValue(Integer.class);
                        Date dateCreated = itemSnapShot.child("date_created").getValue(Date.class);
                        String title = itemSnapShot.child("title").getValue(String.class);
                        String autofillHint = itemSnapShot.child("autofill_hint").getValue(String.class);
                        String encryptedUsername = itemSnapShot.child("username").getValue(String.class);
                        String username =  decryptor.decryptDataWithKey(passwordToUnlock, encryptedUsername);
                        String encryptedPassword = itemSnapShot.child("password").getValue(String.class);
                        String password = decryptor.decryptDataWithKey(passwordToUnlock, encryptedPassword);
                        String url = itemSnapShot.child("url").getValue(String.class);
                        Login login = new Login(id, url, autofillHint, username, password, title, dateCreated);
                        realm.executeTransaction(new Realm.Transaction() {
                            @Override
                            public void execute(Realm realm) {
                                realm.copyToRealmOrUpdate(login);
                            }
                        });
                    } catch (NoSuchPaddingException e) {
                        e.printStackTrace();
                    } catch (NoSuchAlgorithmException e) {
                        e.printStackTrace();
                    } catch (InvalidKeyException e) {
                        e.printStackTrace();
                    } catch (BadPaddingException e) {
                        restoreProgressDialog.dismiss();
                        showWrongRestorePasswordError();
                        e.printStackTrace();
                        return;
                    } catch (IllegalBlockSizeException e) {
                        restoreProgressDialog.dismiss();
                        showWrongRestorePasswordError();
                        e.printStackTrace();
                        return;
                    } catch (UnsupportedEncodingException e) {
                        e.printStackTrace();
                    }
                }
                loadDrawerCount();
                loginDone = true;
                if (loginDone && noteDone && cardDone && userDone)new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        restoreProgressDialog.dismiss();
                        showRestoreSuccessDialog();
                    }
                }, 1000);
                loginRef.removeEventListener(this);
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
                restoreProgressDialog.dismiss();
                showCancelError();
            }
        });
    }

    public void restoreNoteData(DatabaseReference rootRef, String passwordToUnlock){
        Decryptor decryptor = new Decryptor();

        DatabaseReference noteRef = rootRef.child("note");
        noteRef.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                for (DataSnapshot itemSnapShot : dataSnapshot.getChildren()){
                    try {
                        int id = itemSnapShot.child("id").getValue(Integer.class);
                        Date dateCreated = itemSnapShot.child("date_created").getValue(Date.class);
                        String title = itemSnapShot.child("title").getValue(String.class);
                        String encryptedData = itemSnapShot.child("data").getValue(String.class);
                        String data = decryptor.decryptDataWithKey(passwordToUnlock, encryptedData);
                        Note note = new Note(id, title, data, dateCreated);
                        realm.executeTransaction(new Realm.Transaction() {
                            @Override
                            public void execute(Realm realm) {
                                realm.copyToRealmOrUpdate(note);
                            }
                        });
                    } catch (NoSuchPaddingException e) {
                        e.printStackTrace();
                    } catch (NoSuchAlgorithmException e) {
                        e.printStackTrace();
                    } catch (InvalidKeyException e) {
                        e.printStackTrace();
                    } catch (BadPaddingException e) {
                        restoreProgressDialog.dismiss();
                        showWrongRestorePasswordError();
                        e.printStackTrace();
                        return;
                    } catch (IllegalBlockSizeException e) {
                        restoreProgressDialog.dismiss();
                        showWrongRestorePasswordError();
                        e.printStackTrace();
                        return;
                    } catch (UnsupportedEncodingException e) {
                        e.printStackTrace();
                    }
                }
                loadDrawerCount();
                noteDone = true;
                if (loginDone && noteDone && cardDone && userDone)new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        restoreProgressDialog.dismiss();
                        showRestoreSuccessDialog();
                    }
                }, 1000);
                noteRef.removeEventListener(this);
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
                restoreProgressDialog.dismiss();
                showCancelError();
            }
        });
    }

    public void restoreCardData(DatabaseReference rootRef, String passwordToUnlock){
        Decryptor decryptor = new Decryptor();

        DatabaseReference cardRef = rootRef.child("card");
        cardRef.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                for (DataSnapshot itemSnapShot : dataSnapshot.getChildren()){
                    try {
                        int id = itemSnapShot.child("id").getValue(Integer.class);
                        Date dateCreated = itemSnapShot.child("date_created").getValue(Date.class);
                        String title = itemSnapShot.child("title").getValue(String.class);
                        String encryptedPin = itemSnapShot.child("PIN").getValue(String.class);
                        String pin = decryptor.decryptDataWithKey(passwordToUnlock, encryptedPin);
                        String card_holder = itemSnapShot.child("card_holder").getValue(String.class);
                        String encryptedCvv = itemSnapShot.child("cvv").getValue(String.class);
                        String cvv = decryptor.decryptDataWithKey(passwordToUnlock, encryptedCvv);
                        String card_number = itemSnapShot.child("number").getValue(String.class);
                        String type = itemSnapShot.child("type").getValue(String.class);
                        Date validFrom = itemSnapShot.child("valid_from").getValue(Date.class);
                        Date expiration = itemSnapShot.child("expiration").getValue(Date.class);

                        Card card = new Card(id, title, card_holder, type, card_number, expiration, validFrom, dateCreated, pin, cvv);
                        realm.executeTransaction(new Realm.Transaction() {
                            @Override
                            public void execute(Realm realm) {
                                realm.copyToRealmOrUpdate(card);
                            }
                        });
                    } catch (NoSuchPaddingException e) {
                        e.printStackTrace();
                    } catch (NoSuchAlgorithmException e) {
                        e.printStackTrace();
                    } catch (InvalidKeyException e) {
                        e.printStackTrace();
                    } catch (BadPaddingException e) {
                        restoreProgressDialog.dismiss();
                        showWrongRestorePasswordError();
                        e.printStackTrace();
                        return;
                    } catch (IllegalBlockSizeException e) {
                        restoreProgressDialog.dismiss();
                        showWrongRestorePasswordError();
                        e.printStackTrace();
                        return;
                    } catch (UnsupportedEncodingException e) {
                        e.printStackTrace();
                    }
                }
                loadDrawerCount();
                cardDone = true;
                if (loginDone && noteDone && cardDone && userDone)new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        restoreProgressDialog.dismiss();
                        showRestoreSuccessDialog();
                    }
                }, 1000);
                cardRef.removeEventListener(this);
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
                restoreProgressDialog.dismiss();
                showCancelError();
            }
        });
    }

    public void restoreUserData(DatabaseReference rootRef){
        DatabaseReference userRef = rootRef.child("user");
        userRef.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                RealmConfiguration configuration = new RealmConfiguration.Builder()
                        .name("UserInformation.realm")
                        .build();
                Realm realm = Realm.getInstance(configuration);
                UserInfo user = realm.where(UserInfo.class).equalTo("username", username).findFirst();
                Date birthday = dataSnapshot.child("birthday").getValue(Date.class);
                String name = dataSnapshot.child("name").getValue(String.class);
                String gender = dataSnapshot.child("gender").getValue(String.class);
                String mobile = dataSnapshot.child("mobile").getValue(String.class);
                String email = dataSnapshot.child("email").getValue(String.class);
                String address = dataSnapshot.child("address").getValue(String.class);

                DataSnapshot snapshot = dataSnapshot.child("iv");
                byte[] iv = new byte[(int)snapshot.getChildrenCount()];
                int i = 0;
                for (DataSnapshot item : snapshot.getChildren()){
                    iv[Integer.parseInt(item.getKey())] = (byte) ((Long)item.getValue() & 0xff);
                }
                int age = 0;

                if(birthday!=null)age = Calendar.getInstance().get(Calendar.YEAR) - birthday.getYear() - 1900;

                UserInfo userInfo = new UserInfo(user.getId(), name, gender, age, birthday, mobile, email, address, username);
                userInfo.setIv(user.getIv());
                userInfo.setEncryptedPassword(user.getEncryptedPassword());

                realm.executeTransaction(new Realm.Transaction() {
                    @Override
                    public void execute(Realm realm) {
                        realm.copyToRealmOrUpdate(userInfo);
                    }
                });
                loadDrawerCount();
                userDone = true;
                if (loginDone && noteDone && cardDone && userDone)new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        restoreProgressDialog.dismiss();
                        showRestoreSuccessDialog();
                    }
                }, 1000);
                userRef.removeEventListener(this);
                realm.close();
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
                restoreProgressDialog.dismiss();
                showCancelError();
            }
        });
    }

    public void showRestoreSuccessDialog(){
        new SweetAlertDialog(this,  SweetAlertDialog.SUCCESS_TYPE).setTitleText(getString(R.string.success_dialog_title))
                .setContentText(getString(R.string.restore_sucess_message))
                .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                    @Override
                    public void onClick(SweetAlertDialog sweetAlertDialog) {
                        sweetAlertDialog.dismiss();
                    }
                })
                .show();
    }

    public boolean isOnline() {
        ConnectivityManager cm =
                (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo netInfo = cm.getActiveNetworkInfo();
        return netInfo != null && netInfo.isConnectedOrConnecting();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        realm.close();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        Fragment uploadType = getSupportFragmentManager().findFragmentById(R.id.fragment_container);

        if (uploadType != null) {
            uploadType.onActivityResult(requestCode, resultCode, data);
        }
        super.onActivityResult(requestCode, resultCode, data);
    }
}
