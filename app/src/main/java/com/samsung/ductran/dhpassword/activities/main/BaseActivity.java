package com.samsung.ductran.dhpassword.activities.main;

import android.content.ComponentCallbacks2;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.view.WindowManager;

import com.samsung.ductran.dhpassword.utils.ActivityLifecycleHandler;

/**
 * Created by Duc Tran on 5/1/2018.
 */

public class BaseActivity extends AppCompatActivity {

    ActivityLifecycleHandler handler;
    static String baseUsername;
    static boolean isRestore = false;

    private final int RESTORE_REQUEST_CODE = 200;

    public static void setBaseUsername(String username) {
        BaseActivity.baseUsername = username;
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
//        getWindow().setFlags(WindowManager.LayoutParams.FLAG_SECURE, WindowManager.LayoutParams.FLAG_SECURE);
        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);

//        Thread.setDefaultUncaughtExceptionHandler(new MyExceptionHandler(this));

        handler = new ActivityLifecycleHandler();
        handler.onActivityCreated(this, savedInstanceState);
    }

//    @Override
//    protected void onStart() {
//        super.onStart();
//        handler.onActivityStarted(this);
//    }
//
//    @Override
//    protected void onResume() {
//        super.onResume();
//        handler.onActivityResumed(this);
//    }
//
//    @Override
//    protected void onPause() {
//        super.onPause();
//        handler.onActivityPaused(this);
//    }
//
//    @Override
//    protected void onStop() {
//        super.onStop();
//        handler.onActivityStopped(this);
//    }
//
//    @Override
//    public void onWindowFocusChanged(boolean hasFocus) {
////        super.onWindowFocusChanged(hasFocus);
////        if (hasFocus){
////            ActivityLifecycleHandler.setMovedIntoBackground(false);
////        }else ActivityLifecycleHandler.setMovedIntoBackground(true);
//    }

    public static boolean isAppInFg = false;
    public static boolean isScrInFg = false;
    public static boolean isChangeScrFg = false;

    public static void setHandlerUsername(){
        ActivityLifecycleHandler.setUsername(baseUsername);
    }

    @Override
    protected void onStart() {
        super.onStart();
//        if (!isAppInFg) {
//            isAppInFg = true;
//            isChangeScrFg = false;
//            onAppStart();
//        } else {
//            isChangeScrFg = true;
//        }
//        isScrInFg = true;
        onAppStart();

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if(requestCode == RESTORE_REQUEST_CODE) isRestore = true;
    }

    @Override
    protected void onStop() {
        super.onStop();

//        if (!isScrInFg || !isChangeScrFg) {
//            isAppInFg = false;
//            onAppPause();
//        }
//        isScrInFg = false;
    }

    @Override
    public void onTrimMemory(int level) {
        super.onTrimMemory(level);
        if (level == ComponentCallbacks2.TRIM_MEMORY_UI_HIDDEN) {
            onAppPause();
        }
    }


    public void onAppStart() {
        //remove this toast
        if (isRestore){
            isRestore = false;
        }
        else handler.onAppStarted(this);
        // your code
    }

    public void onAppPause() {
        //remove this toast
        handler.onAppPaused(this);
        // your code
    }
}
