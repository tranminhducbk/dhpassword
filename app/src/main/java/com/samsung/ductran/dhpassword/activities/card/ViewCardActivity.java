package com.samsung.ductran.dhpassword.activities.card;

import android.content.ClipData;
import android.content.ClipboardManager;
import android.content.Context;
import android.content.Intent;
import android.content.res.Resources;
import android.graphics.drawable.ColorDrawable;
import android.icu.text.SimpleDateFormat;
import android.support.design.widget.TextInputLayout;
import android.support.v4.app.ActivityOptionsCompat;
import android.support.v7.app.ActionBar;
import android.os.Bundle;
import android.text.Spannable;
import android.text.SpannableString;
import android.text.style.ForegroundColorSpan;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.samsung.ductran.dhpassword.R;
import com.samsung.ductran.dhpassword.activities.main.BaseActivity;
import com.samsung.ductran.dhpassword.model.Card;

import io.realm.Realm;

public class ViewCardActivity extends BaseActivity {
    TextView txt_title,txt_card_name,txt_card_type,txt_card_number,txt_card_cvv,txt_card_expiry_date, txt_card_date_created,
            txt_card_valid_from,txt_card_pin;
    ImageButton img_btn_copy_pin;
    ImageView view_cardd_logo;
    TextInputLayout cvvLayout, pinLayout;
    Card card;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_view_card);
        ActionBar actionBar = getSupportActionBar();
        actionBar.setDisplayOptions(ActionBar.DISPLAY_HOME_AS_UP | ActionBar.DISPLAY_SHOW_TITLE);
        actionBar.setTitle(R.string.view_card_title);
        actionBar.setBackgroundDrawable(new ColorDrawable(getColor(R.color.total_transparent)));
        actionBar.setHomeAsUpIndicator(R.drawable.ic_back);

        Spannable text = new SpannableString(actionBar.getTitle());
        text.setSpan(new ForegroundColorSpan(getColor(R.color.colorPrimary)), 0, text.length(), Spannable.SPAN_INCLUSIVE_INCLUSIVE);
        actionBar.setTitle(text);

        initView();
        Intent intent = getIntent();
        card = (Card)intent.getSerializableExtra("card");
        showCard();
        img_btn_copy_pin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ClipboardManager clipboard = (ClipboardManager)getSystemService(Context.CLIPBOARD_SERVICE);
                ClipData clip = ClipData.newPlainText("Copied",txt_card_pin.getText().toString());
                clipboard.setPrimaryClip(clip);
                Toast.makeText(ViewCardActivity.this, "Copied", Toast.LENGTH_SHORT).show();
            }
        });

    }
    private void showCard() {
        txt_title.setText(card.getTitle());
        txt_card_name.setText(card.getCardHolder());
        txt_card_type.setText(card.getType());
        txt_card_number.setText(card.getCardNumber());
        if(card.getCvv().equals("")){
            cvvLayout.setVisibility(View.GONE);
        }else {
            cvvLayout.setVisibility(View.VISIBLE);
            txt_card_cvv.setText(card.getCvv());
        }
        if(card.getExpirationDate() != null)txt_card_expiry_date.setText(new SimpleDateFormat("yyyy-MM").format(card.getExpirationDate()));
        if(card.getValidFrom()!=null)txt_card_valid_from.setText(new SimpleDateFormat("yyyy-MM").format(card.getValidFrom()));
        if(card.getPin().equals("")){
            pinLayout.setVisibility(View.GONE);
            img_btn_copy_pin.setVisibility(View.GONE);
        }else {
            pinLayout.setVisibility(View.VISIBLE);
            img_btn_copy_pin.setVisibility(View.VISIBLE);
            txt_card_pin.setText(card.getPin());
        }

        String date = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(card.getDateCreated());
        txt_card_date_created.setText(getString (R.string.created) + ": " + date);

        String type = card.getType();
        String nameLogo = null;
        if (type.equals("Diners Club")
                ||type.equals("JCB")
                ||type.equals("Maestro")
                ||type.equals("Solo")
                ||type.equals("Switch")
                ||type.equals("Visa Electron")){
            nameLogo = "credit_card";
        }else if(type.equals("American Express")){
            nameLogo = "amex";
        }else if(type.equals("Discover")){
            nameLogo = "discover";
        }else if ((type.equals("Mastercard"))){
            nameLogo = "mastercard";
        }else if(type.equals("Visa")){
            nameLogo = "visa";
        }else if(type.equals(getResources().getString(R.string.none))){
            nameLogo = "logo_card";
        }

        setLogoCard(nameLogo);
    }
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return super.onSupportNavigateUp();
    }
    private void initView() {
        txt_title = (TextView) findViewById(R.id.txt_card_title);
        txt_card_name = (TextView) findViewById(R.id.txt_card_name);
        txt_card_type = (TextView) findViewById(R.id.txt_card_type);
        txt_card_number = (TextView) findViewById(R.id.txt_card_number);
        txt_card_cvv = (TextView) findViewById(R.id.txt_card_cvv);
        txt_card_expiry_date= (TextView) findViewById(R.id.txt_card_expiry_date);
        txt_card_valid_from = (TextView) findViewById(R.id.txt_card_valid_from);
        txt_card_pin = (TextView) findViewById(R.id.txt_card_pin);
        txt_card_date_created = findViewById(R.id.txt_card_date);
        img_btn_copy_pin = (ImageButton) findViewById(R.id.ic_copy_pin);
        cvvLayout = findViewById(R.id.cvv_layout);
        pinLayout = findViewById(R.id.pin_layout);
        view_cardd_logo = findViewById(R.id.view_card_logo);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_edit_item, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        switch (id){
            case R.id.user_info_edit:
                Intent intent = new Intent(ViewCardActivity.this, AddOrUpdateCreditCardActivity.class);
                Card newCard = new Card(card.getId(), card.getTitle(), card.getCardHolder(), card.getType()
                                        , card.getCardNumber(), card.getExpirationDate(), card.getValidFrom()
                                        , card.getDateCreated(), card.getPin(), card.getCvv());
                intent.putExtra("card", newCard);
                ActivityOptionsCompat options = ActivityOptionsCompat.
                        makeSceneTransitionAnimation(this, view_cardd_logo, "listview_item_imageView");
                startActivity(intent, options.toBundle());
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    public void setLogoCard(String name){
        Glide.with(this).load(getLogo(name)).into(view_cardd_logo);
    }

    public int getLogo(String name){
        Resources resources = getResources();
        final int resourceId = resources.getIdentifier(name, "drawable",
                getPackageName());
        return resourceId;

    }

    @Override
    protected void onResume() {
        super.onResume();
        Realm realm = Realm.getDefaultInstance();
        card = realm.where(Card.class).equalTo("id", card.getId()).findFirst();
        showCard();
        realm.close();
    }
}
