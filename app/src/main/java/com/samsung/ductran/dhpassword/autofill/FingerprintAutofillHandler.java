package com.samsung.ductran.dhpassword.autofill;

import android.Manifest;
import android.content.pm.PackageManager;
import android.hardware.fingerprint.FingerprintManager;
import android.os.CancellationSignal;
import android.support.v4.app.ActivityCompat;

import cn.pedant.SweetAlert.SweetAlertDialog;

import static android.content.Context.FINGERPRINT_SERVICE;

/**
 * Created by Duc Tran on 5/2/2018.
 */

public class FingerprintAutofillHandler extends FingerprintManager.AuthenticationCallback {

    private CancellationSignal cancellationSignal;
    FingerprintManager fingerprintManager;
    private AutofillUnlockActivity context;

    public FingerprintAutofillHandler(AutofillUnlockActivity mContext) {
        context = mContext;
        fingerprintManager = (FingerprintManager) context.getSystemService(FINGERPRINT_SERVICE);
        cancellationSignal = new CancellationSignal();
    }

    public FingerprintAutofillHandler() {
        super();
    }

    //Implement the startAuth method, which is responsible for starting the fingerprint authentication process//

    public void startAuth(FingerprintManager.CryptoObject cryptoObject) {
        if (ActivityCompat.checkSelfPermission(context, Manifest.permission.USE_FINGERPRINT) != PackageManager.PERMISSION_GRANTED) {
            return;
        }
        fingerprintManager.authenticate(cryptoObject, cancellationSignal, 0, this, null);
    }

    public void stopAuth(){
        cancellationSignal.cancel();
    }

    @Override
    public void onAuthenticationError(int errMsgId, CharSequence errString) {
//        new SweetAlertDialog(context, SweetAlertDialog.ERROR_TYPE)
//                .setContentText(String.valueOf(errString))
//                .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
//                    @Override
//                    public void onClick(SweetAlertDialog sweetAlertDialog) {
//                        sweetAlertDialog.dismiss();
//                    }
//                })
//                .show();
    }

    @Override
    public void onAuthenticationHelp(int helpMsgId, CharSequence helpString) {
        new SweetAlertDialog(context, SweetAlertDialog.WARNING_TYPE)
                .setTitleText("")
                .setContentText(String.valueOf(helpString))
                .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                    @Override
                    public void onClick(SweetAlertDialog sweetAlertDialog) {
                        sweetAlertDialog.dismiss();
                    }
                })
                .show();
    }

    @Override
    public void onAuthenticationFailed() {
//        new SweetAlertDialog(context, SweetAlertDialog.ERROR_TYPE)
//                .setContentText(String.valueOf("Failed"))
//                .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
//                    @Override
//                    public void onClick(SweetAlertDialog sweetAlertDialog) {
//                        sweetAlertDialog.dismiss();
//                    }
//                })
//                .show();
        context.showLoginFingerprintFail();
        cancellationSignal.cancel();
    }

    @Override
    public void onAuthenticationSucceeded(FingerprintManager.AuthenticationResult result) {
        cancellationSignal.cancel();
        context.showLoginFingerprintPass();
        context.setResult();
    }
}
