package com.samsung.ductran.dhpassword.adapters;

import android.app.Activity;
import android.content.res.Resources;
import android.support.annotation.LayoutRes;
import android.support.annotation.NonNull;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;


import com.bumptech.glide.Glide;
import com.samsung.ductran.dhpassword.R;
import com.samsung.ductran.dhpassword.model.URL;

import java.util.ArrayList;

/**
 * Created by sev_user on 4/18/2018.
 */

public class GridViewSiteAdapter extends ArrayAdapter<URL> {
    ArrayList<URL> list = null;
    int layout;
    Activity activity;
    public GridViewSiteAdapter(@NonNull Activity activity, @LayoutRes int layout, @NonNull ArrayList<URL> list) {
        super(activity, layout, list);
        this.activity = activity;
        this.list = list;
        this.layout = layout;
    }
    public View getView(int position, View view, ViewGroup parent){
        LayoutInflater inflater = activity.getLayoutInflater();
        view =inflater.inflate(layout,null);
        TextView txt_url =(TextView)view.findViewById(R.id.item_url);
        ImageView item_gridview_logo = (ImageView)view.findViewById(R.id.item_gridview_logo);
//        item_gridview_logo.setImageDrawable(getLogo(list.get(position).getName()));

        Glide.with(activity).load(getLogo(list.get(position).getName())).into(item_gridview_logo);
        txt_url.setText(list.get(position).getTitle());
        return view;
    }

    public int getLogo(String name){
        Resources resources = activity.getResources();
        final int resourceId = resources.getIdentifier(name, "drawable",
                activity.getPackageName());
        return resourceId;

    }
}
