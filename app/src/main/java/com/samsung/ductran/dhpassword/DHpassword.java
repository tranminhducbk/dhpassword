package com.samsung.ductran.dhpassword;

import android.app.Application;
import android.content.Context;

import java.io.File;

import io.realm.Realm;

public class DHpassword extends Application {

    public static DHpassword instance;

    @Override
    public void onCreate() {
        super.onCreate();
        instance = this;
        Realm.init(this);
    }

    @Override
    public Context getApplicationContext() {
        return super.getApplicationContext();
    }
    public static DHpassword getInstance() {
        return instance;
    }

    public void clearApplicationData() {
        File cacheDirectory = getCacheDir();
        String path = cacheDirectory.getAbsolutePath();
        File applicationDirectory = new File(cacheDirectory.getParent());
        if (applicationDirectory.exists()) {
            String[] fileNames = applicationDirectory.list();
            for (String fileName : fileNames) {
                if (!fileName.equals("lib")) {
                    deleteFile(new File(applicationDirectory, fileName));
                }
            }
        }
    }

    public static boolean deleteFile(File file) {
        boolean deletedAll = true;
        if (file != null) {
            if (file.isDirectory()) {
                String[] children = file.list();
                for (int i = 0; i < children.length; i++) {
                    deletedAll = deleteFile(new File(file, children[i])) && deletedAll;
                }
            } else {
                deletedAll = file.delete();
            }
        }

        return deletedAll;
    }

    public void clearCache(){
        File cacheDirectory = getCacheDir();
        if (cacheDirectory.exists()) {
            String[] fileNames = cacheDirectory.list();
            for (String fileName : fileNames) {
                if (!fileName.equals("lib")) {
                    deleteFile(new File(cacheDirectory, fileName));
                }
            }
        }
    }
}
