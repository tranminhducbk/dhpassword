package com.samsung.ductran.dhpassword.fragments;

import android.content.ClipData;
import android.content.ClipboardManager;
import android.content.Context;
import android.content.Intent;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.os.Bundle;
import android.transition.TransitionManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.Toast;

import com.samsung.ductran.dhpassword.R;
import com.samsung.ductran.dhpassword.activities.other.ViewPasswordGeneratedActivity;
import com.samsung.ductran.dhpassword.model.PasswordGenerated;

import org.adw.library.widgets.discreteseekbar.DiscreteSeekBar;

import java.util.Date;
import java.util.Random;

import io.realm.Realm;
import io.realm.RealmConfiguration;

public class PasswordGeneratorFragment extends Fragment {
    DiscreteSeekBar generate_seekbar;
    TextView generate_length,generate_text_password, view_password;
    Switch switch_digit,switch_letter,switch_symbel,switch_peonounceable;
    TextView generate_image;
    TextView generate_copy;
    ViewGroup transitionsContainer;
    private int type_password,password_length;
    private int type_password_digit=0,type_password_letter=0,type_password_symbol=0,type_password_peonounceable=0;
    private String stringPassword;

    Realm realmgenerate;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_password_generator, container, false);
        initView(view);

        RealmConfiguration configuration = new RealmConfiguration.Builder()
                .name("realmgenerated.realm")
                .build();
        realmgenerate = Realm.getInstance(configuration);

//        generate_seekbar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
//            @Override
//            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
//                generate_length.setText(progress+"");
//            }
//
//            @Override
//            public void onStartTrackingTouch(SeekBar seekBar) {
//
//            }
//
//            @Override
//            public void onStopTrackingTouch(SeekBar seekBar) {
//                PasswordGenerate();
//            }
//        });

        generate_seekbar.setOnProgressChangeListener(new DiscreteSeekBar.OnProgressChangeListener() {
            @Override
            public void onProgressChanged(DiscreteSeekBar seekBar, int value, boolean fromUser) {
                generate_length.setText(value+"");
            }

            @Override
            public void onStartTrackingTouch(DiscreteSeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(DiscreteSeekBar seekBar) {
                TransitionManager.beginDelayedTransition(transitionsContainer);
                PasswordGenerate();
            }
        });

        // onclick reset pass
        generate_image.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                PasswordGenerate();
            }
        });
        //OnClick Copy Generate
        generate_copy.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ClipboardManager clipboard = (ClipboardManager)getContext().getSystemService(Context.CLIPBOARD_SERVICE);
                ClipData clip = ClipData.newPlainText("Copied",generate_text_password.getText().toString());
                clipboard.setPrimaryClip(clip);
                Toast.makeText(getContext(), "Copied", Toast.LENGTH_SHORT).show();
                // add to Realm

                //final Realm realmgenerate = Realm.getInstance(PasswordGenerator.this);
                String temp_password = generate_text_password.getText().toString();

                Date today = new Date();
                PasswordGenerated passwordDb = new PasswordGenerated(temp_password,today);
                realmgenerate.beginTransaction();
                realmgenerate.copyToRealm(passwordDb);
                realmgenerate.commitTransaction();
            }
        });
        //View password generator
        view_password.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intentViewPassword = new Intent(getContext(),ViewPasswordGeneratedActivity.class);
                // Toast.makeText(PasswordGenerator.this, "sang view", Toast.LENGTH_SHORT).show();
                startActivity(intentViewPassword);
            }
        });
        return view;
    }

    public void PasswordGenerate(){
        setTypePassword();
        type_password = getTypePass();
        if (type_password==0)
            Toast.makeText(getContext(), "At least one password generation type must be selected", Toast.LENGTH_SHORT).show();
        else{
            password_length = Integer.parseInt(generate_length.getText().toString()) ;
            stringPassword = generatePassword(password_length,type_password);
            generate_text_password.setText(stringPassword);
        }
    }
    public void setTypePassword(){
        if (switch_letter.isChecked()) type_password_letter =1;
        else type_password_letter = 0;

        if (switch_digit.isChecked())type_password_digit =1;
        else type_password_digit = 0;
        if (switch_symbel.isChecked()) type_password_symbol=1;
        else type_password_symbol = 0;
        if (switch_peonounceable.isChecked()) type_password_peonounceable = 1;
        else type_password_peonounceable =0;
//        switch_digit.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
//            @Override
//            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
//                if (isChecked) type_password_digit=1;
//                else type_password_digit=0;
//            }
//        });
//
//        switch_letter.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
//            @Override
//            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
//                if (isChecked) type_password_letter=1;
//                else type_password_letter=0;
//            }
//        });
//        switch_symbel.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
//            @Override
//            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
//                if (isChecked) type_password_symbol=1;
//                else type_password_symbol=0;
//            }
//        });
//        switch_peonounceable.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
//            @Override
//            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
//                if (isChecked) type_password_peonounceable=1;
//                else type_password_peonounceable=0;
//            }
//        });
    }
    public  int getTypePass(){
        int type = 0;
        if(type_password_digit == 0 && type_password_letter == 0 && type_password_symbol == 0 && type_password_peonounceable == 0){
            type=0;
        }else if(type_password_digit == 0 && type_password_letter == 0 && type_password_symbol == 0 && type_password_peonounceable == 1){
            type=1;
        }else if(type_password_digit == 0 && type_password_letter == 0 && type_password_symbol == 1 && type_password_peonounceable == 0){
            type=2;
        }else if(type_password_digit == 0 && type_password_letter == 0 && type_password_symbol == 1 && type_password_peonounceable == 1){
            type=3;
        }else if(type_password_digit == 0 && type_password_letter == 1 && type_password_symbol == 0 && type_password_peonounceable == 0){
            type=4;
        }else if(type_password_digit == 0 && type_password_letter == 1 && type_password_symbol == 0 && type_password_peonounceable == 1){
            type=5;
        }else if(type_password_digit == 0 && type_password_letter == 1 && type_password_symbol == 1 && type_password_peonounceable == 0){
            type=6;
        }else if(type_password_digit == 0 && type_password_letter == 1 && type_password_symbol == 1 && type_password_peonounceable == 1){
            type=7;
        }else if(type_password_digit == 1 && type_password_letter == 0 && type_password_symbol == 0 && type_password_peonounceable == 0){
            type=8;
        }else if(type_password_digit == 1 && type_password_letter == 0 && type_password_symbol == 0 && type_password_peonounceable == 1){
            type=9;
        }else if(type_password_digit == 1 && type_password_letter == 0 && type_password_symbol == 1 && type_password_peonounceable == 0){
            type=10;
        }else if(type_password_digit == 1 && type_password_letter == 0 && type_password_symbol == 1 && type_password_peonounceable == 1){
            type=11;
        }else if(type_password_digit == 1 && type_password_letter == 1 && type_password_symbol == 0 && type_password_peonounceable == 0){
            type=12;
        }else if(type_password_digit == 1 && type_password_letter == 1 && type_password_symbol == 0 && type_password_peonounceable == 1){
            type=13;
        }else if(type_password_digit == 1 && type_password_letter == 1 && type_password_symbol == 1 && type_password_peonounceable == 0){
            type=14;
        }else if(type_password_digit == 1 && type_password_letter == 1 && type_password_symbol == 1 && type_password_peonounceable == 1){
            type=15;
        }
        return type;
    }
    public String generatePassword(int length, int type) {

        //String pass;
        String pass ="";
        Random r = new Random();
        switch (type){
            case 1:{
                //Toast.makeText(getBaseContext(), "gen pass type = 1", Toast.LENGTH_LONG).show();
                String choices = "./,';:`~!@#$%^&*()-=_+[]{}";
                for(int i=0;i<length;i++){
                    pass+=choices.charAt(r.nextInt(choices.length()));
                }
                break;
            }
            case 2:{
                //Toast.makeText(getBaseContext(), "gen pass type = 2", Toast.LENGTH_LONG).show();
                for(int i=0;i<length;i++){
                    pass+=r.nextInt(9)+0;
                }

                break;
            }
            case 3:{
                //Toast.makeText(getBaseContext(), "gen pass type = 3", Toast.LENGTH_LONG).show();
                char[] choices = ("./,';:`~!@#$%^&*()-=_+[]{}" + "0123456789").toCharArray();
                for(int i=0;i<length;i++){
                    pass+=choices[r.nextInt(choices.length)];
                }
                break;
            }
            case 4:{
                //Toast.makeText(getBaseContext(), "gen pass type = 4", Toast.LENGTH_LONG).show();
                String choices = "abcdefghiklmnopqswrtxyzuvj";
                for(int i=0;i<length;i++){
                    pass+=choices.charAt(r.nextInt(choices.length()));
                }
                break;
            }
            case 5:{
                //Toast.makeText(getBaseContext(), "gen pass type = 5", Toast.LENGTH_LONG).show();
                char[] choices = ("./,';:`~!@#$%^&*()-=_+[]{}" + "abcdefghiklmnopqswrtxyzuvj").toCharArray();
                for(int i=0;i<length;i++){
                    pass+=choices[r.nextInt(choices.length)];
                }
                break;
            }
            case 6:{
                char[] choices = ("abcdefghiklmnopqswrtxyzuvj" + "0123456789").toCharArray();
                for(int i=0;i<length;i++){
                    pass+=choices[r.nextInt(choices.length)];
                }
                break;
            }
            case 7:{
                char[] choices = ("./,';:`~!@#$%^&*()-=_+[]{}" +"abcdefghiklmnopqswrtxyzuvj" + "0123456789").toCharArray();
                for(int i=0;i<length;i++){
                    pass+=choices[r.nextInt(choices.length)];
                }
                break;
            }
            case 8:{
                //Toast.makeText(getBaseContext(), "gen pass type = 8", Toast.LENGTH_LONG).show();
                String choices = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
                for(int i=0;i<length;i++){
                    pass+=choices.charAt(r.nextInt(choices.length()));
                }
                break;
            }
            case 9:{
                char[] choices = ("./,';:`~!@#$%^&*()-=_+[]{}" +"ABCDEFGHIJKLMNOPQRSTUVWXYZ").toCharArray();
                for(int i=0;i<length;i++){
                    pass+=choices[r.nextInt(choices.length)];
                }
                break;
            }
            case 10:{
                char[] choices = ("0123456789" +"ABCDEFGHIJKLMNOPQRSTUVWXYZ").toCharArray();
                for(int i=0;i<length;i++){
                    pass+=choices[r.nextInt(choices.length)];
                }
                break;
            }
            case 11:{
                char[] choices = ("./,';:`~!@#$%^&*()-=_+[]{}" +"0123456789" +"ABCDEFGHIJKLMNOPQRSTUVWXYZ").toCharArray();
                for(int i=0;i<length;i++){
                    pass+=choices[r.nextInt(choices.length)];
                }
                break;
            }
            case 12:{
                char[] choices = ("abcdefghiklmnopqswrtxyzuvj" +"ABCDEFGHIJKLMNOPQRSTUVWXYZ").toCharArray();
                for(int i=0;i<length;i++){
                    pass+=choices[r.nextInt(choices.length)];
                }
                break;
            }
            case 13:{
                char[] choices = ("./,';:`~!@#$%^&*()-=_+[]{}" +"abcdefghiklmnopqswrtxyzuvj" +"ABCDEFGHIJKLMNOPQRSTUVWXYZ").toCharArray();
                for(int i=0;i<length;i++){
                    pass+=choices[r.nextInt(choices.length)];
                }
                break;
            }
            case 14:{
                char[] choices = ("0123456789" +"abcdefghiklmnopqswrtxyzuvj" +"ABCDEFGHIJKLMNOPQRSTUVWXYZ").toCharArray();
                for(int i=0;i<length;i++){
                    pass+=choices[r.nextInt(choices.length)];
                }
                break;
            }
            case 15:{
                char[] choices = ("./,';:`~!@#$%^&*()-=_+[]{}" +"0123456789" +"abcdefghiklmnopqswrtxyzuvj" +"ABCDEFGHIJKLMNOPQRSTUVWXYZ").toCharArray();
                for(int i=0;i<length;i++){
                    pass+=choices[r.nextInt(choices.length)];
                }
                break;
            }
        }
        return pass;
    }
    public void initView(View view){
        generate_seekbar = view.findViewById(R.id.generate_seekbar);
        generate_length = (TextView)view.findViewById(R.id.generate_length);
        switch_digit =(Switch)view.findViewById(R.id.generate_switch_digits);
        switch_letter =(Switch)view.findViewById(R.id.generate_switch_letter);
        switch_symbel =(Switch)view.findViewById(R.id.generate_switch_symbol);
        switch_peonounceable=(Switch)view.findViewById(R.id.generate_switch_peonounceable);
        generate_text_password =(TextView)view.findViewById(R.id.generate_text);
        generate_image = (TextView) view.findViewById(R.id.generate_image);
        generate_copy =(TextView)view.findViewById(R.id.generate_copy);
        view_password =(TextView)view.findViewById(R.id.view_password);
        transitionsContainer = (ViewGroup) view.findViewById(R.id.transitions_container);
    }
}
