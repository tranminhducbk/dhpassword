package com.samsung.ductran.dhpassword.fragments;

import android.content.Context;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.getbase.floatingactionbutton.FloatingActionsMenu;
import com.samsung.ductran.dhpassword.activities.main.MainActivity;
import com.samsung.ductran.dhpassword.R;
import com.samsung.ductran.dhpassword.adapters.ListCardAdapter;
import com.samsung.ductran.dhpassword.adapters.ListNoteAdapter;
import com.samsung.ductran.dhpassword.adapters.ListPasswordAdapter;
import com.samsung.ductran.dhpassword.model.Card;
import com.samsung.ductran.dhpassword.model.Login;
import com.samsung.ductran.dhpassword.model.Note;

import io.realm.Case;
import io.realm.OrderedRealmCollection;
import io.realm.Realm;
import io.realm.RealmRecyclerViewAdapter;
import io.realm.RealmResults;

/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link ListViewFragment.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link ListViewFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class ListViewFragment extends Fragment {

    private static final String ITEM_TYPE = "itemType";

    private int itemType;

    private OnFragmentInteractionListener mListener;

    TextView nothingToShow;
    private RecyclerView recyclerView;
    private RealmRecyclerViewAdapter adapter;

    boolean isHide;

    private OrderedRealmCollection<Login> dataLogin;
    private OrderedRealmCollection<Note> dataNote;
    private OrderedRealmCollection<Card> dataCard;

    private Realm realm;

    public void setFloatingActionsMenu(FloatingActionsMenu floatingActionsMenu) {
        this.floatingActionsMenu = floatingActionsMenu;
    }

    public void setItemType(int itemType) {
        this.itemType = itemType;
    }

    public int getItemType() {
        return itemType;
    }

    private FloatingActionsMenu floatingActionsMenu;

    private MainActivity activity;

    public ListViewFragment() {
        // Required empty public constructor

        realm = Realm.getDefaultInstance();
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param item_type Parameter 1.
     * @param param2    Parameter 2.
     * @return A new instance of fragment ListViewFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static ListViewFragment newInstance(String item_type, String param2) {
        ListViewFragment fragment = new ListViewFragment();
        Bundle args = new Bundle();
        args.putString(ITEM_TYPE, item_type);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            itemType = getArguments().getInt(ITEM_TYPE);
        }
        activity = (MainActivity)getActivity();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_list_view, container, false);

        nothingToShow = view.findViewById(R.id.nothing_to_show);
        recyclerView = (RecyclerView) view.findViewById(R.id.list_recyclerView);
        LinearLayoutManager manager = new LinearLayoutManager(getContext());

        DividerItemDecoration mDividerItemDecoration = new DividerItemDecoration(recyclerView.getContext(),
                manager.getOrientation());
        recyclerView.addItemDecoration(mDividerItemDecoration);
        recyclerView.setLayoutManager(manager);
        setupData();
//        (new SetupPasswordAsync()).execute();
        recyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {

            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                if(!(activity.isTypeSearching() || activity.isSearching())){
                    if (dy > 0) {
                        floatingActionsMenu.hideFAB();
                    } else {
                        floatingActionsMenu.showFAB();
                    }
                }
                super.onScrolled(recyclerView, dx, dy);
            }
        });

        return view;
    }

    public void fillData() {
        RealmResults results = null;
        switch (itemType){
            case 0:
                results = realm.where(Login.class).sort("title").findAll();
                dataLogin = results;
                break;
            case 1:
                results = realm.where(Note.class).sort("title").findAll();
                dataNote = results;
                break;
            case 2:
                results = realm.where(Card.class).sort("title").findAll();
                dataCard = results;
                break;
        }
    }

    public void setupData() {
        fillData();
        switch (itemType){
            case 0:
                if(dataLogin.size() == 0)nothingToShow.setVisibility(View.VISIBLE);
                else nothingToShow.setVisibility(View.GONE);
                adapter = new ListPasswordAdapter(dataLogin);
                recyclerView.setAdapter(adapter);
                adapter.notifyDataSetChanged();
                break;
            case 1:
                if(dataNote.size() == 0)nothingToShow.setVisibility(View.VISIBLE);
                else nothingToShow.setVisibility(View.GONE);
                adapter = new ListNoteAdapter(dataNote);
                recyclerView.setAdapter(adapter);
                adapter.notifyDataSetChanged();
                break;
            case 2:
                if(dataCard.size() == 0)nothingToShow.setVisibility(View.VISIBLE);
                else nothingToShow.setVisibility(View.GONE);
                adapter = new ListCardAdapter(dataCard);
                recyclerView.setAdapter(adapter);
                adapter.notifyDataSetChanged();
                break;
        }
    }

    public void clearData() {
        dataLogin.clear();
        adapter.notifyDataSetChanged();
    }

    public void searchData(String query) {
        setupData();
        OrderedRealmCollection searchData = adapter.getData().where().contains("title", query, Case.INSENSITIVE).findAll();
        switch (itemType){
            case 0:
                adapter = new ListPasswordAdapter(searchData);
                break;
            case 1:
                adapter = new ListNoteAdapter(searchData);
                break;
            case 2:
                adapter = new ListCardAdapter(searchData);
                break;
        }

        recyclerView.setAdapter(adapter);
        adapter.notifyDataSetChanged();
    }

    public void checkDataEmpty(){
        switch (itemType){
            case 0:
                if(dataLogin.size() == 0)nothingToShow.setVisibility(View.VISIBLE);
                else nothingToShow.setVisibility(View.GONE);
                break;
            case 1:
                if(dataNote.size() == 0)nothingToShow.setVisibility(View.VISIBLE);
                else nothingToShow.setVisibility(View.GONE);
                break;
            case 2:
                if(dataCard.size() == 0)nothingToShow.setVisibility(View.VISIBLE);
                else nothingToShow.setVisibility(View.GONE);
                break;
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    @Override
    public void onResume() {
        super.onResume();
        checkDataEmpty();
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    }

//    public class SetupPasswordAsync extends AsyncTask<String, Void, Void>{
//
//        @Override
//        protected void onPreExecute() {
//            progressWheel.setVisibility(View.VISIBLE);
//            progressWheel.spin();
//        }
//
//        @Override
//        protected void onPostExecute(Void aVoid) {
//            progressWheel.stopSpinning();
//            progressWheel.setVisibility(View.GONE);
//            realm = Realm.getDefaultInstance();
//            setupData();
//        }
//
//        @Override
//        protected Void doInBackground(String... strings) {
//            realm = Realm.getDefaultInstance();
//            fillData();
//            return null;
//        }
//    }
}
