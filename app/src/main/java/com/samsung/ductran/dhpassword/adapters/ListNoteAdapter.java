package com.samsung.ductran.dhpassword.adapters;

import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.icu.text.SimpleDateFormat;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityOptionsCompat;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.chauthai.swipereveallayout.SwipeRevealLayout;
import com.chauthai.swipereveallayout.ViewBinderHelper;
import com.github.siyamed.shapeimageview.CircularImageView;
import com.samsung.ductran.dhpassword.R;
import com.samsung.ductran.dhpassword.activities.main.MainActivity;
import com.samsung.ductran.dhpassword.activities.note.ViewNotesActivity;
import com.samsung.ductran.dhpassword.model.Note;
import com.samsung.ductran.dhpassword.utils.LetterTileProvider;

import java.io.ByteArrayOutputStream;

import io.realm.OrderedRealmCollection;
import io.realm.Realm;
import io.realm.RealmRecyclerViewAdapter;

/**
 * Created by Duc Tran on 4/30/2018.
 */

public class ListNoteAdapter extends RealmRecyclerViewAdapter<Note, ListNoteAdapter.ViewHolder> {
    private final ViewBinderHelper binderHelper = new ViewBinderHelper();
    private Context mContext;
    Realm realm;
    OrderedRealmCollection<Note> dataList;

    public void setDataList(OrderedRealmCollection<Note> dataList) {
        this.dataList = dataList;
    }

    //    public ListPasswordAdapter(Context context, List<String> dataSet) {
//        mContext = context;
//        mDataSet = dataSet;
//        mInflater = LayoutInflater.from(context);
//        // uncomment if you want to open only one row at a time
//         binderHelper.setOpenOnlyOne(true);
//    }

    public ListNoteAdapter(@Nullable OrderedRealmCollection<Note> data) {
        super(data, true);
//        setHasStableIds(true);
        binderHelper.setOpenOnlyOne(true);
        realm = Realm.getDefaultInstance();
        dataList = data;
    }

    @Override
    public ListNoteAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.listview_item, parent, false);
        mContext = parent.getContext();
        return new ListNoteAdapter.ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ListNoteAdapter.ViewHolder holder, int position) {
//        if (0 <= position && position < mDataSet.size()) {
        final Note item = getItem(position);
        // Use ViewBindHelper to restore and save the open/close state of the SwipeRevealView
        // put an unique string id as value, can be any string which uniquely define the data
        binderHelper.bind(holder.swipeLayout, String.valueOf(item.getId()));
        // Bind your data here
        holder.bind(item);
//        }
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public int getItemCount() {
        return dataList.size();
    }

    /**
     * Only if you need to restore open/close state when the orientation is changed. * Call this method in {@link android.app.Activity#onSaveInstanceState(Bundle)}
     */
    public void saveStates(Bundle outState) {
        binderHelper.saveStates(outState);
    }

    /**
     * Only if you need to restore open/close state when the orientation is changed. * Call this method in {@link android.app.Activity#onRestoreInstanceState(Bundle)}
     */
    public void restoreStates(Bundle inState) {
        binderHelper.restoreStates(inState);
    }

    class ViewHolder extends RecyclerView.ViewHolder {
        private SwipeRevealLayout swipeLayout;
        private View frontLayout;
        private View optionLayout;
        private TextView tvTitle;
        private TextView tvDate;
        private CircularImageView avatar;
        private ImageView avatarCard;
        private ImageButton copyButton;


        public ViewHolder(View itemView) {
            super(itemView);
            swipeLayout = (SwipeRevealLayout) itemView.findViewById(R.id.swipe_layout);
            frontLayout = itemView.findViewById(R.id.front_layout);
            optionLayout = itemView.findViewById(R.id.option_layout);
            tvTitle = (TextView) itemView.findViewById(R.id.tv_title);
            tvDate = itemView.findViewById(R.id.tv_date_created);
            avatar = itemView.findViewById(R.id.list_image_avatar);
            avatarCard = itemView.findViewById(R.id.list_image_card);
            copyButton = itemView.findViewById(R.id.list_btn_copy);

            avatarCard.setVisibility(View.GONE);
            avatar.setVisibility(View.VISIBLE);
        }

        public void bind(final Note data) {
            optionLayout.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    realm.executeTransaction(new Realm.Transaction() {
                        @Override
                        public void execute(Realm realm) {
//                            RealmResults results = realm.where(Login.class).equalTo("id", data.getId()).findAll();
//                            results.deleteAllFromRealm();
                            data.deleteFromRealm();
                            Toast.makeText(mContext, "Delete", Toast.LENGTH_LONG).show();
                            ((MainActivity)mContext).loadDrawerCount();
                        }
                    });

                }
            });

            String date = new SimpleDateFormat("yyyy-MM-dd").format(data.getDateCreated());
            tvDate.setText(mContext.getString(R.string.created) + ": " + date);

            copyButton.setVisibility(View.GONE);

            LetterTileProvider provider = new LetterTileProvider(mContext);
            int widthDimens = (int) mContext.getResources().getDimension(R.dimen.image_avatar_width);
            Bitmap bitmapAvatar;

            tvTitle.setText(data.getTitle());
            bitmapAvatar = provider.getLetterTile(data.getTitle(), data.getTitle(), widthDimens, widthDimens);

            ByteArrayOutputStream stream = new ByteArrayOutputStream();
            bitmapAvatar.compress(Bitmap.CompressFormat.PNG, 100, stream);
            Glide.with(mContext).load(stream.toByteArray()).asBitmap().into(avatar);

            frontLayout.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
//                    String displayText = "" + data.getTitle() + " clicked";
//                    Toast.makeText(mContext, displayText, Toast.LENGTH_SHORT).show();
//                    Log.d("RecyclerAdapter", displayText);
                    Intent intent = new Intent(mContext, ViewNotesActivity.class);
                    Note note = new Note(data.getId(), data.getTitle(), data.getData(), data.getDateCreated());
                    intent.putExtra("note", note);
                    ActivityOptionsCompat options = ActivityOptionsCompat.
                            makeSceneTransitionAnimation((MainActivity)mContext, avatar, "listview_item_imageView");
                    mContext.startActivity(intent, options.toBundle());
                }
            });
        }
    }
}
