package com.samsung.ductran.dhpassword.fragments;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;

import com.samsung.ductran.dhpassword.R;

import cn.pedant.SweetAlert.SweetAlertDialog;

/**
 * Created by sev_user on 5/3/2018.
 */

public class FeedbackFragment extends Fragment {
    Button btn_sendEmail;
    EditText ed_feedback_name,ed_feedback_subject,ed_feedback_content;
    View view;
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_feedback, container, false);
        initView();

        btn_sendEmail.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String subject, name ,content;
                subject = ed_feedback_subject.getText().toString();
                name = ed_feedback_name.getText().toString();
                content = ed_feedback_content.getText().toString();

                if (subject.equals("") || name.equals("") || content.equals("")){
                    showEmptyError();
                }else {
                    Intent emailIntent = new Intent(Intent.ACTION_SENDTO, Uri.fromParts(
                            "mailto","nguyenhai.idol@gmail.com", null));
                    emailIntent.putExtra(Intent.EXTRA_SUBJECT, name+" : "+subject);
                    emailIntent.putExtra(Intent.EXTRA_TEXT, content);
                    startActivity(Intent.createChooser(emailIntent, getString(R.string.send_email_feedback_using)));
                }
            }
        });
        return view;
    }

    private void initView() {
        btn_sendEmail = (Button)view.findViewById(R.id.btn_send_feedback);
        ed_feedback_name = view.findViewById(R.id.ed_feedback_name);
        ed_feedback_subject = view.findViewById(R.id.ed_feedback_subject);
        ed_feedback_content = view.findViewById(R.id.ed_feedback_content);
    }

    public void showEmptyError(){
        new SweetAlertDialog(getActivity(), SweetAlertDialog.ERROR_TYPE)
                .setContentText("Require fields cannot be empty")
                .setTitleText(getString(R.string.error_dialog_title))
                .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                    @Override
                    public void onClick(SweetAlertDialog sweetAlertDialog) {
                        sweetAlertDialog.dismiss();
                    }
                })
                .show();
    }

    public FeedbackFragment() {
    }
}
