package com.samsung.ductran.dhpassword.utils;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;

public class SettingUtil {

    private static Context context;

    public static SettingUtil newInstance() {
        SettingUtil settingUtil = new SettingUtil();
        return settingUtil;
    }

    public static void setContex(Context mContext){
        context = mContext;
    }

    public static boolean getSetting(String name){
        SharedPreferences preferences = context.getSharedPreferences(name, Context.MODE_PRIVATE);
        boolean setting = preferences.getBoolean(name, false);
        return setting;
    }

    public static void saveSetting(String name, boolean value){
        SharedPreferences.Editor editor = context.getSharedPreferences(name, Context.MODE_PRIVATE).edit();
        editor.putBoolean(name, value);
        editor.commit();
    }
}
