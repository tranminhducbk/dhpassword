package com.samsung.ductran.dhpassword.activities.login;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.drawable.Animatable2;
import android.graphics.drawable.AnimatedVectorDrawable;
import android.graphics.drawable.Drawable;
import android.hardware.fingerprint.FingerprintManager;
import android.icu.util.Calendar;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.design.widget.TextInputLayout;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseAuthInvalidCredentialsException;
import com.google.firebase.auth.FirebaseAuthInvalidUserException;
import com.google.firebase.auth.FirebaseAuthUserCollisionException;
import com.google.firebase.auth.FirebaseAuthWeakPasswordException;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.pnikosis.materialishprogress.ProgressWheel;
import com.samsung.ductran.dhpassword.R;
import com.samsung.ductran.dhpassword.activities.main.BaseActivity;
import com.samsung.ductran.dhpassword.activities.main.MainActivity;
import com.samsung.ductran.dhpassword.model.UserInfo;
import com.samsung.ductran.dhpassword.utils.ActivityLifecycleHandler;
import com.samsung.ductran.dhpassword.utils.Decryptor;
import com.samsung.ductran.dhpassword.utils.Encryptor;
import com.samsung.ductran.dhpassword.utils.FingerprintHandler;
import com.samsung.ductran.dhpassword.utils.HashHelper;
import com.samsung.ductran.dhpassword.utils.SettingUtil;

import java.io.IOException;
import java.security.InvalidAlgorithmParameterException;
import java.security.InvalidKeyException;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.UnrecoverableEntryException;
import java.security.cert.CertificateException;
import java.util.Date;
import java.util.UUID;

import javax.crypto.NoSuchPaddingException;

import cn.pedant.SweetAlert.SweetAlertDialog;
import io.realm.Realm;
import io.realm.RealmConfiguration;
import io.realm.RealmResults;
import io.realm.exceptions.RealmFileException;

public class LoginActivity extends BaseActivity implements View.OnClickListener {


    private static final int LOGIN_FRAGMENT = 0;
    private static final int SIGNUP_FRAGMENT = 1;
    private static final int RESET_PASSWORD_FRAGMENT = 2;

    EditText username;
    EditText password;
    TextInputLayout password_inputlayout;
    EditText repeatpassword;
    RelativeLayout repeatpass_layout;
    TextView tv_guide_title;
    TextView tv_question;
    TextView tv_fingerprint_notice;
    RelativeLayout signup_layout;
    LinearLayout chanmode_button;
    LinearLayout login_button;
    SweetAlertDialog mProgressBar;
    ProgressWheel progressWheel;
    ProgressWheel progressWheel_fingerprint;
    ImageView login_fingerprint_button;
    TextView login_use_password;
    RelativeLayout login_fingerprint_layout;
    RelativeLayout login_password_layout;

    int mode;

    Realm realm;

    Handler handler;

    private String mPassword;
    private byte[] avatar;

    AnimatedVectorDrawable avd;

    FirebaseAuth mAuth = FirebaseAuth.getInstance();
    FirebaseDatabase database = FirebaseDatabase.getInstance();

    public void setMode(int mode) {
        this.mode = mode;
    }

    public int getMode() {
        return mode;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
//        initRealm();

        handler = new Handler();

        RealmConfiguration configuration = new RealmConfiguration.Builder()
                .name("UserInformation.realm")
                .build();
        realm = Realm.getInstance(configuration);

        initView();

        login_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String user = username.getText().toString();
                String pass = password.getText().toString();
                String repeatpass = repeatpassword.getText().toString();
                if(mode == 0){
                    boolean ok = true;
                    if (user.length() == 0) {
                        username.setError(getString(R.string.empty));
                        ok = false;
                    }
                    if (pass.length() == 0) {
                        Toast.makeText(LoginActivity.this, R.string.password_is_empty_warning, Toast.LENGTH_LONG).show();
                        ok = false;
                    }
                    if (ok){
//                        if(!checkOfflineUser(user)){
//                            if(isOnline()){
//                                signInOnline(user, pass);
//                            }else showNotOnlineError();
//                        }else
                            signInOffline(user, pass);
                    }
                }else {
                    boolean ok = true;
                    if (user.length() == 0) {
                        username.setError(getString(R.string.empty));
                        ok = false;
                    }
                    if (pass.length() == 0) {
                        Toast.makeText(LoginActivity.this, R.string.password_is_empty_warning, Toast.LENGTH_LONG).show();
                        ok = false;
                    }
                    if (repeatpass.length() == 0) {
                        Toast.makeText(LoginActivity.this, R.string.repeat_password_empty_warning, Toast.LENGTH_LONG).show();
                        ok = false;
                    }
                    if (ok){
                        if(pass.equals(repeatpass)){
                            if(isOnline()){
                                signUPOnline(user, pass);
                            }
                            else {
                                SweetAlertDialog dialog = new SweetAlertDialog(LoginActivity.this, SweetAlertDialog.WARNING_TYPE)
                                        .setContentText("Cannot connect to server. If you want to use this app offline, you will not be able to backup or restore with our server" +
                                                "\nAre you sure?")
                                        .setTitleText(getString(R.string.warning_title))
                                        .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                                            @Override
                                            public void onClick(SweetAlertDialog sweetAlertDialog) {
                                                signUpOffline(user, pass);
                                                SettingUtil.setContex(LoginActivity.this);
                                                SettingUtil.saveSetting(user+getString(R.string.use_offline_setting), true);
                                                sweetAlertDialog.dismiss();
                                            }
                                        })
                                        .setCancelClickListener(new SweetAlertDialog.OnSweetClickListener() {
                                            @Override
                                            public void onClick(SweetAlertDialog sweetAlertDialog) {
                                                sweetAlertDialog.dismiss();
                                            }
                                        })
                                        .setConfirmText(getString(R.string.dialog_confirm_text))
                                        .setCancelText(getString(R.string.dialog_cancel_text))
                                        .showCancelButton(true);
                                dialog.show();
                            }
//                            signUpOffline(user, pass);
                        }else {
                            showPassNotEqualError();
                        }
                    }
                }
            }
        });

        chanmode_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(mode == 0){
                    mode = 1;
                    repeatpass_layout.setVisibility(View.VISIBLE);
                    tv_guide_title.setText(getString(R.string.sign_in_title));
                    tv_question.setText(getString(R.string.already_have_an_account_title));
                    ((TextView)login_button.getChildAt(0)).setText(getString(R.string.sign_up_title));
                    login_password_layout.setVisibility(View.VISIBLE);
                    login_fingerprint_layout.setVisibility(View.GONE);
                    FingerprintHandler helper = new FingerprintHandler(LoginActivity.this);
                    helper.stopAuth();
                }else{
                    mode = 0;
                    repeatpass_layout.setVisibility(View.GONE);
                    tv_guide_title.setText(getString(R.string.sign_up_title));
                    tv_question.setText(getString(R.string.dont_have_an_account_title));
                    ((TextView)login_button.getChildAt(0)).setText(getString(R.string.sign_in_title));
                    showHideFingerprint();
                }
            }
        });

    }

    public void initView(){
        username = findViewById(R.id.user_edittext);
        password = findViewById(R.id.password_edittext);
        password_inputlayout = findViewById(R.id.password_textinputlayout);
        repeatpassword = findViewById(R.id.repeat_password_edittext);
        tv_guide_title = findViewById(R.id.tv_guide_title);
        tv_question = findViewById(R.id.tv_question);
        signup_layout = findViewById(R.id.signup_layout);
        login_button = findViewById(R.id.login_button);
        chanmode_button = findViewById(R.id.change_mode_button);
        repeatpass_layout = findViewById(R.id.repeat_password_layout);
        progressWheel = findViewById(R.id.progressWheel);
        progressWheel.setSpinSpeed(3.0f);
        progressWheel_fingerprint = findViewById(R.id.progressWheel_fingerprint);
        progressWheel_fingerprint.setSpinSpeed(3.0f);
        login_fingerprint_button = findViewById(R.id.button_login_fingerprint);
        login_fingerprint_layout = findViewById(R.id.login_fingerprint_layout);
        login_password_layout = findViewById(R.id.login_password_layout);
        tv_fingerprint_notice = findViewById(R.id.txt_fingerprint_notification);
        login_use_password = findViewById(R.id.txt_fingerprint_use_password);
        login_use_password.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                FingerprintHandler helper = new FingerprintHandler(LoginActivity.this);
                helper.stopAuth();
                login_password_layout.setVisibility(View.VISIBLE);
                login_fingerprint_layout.setVisibility(View.GONE);
            }
        });
    }

    public void onButtonFingerprintClick(View view){
        startFingerprintAuthentication();
    }

    public void startFingerprintAuthentication(){
        showLoginFingerprintScanning();
        String user = getRecentUser();
        RealmResults<UserInfo> results = realm.where(UserInfo.class).equalTo("username", user).findAll();
        UserInfo userInfo = results.first();
        try {
            Decryptor decryptor = new Decryptor(userInfo.getUsername(), userInfo.getIv());
            FingerprintManager.CryptoObject cryptoObject = new FingerprintManager.CryptoObject(decryptor.getCipher());
            FingerprintHandler helper = new FingerprintHandler(this);
            helper.startAuth(cryptoObject);
        } catch (CertificateException e) {
            e.printStackTrace();
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        } catch (KeyStoreException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (NoSuchPaddingException e) {
            e.printStackTrace();
        } catch (UnrecoverableEntryException e) {
            e.printStackTrace();
        } catch (InvalidAlgorithmParameterException e) {
            e.printStackTrace();
        } catch (InvalidKeyException e) {
            e.printStackTrace();
        }
    }

    public void setupLogin(){
        mode = 0;
        repeatpass_layout.setVisibility(View.GONE);
        tv_guide_title.setText(getString(R.string.sign_up_title));
        tv_question.setText(getString(R.string.dont_have_an_account_title));
        ((TextView)login_button.getChildAt(0)).setText(getString(R.string.sign_in_title));

        String recentUser = getRecentUser();
        username.setText(recentUser);
        password.setText("");
        repeatpassword.setText("");

        showHideFingerprint();
    }

    public void setupTest(){
        username.setText("duc");
        password.setText("Aa111111");
    }

    public void signUPOnline(String user, String pass){
        showProgressBar();
        mAuth.createUserWithEmailAndPassword(user + "@dhpassword.com", pass)
                .addOnCompleteListener(this, new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        if (task.isSuccessful()){
                            signUpOffline(user, pass);
                        }else {
                            hideProgressbar();
                            try {
                                throw task.getException();
                            } catch(FirebaseAuthWeakPasswordException e) {
                                showPasswordNotSecureError();
                                password.requestFocus();
                            } catch(FirebaseAuthInvalidCredentialsException e) {
                                username.setError(getString(R.string.error_invalid_email));
                                username.requestFocus();
                            } catch(FirebaseAuthUserCollisionException e) {
                                showUsernameAlreadyExistsError();
                                username.requestFocus();
                            } catch(Exception e) {
                                Log.e("Exception", e.getMessage());
                            }
                        }
                    }
                });
    }

    public void signUpOffline(String user, String pass) {
        final int[] isSuccess = new int[1];
        Runnable signupRunnable = new Runnable() {
            @Override
            public void run() {
                realm.executeTransactionAsync(new Realm.Transaction() {
                    @Override
                    public void execute(Realm realm) {
                        RealmResults<UserInfo> results = realm.where(UserInfo.class)
                                .equalTo("username", user)
                                .findAll();
                        if(results.size()==0){
                            if(checkPasswordSecure(pass)){
                                isSuccess[0] = 0;
                                int id = (int) UUID.randomUUID().getMostSignificantBits();
                                UserInfo userInfo = new UserInfo(id, user);
                                try {
                                    Encryptor encryptor = new Encryptor(user);
                                    userInfo.setIv(encryptor.getIv());
                                    userInfo.setEncryptedPassword(encryptor.encryptText(user, pass));
                                } catch (Exception e){
                                    Log.d("Exception signup", "execute: " + e.getMessage());
                                }
                                realm.copyToRealm(userInfo);
                            }else {
                                isSuccess[0] = 1;
                            }
                        }else {
                            isSuccess[0] = 2;
                        }
                    }
                }, new Realm.Transaction.OnSuccess() {
                    @Override
                    public void onSuccess() {
                        hideProgressbar();
                        if(isSuccess[0]==0){
                            showSignupSuccessDialog();
                        }else if (isSuccess[0] == 2){
                            showUsernameAlreadyExistsError();
                        }else {
                            showPasswordNotSecureError();
                        }
                    }
                });
            }
        };
        handler.postDelayed(signupRunnable, 200);
    }

    public void signInOnline(String user, String pass){
        showProgressBar();
        mAuth.signInWithEmailAndPassword(user + "@dhpassword.com", pass)
                .addOnCompleteListener(this, new OnCompleteListener<AuthResult>() {
            @Override
            public void onComplete(@NonNull Task<AuthResult> task) {
                if(task.isSuccessful()){
                    int id = (int) UUID.randomUUID().getMostSignificantBits();
                    UserInfo userInfo = new UserInfo(id, user);
                    try {
                        Encryptor encryptor = new Encryptor(user);
                        userInfo.setIv(encryptor.getIv());
                        userInfo.setEncryptedPassword(encryptor.encryptText(user, pass));
                    } catch (Exception ex){
                        Log.d("Exception signin", "execute: " + ex.getMessage());
                        showSomeError();
                    }
                    realm.executeTransaction(new Realm.Transaction() {
                        @Override
                        public void execute(Realm realm) {
                            realm.copyToRealm(userInfo);
                        }
                    });

                    DatabaseReference reference = database.getReference().child(HashHelper.get_SHA_512(user, ""));
                    reference.addValueEventListener(new ValueEventListener() {
                        @Override
                        public void onDataChange(DataSnapshot dataSnapshot) {
                            int i = 0; long cnt = dataSnapshot.getChildrenCount();
                            if (cnt == 0){
                                saveRecentUser(user);
                                passLogin(user);
                            }
                            else for (DataSnapshot child : dataSnapshot.getChildren()){
                                if(i < cnt - 1){
                                    i++;
                                    continue;
                                }
                                DatabaseReference userRef = child.getRef();
                                userRef.addValueEventListener(new ValueEventListener() {
                                    @Override
                                    public void onDataChange(DataSnapshot dataSnapshot) {
                                        UserInfo user = realm.where(UserInfo.class).equalTo("username", userInfo.getUsername()).findFirst();
                                        DatabaseReference reference = dataSnapshot.child("user").getRef();
                                        reference.addValueEventListener(new ValueEventListener() {
                                            @Override
                                            public void onDataChange(DataSnapshot dataSnapshot) {
                                                Date birthday = dataSnapshot.child("birthday").getValue(Date.class);
                                                String name = dataSnapshot.child("name").getValue(String.class);
                                                String gender = dataSnapshot.child("gender").getValue(String.class);
                                                int age = Calendar.getInstance().get(Calendar.YEAR) - birthday.getYear() - 1900;
                                                String mobile = dataSnapshot.child("mobile").getValue(String.class);
                                                String email = dataSnapshot.child("email").getValue(String.class);
                                                String address = dataSnapshot.child("address").getValue(String.class);

                                                UserInfo userInfo = new UserInfo(user.getId(), name, gender, age, birthday, mobile, email, address, user.getUsername());
                                                userInfo.setIv(user.getIv());
                                                userInfo.setEncryptedPassword(user.getEncryptedPassword());

                                                realm.executeTransaction(new Realm.Transaction() {
                                                    @Override
                                                    public void execute(Realm realm) {
                                                        realm.copyToRealmOrUpdate(userInfo);
                                                    }
                                                });
                                                saveRecentUser(user.getUsername());
                                                passLogin(user.getUsername());
                                            }

                                            @Override
                                            public void onCancelled(DatabaseError databaseError) {

                                            }
                                        });
                                    }

                                    @Override
                                    public void onCancelled(DatabaseError databaseError) {

                                    }
                                });
                            }
                        }

                        @Override
                        public void onCancelled(DatabaseError databaseError) {

                        }
                    });

                }else {
                    try {
                        throw task.getException();
                    } catch (FirebaseAuthInvalidUserException e) {
                        showUsernameNotExistError();
                        hideProgressbar();
                        e.printStackTrace();
                    } catch (FirebaseAuthInvalidCredentialsException e) {
                        showWrongPasswordError();
                        hideProgressbar();
                        e.printStackTrace();
                    } catch (Exception e) {
                        hideProgressbar();
                        e.printStackTrace();
                    }
                }
            }
        }).addOnFailureListener(this, new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception e) {
                hideProgressbar();
                Toast.makeText(LoginActivity.this, e.getMessage(), Toast.LENGTH_LONG).show();
            }
        });
    }

    public RealmResults<UserInfo> checkUser(Realm realm, String username){
        RealmResults<UserInfo> results = realm.where(UserInfo.class)
                .equalTo("username", username)
                .findAll();
        return results;
    }

    public void signInOffline(String user, String pass) {
        showProgressBar();
        final int[] isSuccess = new int[]{-1}; //0: user not exist, 1: wrong password, 2: success
        final String[] masterPassword = new String[1];
        Runnable signinRunnable = new Runnable() {
            @Override
            public void run() {
                realm.executeTransactionAsync(new Realm.Transaction() {
                    @Override
                    public void execute(Realm realm) {
                        RealmResults<UserInfo> results = checkUser(realm, user);
                        if(results.size()==0){
                            isSuccess[0] = 0;
                        }else {
                            UserInfo userInfo = results.first();
                            try {
                                Decryptor decryptor = new Decryptor(userInfo.getUsername(), userInfo.getIv());
                                masterPassword[0] = decryptor.decryptData(userInfo.getEncryptedPassword());
                                if(!pass.equals(masterPassword[0])){
                                    isSuccess[0] = 1;
                                }else {
                                    isSuccess[0] = 2;
                                    avatar = userInfo.getAvatar();
                                }
                            } catch (Exception e){
                                //Lost the key
                                Log.d("Exception signin", "execute: " + e.getMessage() + " " + e.getClass());
                                try {
                                    RealmConfiguration configuration = new RealmConfiguration.Builder()
                                        .name(user+".realm")
                                        .encryptionKey(HashHelper.hexStringToByteArray(HashHelper.get_SHA_512(pass, user)))
                                        .build();

                                    userInfo.setUsername(user);
                                    try {
                                        Encryptor encryptor = new Encryptor(user);
                                        userInfo.setIv(encryptor.getIv());
                                        userInfo.setEncryptedPassword(encryptor.encryptText(user, pass));
                                        avatar = userInfo.getAvatar();
                                        isSuccess[0] = 0;
                                    } catch (Exception ex){
                                        Log.d("Exception signin", "execute: " + ex.getMessage());
                                    }
                                    realm.copyToRealmOrUpdate(userInfo);
                                }catch (RealmFileException re){
                                    isSuccess[0] = 1;
                                }
                            }
                        }
                    }
                }, new Realm.Transaction.OnSuccess() {
                    @Override
                    public void onSuccess() {
                        if(isSuccess[0] == 0){
                            if(isOnline()){
                                signInOnline(user, pass);
                            }else {
                                hideProgressbar();
                                showNotOnlineError();
                            }
                        }else if(isSuccess[0] == 1){
                            showWrongPasswordError();
                            hideProgressbar();
                        }else {
                            ActivityLifecycleHandler.setAppLock(false);
                            new Handler().postDelayed(new Runnable() {
                                @Override
                                public void run() {
                                    if(!ActivityLifecycleHandler.isAppInBackground() || ActivityLifecycleHandler.isIsRunFormIcon()){
                                        ActivityLifecycleHandler.setIsRunFormIcon(false);
                                        saveRecentUser(user);
                                        Intent intent = new Intent(LoginActivity.this, MainActivity.class);
                                        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
                                        Bundle bundle = new Bundle();
                                        bundle.putString("username", user);
                                        bundle.putString("masterPassword", masterPassword[0]);
                                        bundle.putByteArray("avatar", avatar);
                                        intent.putExtra("userInfo", bundle);
                                        startActivity(intent);
                                        finishAffinity();
                                    }
                                    else {
                                        ActivityLifecycleHandler.setMovedIntoBackground(false);
                                        finish();
                                    }
                                }
                            }, 100);
                        }
                    }
                });
            }
        };
        handler.postDelayed(signinRunnable, 0);
    }

    public void passLogin(String user){
        showProgressBar();
        realm.executeTransaction(new Realm.Transaction() {
            @Override
            public void execute(Realm realm) {
                RealmResults<UserInfo> results = realm.where(UserInfo.class)
                        .equalTo("username", user)
                        .findAll();
                UserInfo userInfo = results.first();
                try {
                    Decryptor decryptor = new Decryptor(userInfo.getUsername(), userInfo.getIv());
                    mPassword = decryptor.decryptData(userInfo.getEncryptedPassword());
                    avatar = userInfo.getAvatar();
                } catch (Exception e) {
//                    e.printStackTrace();
                    login_fingerprint_layout.setVisibility(View.GONE);
                    login_password_layout.setVisibility(View.VISIBLE);
                    showSomeError();
                }
            }
        });
        ActivityLifecycleHandler.setAppLock(false);
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                if(!ActivityLifecycleHandler.isAppInBackground() || ActivityLifecycleHandler.isIsRunFormIcon()){
                    Intent intent = new Intent(LoginActivity.this, MainActivity.class);
                    Bundle bundle = new Bundle();
                    bundle.putString("username", user);
                    bundle.putString("masterPassword", mPassword);
                    bundle.putByteArray("avatar", avatar);
                    intent.putExtra("userInfo", bundle);
                    startActivity(intent);
                }
                else {
                    ActivityLifecycleHandler.setMovedIntoBackground(false);
                    finish();
                }
            }
        }, 300);
    }

    public void getDataFromServer(String username){

    }

    public void showLoginFingerprintFail(){
        tv_fingerprint_notice.setText(R.string.fingerprint_no_match);
        login_fingerprint_button.setImageDrawable(getDrawable(R.drawable.fingerprint_to_cross));
        Drawable don = login_fingerprint_button.getDrawable();
        if (don instanceof AnimatedVectorDrawable){
            avd = (AnimatedVectorDrawable)don;
            avd.start();
        }
    }

    public void showLoginFingerprintScanning(){
        tv_fingerprint_notice.setText(R.string.place_finger_on_the_sensor);
        login_fingerprint_button.setImageDrawable(getDrawable(R.drawable.scan_fingerprint));
        Drawable don = login_fingerprint_button.getDrawable();
        if (don instanceof AnimatedVectorDrawable){
            avd = (AnimatedVectorDrawable)don;
            avd.registerAnimationCallback(new Animatable2.AnimationCallback() {
                @Override
                public void onAnimationEnd(Drawable drawable) {
                    avd.start();
                }
            });
            avd.start();
        }
    }

    public void showLoginFingerPrint(){
        tv_fingerprint_notice.setText(R.string.place_finger_on_the_sensor);
        login_fingerprint_button.setImageDrawable(getDrawable(R.drawable.show_fingerprint));
        Drawable don = login_fingerprint_button.getDrawable();
        if (don instanceof AnimatedVectorDrawable){
            avd = (AnimatedVectorDrawable)don;
            avd.start();
        }

    }

    public void showLoginFingerprintPass(){
        tv_fingerprint_notice.setText(R.string.fingerprint_pass);
        login_fingerprint_button.setImageDrawable(getDrawable(R.drawable.fingerprint_to_tick));
        Drawable don = login_fingerprint_button.getDrawable();
        if (don instanceof AnimatedVectorDrawable){
            avd = (AnimatedVectorDrawable)don;
            avd.start();
        }
    }

    public void showPasswordNotSecureError(){
        new SweetAlertDialog(this, SweetAlertDialog.ERROR_TYPE)
                .setContentText(getString(R.string.unsecure_master_password_error_content))
                .setTitleText(getString(R.string.error_dialog_title))
                .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                    @Override
                    public void onClick(SweetAlertDialog sweetAlertDialog) {
                        sweetAlertDialog.dismiss();
                    }
                })
                .show();
    }

    public void showPassNotEqualError(){
        new SweetAlertDialog(this, SweetAlertDialog.ERROR_TYPE)
                .setContentText(getString(R.string.check_two_password_error_content))
                .setTitleText(getString(R.string.error_dialog_title))
                .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                    @Override
                    public void onClick(SweetAlertDialog sweetAlertDialog) {
                        sweetAlertDialog.dismiss();
                    }
                })
                .show();
    }

    public void showUsernameNotExistError(){
        new SweetAlertDialog(this, SweetAlertDialog.ERROR_TYPE)
                .setContentText(getString(R.string.username_does_not_exist_error_content))
                .setTitleText(getString(R.string.error_dialog_title))
                .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                    @Override
                    public void onClick(SweetAlertDialog sweetAlertDialog) {
                        sweetAlertDialog.dismiss();
                    }
                })
                .show();
    }

    public void showNotOnlineError(){
        new SweetAlertDialog(this, SweetAlertDialog.ERROR_TYPE)
                .setContentText(getString(R.string.not_online_error))
                .setTitleText(getString(R.string.warning_title))
                .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                    @Override
                    public void onClick(SweetAlertDialog sweetAlertDialog) {
                        sweetAlertDialog.dismiss();
                    }
                })
                .show();
    }

//    public void showSignUpOfflineWarning(){
//
//    }

    public void showWrongPasswordError(){
        new SweetAlertDialog(this, SweetAlertDialog.ERROR_TYPE)
                .setContentText(getString(R.string.wrong_master_password_error))
                .setTitleText(getString(R.string.error_dialog_title))
                .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                    @Override
                    public void onClick(SweetAlertDialog sweetAlertDialog) {
                        sweetAlertDialog.dismiss();
                    }
                })
                .show();
    }

    public void showSomeError(){
        new SweetAlertDialog(this, SweetAlertDialog.ERROR_TYPE)
                .setContentText("Some errors happend\nPlease log in using password")
                .setTitleText(getString(R.string.error_dialog_title))
                .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                    @Override
                    public void onClick(SweetAlertDialog sweetAlertDialog) {
                        sweetAlertDialog.dismiss();
                    }
                })
                .show();
    }

    public void showUsernameAlreadyExistsError(){
        new SweetAlertDialog(this, SweetAlertDialog.ERROR_TYPE)
                .setContentText(getString(R.string.username_already_exists_error_content))
                .setTitleText(getString(R.string.error_dialog_title))
                .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                    @Override
                    public void onClick(SweetAlertDialog sweetAlertDialog) {
                        sweetAlertDialog.dismiss();
                    }
                })
                .show();
    }

    public void showSignupSuccessDialog(){
        new SweetAlertDialog(this, SweetAlertDialog.SUCCESS_TYPE)
                .setTitleText(getString(R.string.success_dialog_title))
                .setContentText(getString(R.string.sign_up_successful_dialog_content))
                .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                    @Override
                    public void onClick(SweetAlertDialog sweetAlertDialog) {
                        sweetAlertDialog.dismiss();
                        setupLogin();
                    }
                })
                .show();
    }

    public void showProgressBar(){
        login_button.setVisibility(View.GONE);
        progressWheel.spin();
        progressWheel.setVisibility(View.VISIBLE);

    }

    public void hideProgressbar(){
        progressWheel.setVisibility(View.GONE);
        progressWheel.stopSpinning();
        login_button.setVisibility(View.VISIBLE);
    }

    public void showFingerProgressbar(){
        progressWheel_fingerprint.setVisibility(View.VISIBLE);
        progressWheel_fingerprint.spin();
    }

    public void hideFingerProgressbar(){
        progressWheel_fingerprint.stopSpinning();
        progressWheel_fingerprint.setVisibility(View.GONE);
    }

    public void saveRecentUser(String username){
        SharedPreferences.Editor editor = getSharedPreferences("username", Context.MODE_PRIVATE).edit();
        editor.putString("username", username);
        editor.commit();
    }

    public String getRecentUser(){
        SharedPreferences preferences = getSharedPreferences("username", Context.MODE_PRIVATE);
        String user = preferences.getString("username", "");
        return user;
    }

    public boolean checkPasswordSecure(String pass){
        boolean checkUpper = false, checkLower = false, checkNumber = false, checkLenght = false;
        if (pass.length() >= 8)checkLenght = true;
        for (char c = 'a'; c <= 'z'; c++){
            if(pass.contains(String.valueOf(c))){
                checkLower = true;
                break;
            }
        }

        for (char c = 'A'; c <= 'Z'; c++){
            if(pass.contains(String.valueOf(c))){
                checkUpper = true;
                break;
            }
        }

        for (int c = 0; c <= 9; c++){
            if(pass.contains(String.valueOf(c))){
                checkNumber = true;
                break;
            }
        }

        return checkLower && checkUpper && checkNumber && checkLenght;
    }

    public void initRealm(){
        Realm.init(this);
    }

    @Override
    protected void onResume() {
        super.onResume();
        showHideFingerprint();
    }

    public void showHideFingerprint(){
        if(checkLoginFingerPrintSetting()){
            login_fingerprint_layout.setVisibility(View.VISIBLE);
            login_password_layout.setVisibility(View.GONE);
            showLoginFingerPrint();
            showLoginFingerprintScanning();
            startFingerprintAuthentication();
        }else {
            login_password_layout.setVisibility(View.VISIBLE);
            login_fingerprint_layout.setVisibility(View.GONE);

        }
    }

    @Override
    public void onBackPressed() {
        int mode = getMode();
        if(mode == 1){
            setupLogin();
        }
        else {
            new SweetAlertDialog(this, SweetAlertDialog.NORMAL_TYPE)
                    .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                        @Override
                        public void onClick(SweetAlertDialog sweetAlertDialog) {
                            sweetAlertDialog.dismiss();
                            LoginActivity.this.finishAndRemoveTask();
                            System.exit(0);
                        }
                    })
                    .setCancelClickListener(new SweetAlertDialog.OnSweetClickListener() {
                        @Override
                        public void onClick(SweetAlertDialog sweetAlertDialog) {
                            sweetAlertDialog.dismiss();
                        }
                    })
                    .setTitleText(getString(R.string.warning_title))
                    .setContentText(getString(R.string.exit_warning_content_text))
                    .setConfirmText(getString(R.string.dialog_confirm_text))
                    .setCancelText(getString(R.string.dialog_cancel_text))
                    .showCancelButton(true)
                    .show();
        }
    }

    public boolean checkLoginFingerPrintSetting(){
        final RealmResults<UserInfo>[] results = new RealmResults[1];
        realm.executeTransaction(new Realm.Transaction() {
            @Override
            public void execute(Realm realm) {
                results[0] = realm.where(UserInfo.class).findAll();
            }
        });
        if (results[0].size() == 0) return false;
        SharedPreferences preferences = getSharedPreferences(getRecentUser()+ "_" + getString(R.string.setting_fingerprint_unlock), MODE_PRIVATE);
        boolean ischeck = preferences.getBoolean(getRecentUser()+ "_" + getString(R.string.setting_fingerprint_unlock), false);
        return ischeck;
    }

    public boolean checkFirstLogin(String user){
        SharedPreferences preferences = getSharedPreferences(user + "_run_first", MODE_PRIVATE);
        boolean ischeck = preferences.getBoolean(user + "_run_first", true);
        return ischeck;
    }

    public void saveFirstLogin(String user){
        SharedPreferences.Editor editor = getSharedPreferences(user + "_run_first", MODE_PRIVATE).edit();
        editor.putBoolean(user + "_run_first", false);
        editor.commit();
    }

    public boolean isOnline() {
        ConnectivityManager cm =
                (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo netInfo = cm.getActiveNetworkInfo();
        return netInfo != null && netInfo.isConnectedOrConnecting();
    }

    public boolean checkOfflineUser(String user){
        final RealmResults<UserInfo>[] results = new RealmResults[1];
        realm.executeTransaction(new Realm.Transaction() {
            @Override
            public void execute(Realm realm) {
                results[0] = realm.where(UserInfo.class).equalTo("username", user).findAll();
            }
        });
        if (results[0].size() == 0) return false;
        else return true;
    }

    public void getLastestUserInfo(String user, String pass){
        if (isOnline()){

        }else {
            showNotOnlineError();
        }
    }

    @Override
    public void onClick(View view) {

    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
    }
}
