package com.samsung.ductran.dhpassword.model;

import android.widget.Toast;

import java.util.Date;

import io.realm.RealmObject;

/**
 * Created by sev_user on 4/23/2018.
 */

public class PasswordGenerated extends RealmObject {

    private String passwordgenerate;
    private Date date;

    public PasswordGenerated(String passwordgenerate, Date date) {
        this.passwordgenerate = passwordgenerate;
        this.date = date;
    }

    public PasswordGenerated() {
    }

    public String getPasswordgenerate() {
        return passwordgenerate;
    }

    public void setPasswordgenerate(String passwordgenerate) {
        this.passwordgenerate = passwordgenerate;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }
}
