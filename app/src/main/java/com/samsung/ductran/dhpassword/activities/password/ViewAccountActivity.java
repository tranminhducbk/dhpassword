package com.samsung.ductran.dhpassword.activities.password;

import android.content.ClipData;
import android.content.ClipboardManager;
import android.content.Context;
import android.content.Intent;
import android.graphics.drawable.ColorDrawable;
import android.icu.text.SimpleDateFormat;
import android.net.Uri;
import android.support.customtabs.CustomTabsIntent;
import android.support.design.widget.TextInputLayout;
import android.support.v4.app.ActivityOptionsCompat;
import android.support.v7.app.ActionBar;
import android.os.Bundle;
import android.text.Spannable;
import android.text.SpannableString;
import android.text.style.ForegroundColorSpan;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.samsung.ductran.dhpassword.R;
import com.samsung.ductran.dhpassword.activities.main.BaseActivity;
import com.samsung.ductran.dhpassword.utils.URLHelper;
import com.samsung.ductran.dhpassword.model.Login;
import com.samsung.ductran.dhpassword.model.URL;

import java.util.ArrayList;

import io.realm.Realm;

public class ViewAccountActivity extends BaseActivity {
    TextView txt_account_title,txt_account_url,txt_account_name,txt_account_password,txt_account_date;
    ImageButton img_btn_copy_password;
    TextInputLayout passwordLayout;
    ImageView avatar;
    Login account;
    URLHelper helper;
    ArrayList<URL> urls;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_view_account);
        ActionBar actionBar = getSupportActionBar();
        actionBar.setDisplayOptions(ActionBar.DISPLAY_HOME_AS_UP | ActionBar.DISPLAY_SHOW_TITLE);
        actionBar.setTitle(R.string.view_account_title);

        actionBar.setBackgroundDrawable(new ColorDrawable(getColor(R.color.total_transparent)));
        actionBar.setHomeAsUpIndicator(R.drawable.ic_back);

        Spannable text = new SpannableString(actionBar.getTitle());
        text.setSpan(new ForegroundColorSpan(getColor(R.color.colorPrimary)), 0, text.length(), Spannable.SPAN_INCLUSIVE_INCLUSIVE);
        actionBar.setTitle(text);

        helper = new URLHelper(this);
        urls = helper.initURL();

        initView();
        Intent intent = getIntent();
        account = (Login) intent.getSerializableExtra("account");
        showAccount();

        img_btn_copy_password.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ClipboardManager clipboard = (ClipboardManager)getSystemService(Context.CLIPBOARD_SERVICE);
                ClipData clip = ClipData.newPlainText("Copied",txt_account_password.getText().toString());
                clipboard.setPrimaryClip(clip);
                Toast.makeText(ViewAccountActivity.this, "Copied", Toast.LENGTH_SHORT).show();
            }
        });
        txt_account_url.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                sendBrowser();
            }
        });
    }
    private void sendBrowser() {
        String login_url = account.getUrl();
        if(login_url != null && !login_url.equals("")){
            CustomTabsIntent customTabsIntent = new CustomTabsIntent.Builder().build();
            for (URL url : urls){
                if(account.getUrl().equals(url.getUrl()) || account.getUrl().contains(url.getTitle().toLowerCase())){
                    login_url = url.getLogin_url();
                }
            }
            customTabsIntent.launchUrl(this, Uri.parse(login_url));
        }
    }
    private void showAccount() {
        txt_account_title.setText(account.getTitle());
        txt_account_url.setText(account.getUrl());
        txt_account_name.setText(account.getUsername());
        if(account.getPassword().equals("")){
            passwordLayout.setVisibility(View.GONE);
            img_btn_copy_password.setVisibility(View.GONE);
        }else {
            passwordLayout.setVisibility(View.VISIBLE);
            img_btn_copy_password.setVisibility(View.VISIBLE);
            txt_account_password.setText(account.getPassword());
        }
        String date = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(account.getDateCreated());
        txt_account_date.setText(getString (R.string.created) + ": " + date);

        boolean in = false;
        if(account.getUrl() != null){
            String dataURL = account.getUrl();
            for (URL url : urls){
                if( (!dataURL.equals(""))
                        && (dataURL.equals(url.getUrl())
                        || dataURL.contains(url.getTitle().toLowerCase())
                        || url.getUrl().contains(dataURL.toLowerCase()))){
                    Glide.with(this).load(helper.getLogo(url.getName())).asBitmap().into(avatar);
                    in = true;
                    break;
                }
            }
        }
        if (!in)Glide.with(this).load(helper.getLogo("browser")).into((ImageView) avatar);
    }

    public void reloadData(){
        Realm realm = Realm.getDefaultInstance();
        account = realm.where(Login.class).equalTo("id", account.getId()).findAll().first();
        txt_account_title.setText(account.getTitle());
        txt_account_url.setText(account.getUrl());
        txt_account_name.setText(account.getUsername());
        if(account.getPassword().equals("")){
            passwordLayout.setVisibility(View.GONE);
            img_btn_copy_password.setVisibility(View.GONE);
        }else {
            passwordLayout.setVisibility(View.VISIBLE);
            img_btn_copy_password.setVisibility(View.VISIBLE);
            txt_account_password.setText(account.getPassword());
        }
        String date = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(account.getDateCreated());
        txt_account_date.setText(getString (R.string.created) + ": " + date);

        boolean in = false;
        if(account.getUrl() != null){
            String dataURL = account.getUrl();
            for (URL url : urls){
                if( (!dataURL.equals(""))
                        && (dataURL.equals(url.getUrl())
                        || dataURL.contains(url.getTitle().toLowerCase())
                        || url.getUrl().contains(dataURL.toLowerCase()))){
                    Glide.with(this).load(helper.getLogo(url.getName())).asBitmap().into(avatar);
                    in = true;
                    break;
                }
            }
        }
        if (!in)Glide.with(this).load(helper.getLogo("browser")).into((ImageView) avatar);
        realm.close();
    }

    public boolean onSupportNavigateUp() {
        onBackPressed();
        return super.onSupportNavigateUp();
    }
    private void initView() {
        txt_account_title = (TextView) findViewById(R.id.txt_account_title);
        txt_account_name= (TextView) findViewById(R.id.txt_account_name);
        txt_account_password = (TextView) findViewById(R.id.txt_account_password);
        txt_account_url = (TextView) findViewById(R.id.txt_account_url);
        txt_account_date = (TextView) findViewById(R.id.txt_account_date);
        img_btn_copy_password= (ImageButton) findViewById(R.id.ic_copy_password);
        passwordLayout = findViewById(R.id.view_account_password_layout);
        avatar  = findViewById(R.id.view_account_avatar);
        avatar.setImageDrawable(null);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_edit_item, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        switch (id){
            case R.id.user_info_edit:
                Intent intent = new Intent(ViewAccountActivity.this, AddOrUpdatePasswordActivity.class);
                Login login = new Login(account.getId(), account.getUrl(), account.getAutofillHint()
                                        , account.getUsername(), account.getPassword()
                                        , account.getTitle(), account.getDateCreated());
                intent.putExtra("account", login);
                ActivityOptionsCompat options = ActivityOptionsCompat.
                        makeSceneTransitionAnimation(this, avatar, "listview_item_imageView");
                startActivity(intent, options.toBundle());
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onResume() {
        super.onResume();
        reloadData();
    }
}
