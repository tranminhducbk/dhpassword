package com.samsung.ductran.dhpassword.model;

import java.io.Serializable;
import java.util.Date;

import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;

public class Note extends RealmObject implements Serializable {
    @PrimaryKey
    int id;
    String title;
    String data;
    Date dateCreated;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getData() {
        return data;
    }

    public void setData(String data) {
        this.data = data;
    }

    public Date getDateCreated() {
        return dateCreated;
    }

    public void setDateCreated(Date dateCreated) {
        this.dateCreated = dateCreated;
    }

    public Note(){

    }

    public Note(int id, String title, String data, Date dateCreated) {
        this.id = id;
        this.title = title;
        this.data = data;
        this.dateCreated = dateCreated;
    }
}
