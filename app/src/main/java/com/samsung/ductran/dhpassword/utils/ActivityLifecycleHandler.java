package com.samsung.ductran.dhpassword.utils;

import android.app.Activity;
import android.app.Application;
import android.content.Intent;
import android.os.Bundle;

import com.samsung.ductran.dhpassword.R;
import com.samsung.ductran.dhpassword.activities.login.LoginActivity;
import com.samsung.ductran.dhpassword.activities.login.SplashActivity;

/**
 * Created by Duc Tran on 5/1/2018.
 */

public class ActivityLifecycleHandler implements Application.ActivityLifecycleCallbacks {

    private static Activity mCurrentActivity;
    private static String mCurrentActivityName;
    private static boolean mMovedIntoBkg = false;
    private static boolean mAppLock = true;
    private static boolean isRunFormIcon = true;
    private static String username;

    public static void setUsername(String username) {
        ActivityLifecycleHandler.username = username;
    }

    public static boolean isIsRunFormIcon() {
        return isRunFormIcon;
    }

    public static void setIsRunFormIcon(boolean isRunFormIcon) {
        ActivityLifecycleHandler.isRunFormIcon = isRunFormIcon;
    }

    private int numberActivitiesStart = 0;

    public static boolean isAppInBackground() {
        return mMovedIntoBkg;
    }

    public static boolean isAppLock() {
        return mAppLock;
    }

    public static void setMovedIntoBackground(boolean mMovedIntoBkg) {
        ActivityLifecycleHandler.mMovedIntoBkg = mMovedIntoBkg;
    }

    public static void setAppLock(boolean mAppLock) {
        ActivityLifecycleHandler.mAppLock = mAppLock;
    }

    @Override
    public void onActivityCreated(Activity activity, Bundle bundle) {

    }

    @Override
    public void onActivityStarted(Activity activity) {
//        if(isAppInBackground()){
//            setMovedIntoBackground(false);
//            if(isAppLock() && !(activity instanceof LoginActivity)){
//                Intent intent = new Intent(activity, LoginActivity.class);
//                activity.startActivity(intent);
//                boolean test = isAppInBackground();
//                int i = 0;
//            }
//        }

//        if (numberActivitiesStart == 0) {
//            // The application come from background to foreground
//            setMovedIntoBackground(false);
//            if(isAppLock() && !(activity instanceof LoginActivity)){
//                Intent intent = new Intent(activity, LoginActivity.class);
//                activity.startActivity(intent);
//            }
//        }
//        numberActivitiesStart++;
//        int i = 0;
    }

    public static void onActivityRestarted(Activity activity){

    }

    @Override
    public void onActivityResumed(Activity activity) {

    }

    @Override
    public void onActivityPaused(Activity activity) {

    }

    @Override
    public void onActivityStopped(Activity activity) {
//        if (!isAppInBackground()){
//            setMovedIntoBackground(true);
//            setAppLock(true);
//        }

//        numberActivitiesStart--;
//        if (numberActivitiesStart == 0) {
//            // The application go from foreground to background
//            setMovedIntoBackground(true);
//            setAppLock(true);
//        }
//        int i = 0;
    }

    @Override
    public void onActivitySaveInstanceState(Activity activity, Bundle bundle) {

    }

    @Override
    public void onActivityDestroyed(Activity activity) {

    }

    public void onAppStarted(Activity activity){
        setMovedIntoBackground(false);
        if(isAppLock() && !(activity instanceof LoginActivity)){
            Intent intent = new Intent(activity, SplashActivity.class);
            activity.startActivity(intent);
        }
    }

    public void onAppPaused(Activity activity){
        setMovedIntoBackground(true);
        SettingUtil.setContex(activity);
        boolean isApplockSetting = SettingUtil.getSetting(username+"_"+activity.getString(R.string.auto_lock_app));
        if(isApplockSetting)setAppLock(true);
    }
}
