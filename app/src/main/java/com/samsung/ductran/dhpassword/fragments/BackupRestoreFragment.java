package com.samsung.ductran.dhpassword.fragments;

import android.Manifest;
import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.LinearLayout;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.pnikosis.materialishprogress.ProgressWheel;
import com.samsung.ductran.dhpassword.R;
import com.samsung.ductran.dhpassword.activities.main.MainActivity;
import com.samsung.ductran.dhpassword.adapters.RestoreRecyclerViewAdapter;
import com.samsung.ductran.dhpassword.model.Card;
import com.samsung.ductran.dhpassword.model.Login;
import com.samsung.ductran.dhpassword.model.Note;
import com.samsung.ductran.dhpassword.model.UserInfo;
import com.samsung.ductran.dhpassword.utils.FileChooser;
import com.samsung.ductran.dhpassword.utils.HashHelper;
import com.samsung.ductran.dhpassword.utils.SettingUtil;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.net.URI;
import java.util.ArrayList;

import cn.pedant.SweetAlert.SweetAlertDialog;
import io.realm.Realm;
import io.realm.RealmConfiguration;
import io.realm.RealmResults;
import io.realm.exceptions.RealmFileException;

/**
 * A fragment representing a list of Items.
 * <p/>
 * Activities containing this fragment MUST implement the {@link OnListFragmentInteractionListener}
 * interface.
 */
public class BackupRestoreFragment extends Fragment {

    private OnListFragmentInteractionListener mListener;
    ArrayList<String> restoreData;

    LinearLayout backUpToServerLayout;
    LinearLayout backUpToFileLayout;
    LinearLayout restoreFromServerlayout;
    LinearLayout restoreFromFileLayout;
    RecyclerView recyclerView;
    ProgressWheel progressWheel;
    RestoreRecyclerViewAdapter recyclerViewAdapter;

    FirebaseAuth auth = FirebaseAuth.getInstance();
    FirebaseDatabase database = FirebaseDatabase.getInstance();

    Realm realm;

    private final int BACKUP_REQUEST_CODE = 100;
    private final int RESTORE_REQUEST_CODE = 200;

    private String username;
    private String password;

    boolean flag = false;

    /**
     * Mandatory empty constructor for the fragment manager to instantiate the
     * fragment (e.g. upon screen orientation changes).
     */
    public BackupRestoreFragment() {
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

//        if (getArguments() != null) {
//            mColumnCount = getArguments().getInt(ARG_COLUMN_COUNT);
//        }

        username = ((MainActivity)getActivity()).getUsername();
        password = ((MainActivity)getActivity()).getMasterPassword();

        realm = Realm.getDefaultInstance();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_item_list_restore, container, false);

        restoreData = new ArrayList<>();
        recyclerView = view.findViewById(R.id.list);
        backUpToServerLayout = view.findViewById(R.id.backup_server_layout);
        backUpToServerLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                backupDataToServer();
            }
        });
        backUpToFileLayout = view.findViewById(R.id.backup_file_layout);
        backUpToFileLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showBackuptoFileMessage();
            }
        });
        restoreFromFileLayout = view.findViewById(R.id.restore_from_file_layout);
        restoreFromFileLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                restoreFromFile();
            }
        });
        restoreFromServerlayout = view.findViewById(R.id.restore_from_server_layout);
        progressWheel = view.findViewById(R.id.restore_progress_wheel);

        // Set the adapter
        String username = ((MainActivity)getActivity()).getUsername();
        SettingUtil.setContex(getActivity());
        boolean isOfflineMode = SettingUtil.getSetting(username + getString(R.string.use_offline_setting));
        if(isOfflineMode){
            backUpToServerLayout.setVisibility(View.GONE);
            restoreFromServerlayout.setVisibility(View.GONE);
            recyclerView.setVisibility(View.GONE);
        }else {
            backUpToServerLayout.setVisibility(View.VISIBLE);
            restoreFromServerlayout.setVisibility(View.VISIBLE);
            recyclerView.setVisibility(View.VISIBLE);
            recyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
            getData();
        }

        return view;
    }

    public void backupDataToServer(){
        ((MainActivity)getActivity()).backUpData();
    }

    public void backUpToFile(){
        if (ContextCompat.checkSelfPermission(getActivity(), Manifest.permission.WRITE_EXTERNAL_STORAGE)
                != PackageManager.PERMISSION_GRANTED) {

            if (ActivityCompat.shouldShowRequestPermissionRationale(getActivity(),
                    Manifest.permission.WRITE_EXTERNAL_STORAGE)) {
                showBackupExplanation();
            } else {
                ActivityCompat.requestPermissions(getActivity(), new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE}, BACKUP_REQUEST_CODE);
            }
        } else {
            String username = ((MainActivity)getActivity()).getUsername();

            File sdCard = Environment.getExternalStorageDirectory();
            String sdcard_dir = sdCard.getAbsolutePath();
            String savedir = sdcard_dir + "/DHpasswordBackup";
            // Create if necessary the desired folder
            File directory = new File (savedir);
            if (!directory.exists())directory.mkdirs();
            File destination = new File(directory,username + ".realm");
            if (destination.exists())destination.delete();
            realm.writeEncryptedCopyTo(new File(directory, username + ".realm"),
                    HashHelper.hexStringToByteArray(HashHelper.get_SHA_512(password, username)));
            ((MainActivity)getActivity()).showBackUpSuccessDialog();
        }
    }

    public void restoreFromFile(){
        if (ContextCompat.checkSelfPermission(getActivity(), Manifest.permission.READ_EXTERNAL_STORAGE)
                != PackageManager.PERMISSION_GRANTED) {

            if (ActivityCompat.shouldShowRequestPermissionRationale(getActivity(),
                    Manifest.permission.READ_EXTERNAL_STORAGE)) {
                showRestoreExplanation();
            } else {
                ActivityCompat.requestPermissions(getActivity(), new String[]{Manifest.permission.READ_EXTERNAL_STORAGE}, BACKUP_REQUEST_CODE);
            }
        } else {
            boolean isLockAppEnable = SettingUtil.getSetting(username + "_" + getString(R.string.auto_lock_app));
            if(isLockAppEnable){
                SettingUtil.saveSetting(username + "_" + getString(R.string.auto_lock_app), false);
                flag = true;
            }
            File sdCard = Environment.getExternalStorageDirectory();
            String sdcard_dir = sdCard.getAbsolutePath();
            String savedir = sdcard_dir + "/DHpasswordBackup";

            Intent intent = new Intent(Intent.ACTION_OPEN_DOCUMENT);
            intent.addCategory(Intent.CATEGORY_OPENABLE);
            intent.setData(Uri.parse(savedir));
            intent.setType("*/*");
            startActivityForResult(intent, RESTORE_REQUEST_CODE);
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        switch (requestCode) {
            case BACKUP_REQUEST_CODE: {
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    String username = ((MainActivity)getActivity()).getUsername();

                    File sdCard = Environment.getExternalStorageDirectory();
                    String sdcard_dir = sdCard.getAbsolutePath();
                    String savedir = sdcard_dir + "/DHpasswordBackup";
                    // Create if necessary the desired folder
                    File directory = new File (savedir);
                    if (!directory.exists())directory.mkdirs();
                    File destination = new File(directory,username + ".realm");
                    if (destination.exists())destination.delete();
                    realm.writeEncryptedCopyTo(new File(directory, username + ".realm"),
                            HashHelper.hexStringToByteArray(HashHelper.get_SHA_512(password, username)));
                    ((MainActivity)getActivity()).showBackUpSuccessDialog();
                } else {
                    showCannotBackuptoFileError();
                }
                return;
            }

            case RESTORE_REQUEST_CODE: {
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    boolean isLockAppEnable = SettingUtil.getSetting(username + "_" + getString(R.string.auto_lock_app));
                    if(isLockAppEnable){
                        SettingUtil.saveSetting(username + "_" + getString(R.string.auto_lock_app), false);
                        flag = true;
                    }
                    File sdCard = Environment.getExternalStorageDirectory();
                    String sdcard_dir = sdCard.getAbsolutePath();
                    String savedir = sdcard_dir + "/DHpasswordBackup";

                    Intent intent = new Intent(Intent.ACTION_GET_CONTENT);
                    intent.addCategory(Intent.CATEGORY_OPENABLE);
                    intent.setData(Uri.parse(savedir));
                    intent.setType("*/*");
                    getActivity().startActivityForResult(Intent.createChooser(intent, "Open folder"), RESTORE_REQUEST_CODE);
                } else {
                    showCannotRestoreFromFileError();
                }
                return;
            }
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if(flag){
            flag = false;
            SettingUtil.saveSetting(username + "_" + getString(R.string.auto_lock_app), true);
        }
        if(resultCode == Activity.RESULT_OK){
            Uri uri = null;
            if (data != null) {
                uri = data.getData();
            }
            if(uri != null){
                View customView = getActivity().getLayoutInflater().inflate(R.layout.layout_input_restore_password, null);
                EditText edt_pass_to_unlock = customView.findViewById(R.id.restore_edt_password);
                AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
                builder.setTitle("Input your master password when you backup this file");
                builder.setView(customView);
                builder.setCancelable(true);
                Uri finalUri = uri;
                builder.setPositiveButton(getString(R.string.dialog_ok), new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        String passwordToUnlock = edt_pass_to_unlock.getText().toString();
                        if (passwordToUnlock == null || passwordToUnlock.equals(""))edt_pass_to_unlock.setError(getString(R.string.empty));
                        else {
                            dialog.dismiss();
                            copyAllDataFromFile(finalUri, passwordToUnlock);
                        }
                    }
                });
                builder.setNegativeButton(getString(R.string.cancel_label), new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                });

                AlertDialog dialog = builder.create();
                if (!dialog.isShowing())dialog.show();
            }
        }
    }

    public void copyAllDataFromFile(Uri uri, String password){
        try {
            showProgressWheel();
            File restoreFolder = new File(Environment.getExternalStorageDirectory().getAbsolutePath() + "/DHrestore");
            if (!restoreFolder.exists())restoreFolder.mkdir();
            File restoreFile = new File(new File(Environment.getExternalStorageDirectory().getAbsolutePath() + "/DHrestore"), username + ".realm");
            if (restoreFile.exists())restoreFile.delete();
            FileOutputStream outputStream = new FileOutputStream(restoreFile);
            FileInputStream inputStream = new FileInputStream(FileChooser.getPath(getActivity(), uri));
            byte[] buf = new byte[1024];
            int bytesRead;
            while ((bytesRead = inputStream.read(buf)) > 0) {
                outputStream.write(buf, 0, bytesRead);
            }
            outputStream.close();

            RealmConfiguration restoreConfig = new RealmConfiguration.Builder()
                    .directory(new File(Environment.getExternalStorageDirectory().getAbsolutePath() + "/DHrestore"))
                    .name(username+".realm")
                    .encryptionKey(HashHelper.hexStringToByteArray(HashHelper.get_SHA_512(password, username)))
                    .build();
            Realm restoreRealm = Realm.getInstance(restoreConfig);

            RealmResults<Login> loginResults = restoreRealm.where(Login.class).findAll();
            for (Login login : loginResults){
                Login newLogin = new Login(login.getId(), login.getUrl(), login.getAutofillHint(), login.getUsername(), login.getPassword(),
                        login.getTitle(), login.getDateCreated());
                realm.executeTransaction(new Realm.Transaction() {
                    @Override
                    public void execute(Realm realm) {
                        realm.copyToRealmOrUpdate(newLogin);
                    }
                });
            }

            RealmResults<Note> noteResults = restoreRealm.where(Note.class).findAll();
            for (Note note : noteResults){
                Note newNote = new Note(note.getId(), note.getTitle(), note.getData(), note.getDateCreated());
                realm.executeTransaction(new Realm.Transaction() {
                    @Override
                    public void execute(Realm realm) {
                        realm.copyToRealmOrUpdate(newNote);
                    }
                });
            }

            RealmResults<Card> cardResults = restoreRealm.where(Card.class).findAll();
            for (Card card : cardResults){
                Card newCard = new Card(card.getId(), card.getTitle(), card.getCardHolder(), card.getType(),
                        card.getCardNumber(), card.getExpirationDate(), card.getValidFrom(), card.getDateCreated(),
                        card.getPin(), card.getCvv());
                realm.executeTransaction(new Realm.Transaction() {
                    @Override
                    public void execute(Realm realm) {
                        realm.copyToRealmOrUpdate(newCard);
                    }
                });
            }
            ((MainActivity)getActivity()).loadDrawerCount();
            hideProgressWheel();
            ((MainActivity)getActivity()).showRestoreSuccessDialog();
        }catch (RealmFileException re){
            hideProgressWheel();
            showRealmError();
        }
        catch (Exception e){
            hideProgressWheel();
            showCannotGetDataError();
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnListFragmentInteractionListener) {
            mListener = (OnListFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnListFragmentInteractionListener");
        }
    }

    public void getData(){
        showProgressWheel();
        String username = ((MainActivity)getActivity()).getUsername();
        String password = ((MainActivity)getActivity()).getMasterPassword();
        auth.signInWithEmailAndPassword(username + "@dhpassword.com", password)
                .addOnCompleteListener(new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        if (task.isSuccessful()){
                            DatabaseReference rootRef = database.getReference().child(HashHelper.get_SHA_512(username, ""));
                            rootRef.addValueEventListener(new ValueEventListener() {
                                @Override
                                public void onDataChange(DataSnapshot dataSnapshot) {
                                    restoreData.clear();
                                    for (DataSnapshot dateSnapshot: dataSnapshot.getChildren()) {
                                        restoreData.add(dateSnapshot.getKey());
                                    }
                                    recyclerViewAdapter = new RestoreRecyclerViewAdapter(restoreData, mListener);
                                    recyclerView.setAdapter(recyclerViewAdapter);
                                    recyclerViewAdapter.notifyDataSetChanged();
                                    hideProgressWheel();
                                }

                                @Override
                                public void onCancelled(DatabaseError databaseError) {
                                    showCannotGetDataError();
                                }
                            });
                        }else {
                            showCannotGetDataError();
                        }
                    }
                });
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    public void showProgressWheel(){
        recyclerView.setVisibility(View.GONE);
        progressWheel.setVisibility(View.VISIBLE);
        progressWheel.spin();
    }

    public void hideProgressWheel(){
        recyclerView.setVisibility(View.VISIBLE);
        progressWheel.setVisibility(View.GONE);
        progressWheel.stopSpinning();
    }

    public void showBackupExplanation(){
        hideProgressWheel();
        new SweetAlertDialog(getActivity(), SweetAlertDialog.NORMAL_TYPE)
                .setTitleText(getString(R.string.normal_title))
                .setContentText("You must accept the permission in order to backup to a file")
                .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                    @Override
                    public void onClick(SweetAlertDialog sweetAlertDialog) {
                        sweetAlertDialog.dismiss();
                        ActivityCompat.requestPermissions(getActivity(), new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE}, BACKUP_REQUEST_CODE);
                    }
                })
                .show();
    }

    public void showRestoreExplanation(){
        hideProgressWheel();
        new SweetAlertDialog(getActivity(), SweetAlertDialog.NORMAL_TYPE)
                .setTitleText(getString(R.string.normal_title))
                .setContentText("You must accept the permission in order to restore data")
                .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                    @Override
                    public void onClick(SweetAlertDialog sweetAlertDialog) {
                        sweetAlertDialog.dismiss();
                        ActivityCompat.requestPermissions(getActivity(), new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE}, RESTORE_REQUEST_CODE);
                    }
                })
                .show();
    }

    public void showCannotGetDataError(){
        hideProgressWheel();
        new SweetAlertDialog(getActivity(), SweetAlertDialog.ERROR_TYPE)
                .setTitleText(getString(R.string.error_dialog_title))
                .setContentText(getString(R.string.cannot_retrieve_data_error))
                .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                    @Override
                    public void onClick(SweetAlertDialog sweetAlertDialog) {
                        sweetAlertDialog.dismiss();
                    }
                })
                .show();
    }

    public void showRealmError(){
        hideProgressWheel();
        new SweetAlertDialog(getActivity(), SweetAlertDialog.ERROR_TYPE)
                .setTitleText(getString(R.string.error_dialog_title))
                .setContentText("Cannot unlock data. May be your password is wrong or it's not your backup file")
                .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                    @Override
                    public void onClick(SweetAlertDialog sweetAlertDialog) {
                        sweetAlertDialog.dismiss();
                    }
                })
                .show();
    }

    public void showCannotBackuptoFileError(){
        hideProgressWheel();
        new SweetAlertDialog(getActivity(), SweetAlertDialog.ERROR_TYPE)
                .setTitleText(getString(R.string.error_dialog_title))
                .setContentText("Cannot backup to file")
                .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                    @Override
                    public void onClick(SweetAlertDialog sweetAlertDialog) {
                        sweetAlertDialog.dismiss();
                    }
                })
                .show();
    }

    public void showCannotRestoreFromFileError(){
        hideProgressWheel();
        new SweetAlertDialog(getActivity(), SweetAlertDialog.ERROR_TYPE)
                .setTitleText(getString(R.string.error_dialog_title))
                .setContentText("Cannot restore from file")
                .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                    @Override
                    public void onClick(SweetAlertDialog sweetAlertDialog) {
                        sweetAlertDialog.dismiss();
                    }
                })
                .show();
    }

    public void showBackuptoFileMessage(){
        hideProgressWheel();
        new SweetAlertDialog(getActivity(), SweetAlertDialog.NORMAL_TYPE)
                .setTitleText(getString(R.string.normal_title))
                .setContentText(getString(R.string.backup_file_message))
                .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                    @Override
                    public void onClick(SweetAlertDialog sweetAlertDialog) {
                        sweetAlertDialog.dismiss();
                        backUpToFile();
                    }
                })
                .show();
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p/>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnListFragmentInteractionListener {
        // TODO: Update argument type and name
        void onListFragmentInteraction(String item);
    }
}
