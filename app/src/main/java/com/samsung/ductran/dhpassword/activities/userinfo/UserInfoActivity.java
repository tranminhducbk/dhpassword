package com.samsung.ductran.dhpassword.activities.userinfo;

import android.content.ClipData;
import android.content.ClipboardManager;
import android.content.Context;
import android.content.Intent;
import android.icu.text.SimpleDateFormat;
import android.os.Bundle;
import android.support.v4.app.ActivityOptionsCompat;
import android.support.v7.app.ActionBar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.Toast;

import com.github.siyamed.shapeimageview.CircularImageView;
import com.samsung.ductran.dhpassword.R;
import com.samsung.ductran.dhpassword.activities.main.BaseActivity;
import com.samsung.ductran.dhpassword.model.UserInfo;
import com.samsung.ductran.dhpassword.utils.BitmapHelper;

import io.realm.Realm;
import io.realm.RealmConfiguration;

public class UserInfoActivity extends BaseActivity {

    CircularImageView user_avatar;
    TextView name, gender, age, birthday, mobile, email, address;
    ImageButton copyMobile, copyEmail, copyAddress;

    UserInfo userInfo;
    Realm realm;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_user_info);
        ActionBar actionBar = getSupportActionBar();
        actionBar.setDisplayOptions(ActionBar.DISPLAY_HOME_AS_UP | ActionBar.DISPLAY_SHOW_TITLE);
        actionBar.setTitle(getString(R.string.user_info_title));

        initView();

        RealmConfiguration configuration = new RealmConfiguration.Builder()
                .name("UserInformation.realm")
                .build();
        realm = Realm.getInstance(configuration);

        getUserInfo();

        showInfo();
    }

    public void initView(){
        user_avatar = findViewById(R.id.user_image);
        name = findViewById(R.id.user_info_name);
        gender = findViewById(R.id.user_info_gender);
        age = findViewById(R.id.user_info_age);
        birthday = findViewById(R.id.user_info_birthday);
        mobile = findViewById(R.id.user_info_mobile);
        copyMobile = findViewById(R.id.user_copy_mobile);

        email = findViewById(R.id.user_info_email);
        copyEmail = findViewById(R.id.user_copy_email);

        address = findViewById(R.id.user_info_address);
        copyAddress = findViewById(R.id.user_copy_address);
    }

    public void getUserInfo(){
        String username = getIntent().getStringExtra("username");
        userInfo = realm.where(UserInfo.class).equalTo("username", username).findFirst();
        if (userInfo == null)userInfo = new UserInfo();
    }

    public void showInfo(){
        if(userInfo != null){
            name.setText(userInfo.getName());
            gender.setText(userInfo.getGender());
            age.setText(userInfo.getAge() == 0? "" : String.valueOf(userInfo.getAge()));
            if(userInfo.getBirthday() != null) birthday.setText(new SimpleDateFormat("dd/MM/yyyy").format(userInfo.getBirthday()));
            if (userInfo.getAvatar() !=null) user_avatar.setImageBitmap(BitmapHelper.getBitmapFromBytes(userInfo.getAvatar()));
            mobile.setText(userInfo.getMobile());
            email.setText(userInfo.getEmail());
            address.setText(userInfo.getAddress());

            copyMobile.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    copyInfo(mobile.getText().toString());
                }
            });

            copyAddress.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    copyInfo(address.getText().toString());
                }
            });

            copyEmail.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    copyInfo(email.getText().toString());
                }
            });
        }
    }

    public void copyInfo(String info){
        ClipboardManager clipboard = (ClipboardManager)getSystemService(Context.CLIPBOARD_SERVICE);
        ClipData clip = ClipData.newPlainText("Copied",info);
        clipboard.setPrimaryClip(clip);
        Toast.makeText(UserInfoActivity.this, "Copied", Toast.LENGTH_SHORT).show();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_edit_user, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        switch (id){
            case R.id.user_info_edit:
                Intent intent_user = new Intent(UserInfoActivity.this, EditUserInfoActivity.class);
                UserInfo user = new UserInfo(userInfo.getId(), userInfo.getUsername());
                user.setName(userInfo.getName());
                user.setGender(userInfo.getGender());
                user.setAge(userInfo.getAge());
                user.setBirthday(userInfo.getBirthday());
                user.setMobile(userInfo.getMobile());
                user.setAddress(userInfo.getAddress());
                user.setEmail(userInfo.getEmail());

                user.setEncryptedPassword(userInfo.getEncryptedPassword());
                user.setIv(userInfo.getIv());
                user.setAvatar(userInfo.getAvatar());

                intent_user.putExtra("user", user);
                ActivityOptionsCompat options = ActivityOptionsCompat.
                        makeSceneTransitionAnimation(this, user_avatar, getString(R.string.nav_header_main_transition_name));
                startActivity(intent_user);
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return super.onSupportNavigateUp();
    }

    @Override
    protected void onResume() {
        super.onResume();
        getUserInfo();
        showInfo();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        realm.close();
        Intent resIntent = new Intent();
        resIntent.putExtra("avatar", userInfo.getAvatar());
        setResult(1000, resIntent);
    }
}
