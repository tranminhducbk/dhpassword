package com.samsung.ductran.dhpassword.autofill;

import android.annotation.TargetApi;
import android.app.PendingIntent;
import android.app.assist.AssistStructure;
import android.content.Context;
import android.content.Intent;
import android.content.IntentSender;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.graphics.BitmapFactory;
import android.os.Build;
import android.os.Bundle;
import android.os.CancellationSignal;
import android.service.autofill.AutofillService;
import android.service.autofill.Dataset;
import android.service.autofill.FillCallback;
import android.service.autofill.FillContext;
import android.service.autofill.FillRequest;
import android.service.autofill.FillResponse;
import android.service.autofill.SaveCallback;
import android.service.autofill.SaveRequest;
import android.support.annotation.NonNull;
import android.util.Log;
import android.view.View;
import android.view.autofill.AutofillId;
import android.view.autofill.AutofillValue;
import android.widget.RemoteViews;
import android.widget.Toast;

import com.samsung.ductran.dhpassword.R;
import com.samsung.ductran.dhpassword.model.Login;
import com.samsung.ductran.dhpassword.model.URL;
import com.samsung.ductran.dhpassword.model.UserInfo;
import com.samsung.ductran.dhpassword.utils.Decryptor;
import com.samsung.ductran.dhpassword.utils.Encryptor;
import com.samsung.ductran.dhpassword.utils.HashHelper;
import com.samsung.ductran.dhpassword.utils.URLHelper;

import java.util.ArrayList;
import java.util.List;

import cn.pedant.SweetAlert.SweetAlertDialog;
import io.realm.Case;
import io.realm.Realm;
import io.realm.RealmConfiguration;
import io.realm.RealmQuery;
import io.realm.RealmResults;
import io.realm.exceptions.RealmFileException;

import static android.view.autofill.AutofillManager.EXTRA_AUTHENTICATION_RESULT;
import static java.util.stream.Collectors.toList;

@TargetApi(Build.VERSION_CODES.O)
public class DHpasswordAutoFillService extends AutofillService {

    private static String recentPackageName;
    Realm realm;

    @Override
    public void onCreate() {
        super.onCreate();
        Realm.init(this);
        RealmConfiguration configuration = new RealmConfiguration.Builder()
                .name("UserInformation.realm")
                .build();
        realm = Realm.getInstance(configuration);
    }

    @Override
    public void onFillRequest(@NonNull FillRequest fillRequest, @NonNull CancellationSignal cancellationSignal, @NonNull FillCallback fillCallback) {
        // Create an empty list

        ArrayList<AssistStructure.ViewNode> emailFields = new ArrayList<>();
        ArrayList<AssistStructure.ViewNode> passFields = new ArrayList<>();

        // Populate the list
        List<FillContext> fillContexts = fillRequest.getFillContexts();
        List<AssistStructure> structures = fillContexts.stream().map(FillContext::getStructure).collect(toList());

        for (AssistStructure structure : structures){
            identifyEmailFields(structure.getWindowNodeAt(0).getRootViewNode(), emailFields);
            identifyPasswordFields(structure.getWindowNodeAt(0).getRootViewNode(), passFields);
        }

        String packageName = structures.get(structures.size()-1).getActivityComponent().getPackageName();

        if(emailFields.size() == 0 || passFields.size() == 0 || packageName.equals("com.samsung.ductran.dhpassword")){
//            Toast.makeText(this, "Field cannot be autofilled", Toast.LENGTH_LONG).show();
            return;
        }

        AssistStructure.ViewNode emailField = emailFields.get(0);
        AssistStructure.ViewNode passField = passFields.get(0);

        if(recentPackageName!=null && recentPackageName.equals(packageName)){
            FillResponse response = null;
            try {
                response = getResponse(packageName, emailField.getAutofillId(), passField.getAutofillId());
            } catch (PackageManager.NameNotFoundException e) {
                e.printStackTrace();
            }
            fillCallback.onSuccess(response);
        }
        else {
            if(recentPackageName == null || !recentPackageName.equals(packageName))recentPackageName = packageName;

            RemoteViews rvAuthen = new RemoteViews(getPackageName(), R.layout.autofill_service_unlock_item);
            Intent authIntent = new Intent(this, AutofillUnlockActivity.class);
            authIntent.putExtra("username", getRecentUser());
            authIntent.putExtra("packageName", packageName);
            authIntent.putExtra("emailFieldID", emailField.getAutofillId());
            authIntent.putExtra("passwordFieldID", passField.getAutofillId());
            authIntent.putExtra("MY_EXTRA_DATASET_NAME", "my_dataset");
            IntentSender intentSender = PendingIntent.getActivity(
                    this,
                    1001,
                    authIntent,
                    PendingIntent.FLAG_CANCEL_CURRENT
            ).getIntentSender();

            FillResponse fillResponse = new FillResponse.Builder()
                    .setAuthentication(new AutofillId[]{emailField.getAutofillId(), passField.getAutofillId()}, intentSender, rvAuthen)
                    .build();
            if(fillResponse != null)fillCallback.onSuccess(fillResponse);
        }
    }

    @Override
    public void onSaveRequest(@NonNull SaveRequest saveRequest, @NonNull SaveCallback saveCallback) {

    }

    public void identifyEmailFields(AssistStructure.ViewNode node, List<AssistStructure.ViewNode> emailFields) {
        if (node.getAutofillType() == View.AUTOFILL_TYPE_TEXT && (node.getClassName() != null && (node.getClassName().toLowerCase().contains("edittext")
                || node.getClassName().toLowerCase().contains("textview")))) {
            String hint = node.getHint();
            String viewId = node.getIdEntry();
            if ((hint != null && (hint.toLowerCase().contains("email") || hint.toLowerCase().contains("username")))
                    || (viewId != null && (viewId.toLowerCase().contains("email") || viewId.toLowerCase().contains("username")))) {
                emailFields.add(node);
                return;
            }
        }
        for (int i = 0; i < node.getChildCount(); i++) {
            identifyEmailFields(node.getChildAt(i), emailFields);
        }
    }

    public void identifyPasswordFields(AssistStructure.ViewNode node, List<AssistStructure.ViewNode> passwordFields) {
        if (node.getAutofillType() == View.AUTOFILL_TYPE_TEXT &&(node.getClassName() != null && (node.getClassName().toLowerCase().contains("edittext")
                || node.getClassName().toLowerCase().contains("textview")))) {
            String hint = node.getHint();
            String viewId = node.getIdEntry();
            if ((hint != null && (hint.toLowerCase().contains("password") || hint.toLowerCase().contains("mật khẩu"))) || (viewId != null && (viewId.contains("password")))) {
                passwordFields.add(node);
                return;
            }
        }
        for (int i = 0; i < node.getChildCount(); i++) {
            identifyPasswordFields(node.getChildAt(i), passwordFields);
        }
    }

    public String getRecentUser(){
        SharedPreferences preferences = getSharedPreferences("username", MODE_PRIVATE);
        String user = preferences.getString("username", "");
        return user;
    }

    protected FillResponse getResponse(String packageName, AutofillId emailID, AutofillId passID) throws PackageManager.NameNotFoundException {
        String username = getRecentUser();
        String password = null;
        UserInfo userInfo = realm.where(UserInfo.class)
                .equalTo("username", username)
                .findFirst();
        try {
            Decryptor decryptor = new Decryptor(userInfo.getUsername(), userInfo.getIv());
            password = decryptor.decryptData(userInfo.getEncryptedPassword());
        } catch (Exception e){
            //Lost the key
            Log.d("Exception signin", "execute: " + e.getMessage() + " " + e.getClass());
            RealmConfiguration configuration = new RealmConfiguration.Builder()
                    .name(username+".realm")
                    .encryptionKey(HashHelper.hexStringToByteArray(HashHelper.get_SHA_512(password, username)))
                    .build();
            try {
                userInfo.setUsername(username);
                try {
                    Encryptor encryptor = new Encryptor(username);
                    userInfo.setIv(encryptor.getIv());
                    userInfo.setEncryptedPassword(encryptor.encryptText(username, password));
                } catch (Exception ex){
                    Log.d("Exception signin", "execute: " + ex.getMessage());
                }
                realm.copyToRealm(userInfo);
            }catch (RealmFileException re){
                e.printStackTrace();
            }
        }
        FillResponse.Builder builder = new FillResponse.Builder();
        if (password != null){

            RealmConfiguration configuration = new RealmConfiguration.Builder()
                    .name(username+".realm")
                    .encryptionKey(HashHelper.hexStringToByteArray(HashHelper.get_SHA_512(password, username)))
                    .build();
            Realm realm = Realm.getInstance(configuration);

            RealmQuery<Login> firstQuery = realm.where(Login.class).in("title", packageName.split("\\."), Case.INSENSITIVE)
                    .or()
                    .in("autofillHint", packageName.split("\\."), Case.INSENSITIVE);

            RealmResults<Login> tempResults = firstQuery.findAll();

            RealmResults<Login> results = tempResults.where()
                    .notEqualTo("username", "")
                    .or().notEqualTo("password", "").findAll();

            if (results.size() > 0){
                for (Login login : results){
                    RemoteViews remoteViews = new RemoteViews(getPackageName(), R.layout.autofill_item);
                    remoteViews.setTextViewText(R.id.tv_title, login.getUsername());
                    URLHelper helper = new URLHelper(getApplicationContext().createPackageContext("com.samsung.ductran.dhpassword", CONTEXT_INCLUDE_CODE));
                    List<URL> urls = helper.initURL();
                    for (URL url : urls){
                        if(login.getUrl().equals(url.getUrl()) || login.getUrl().contains(url.getTitle().toLowerCase())){
                            remoteViews.setImageViewBitmap(R.id.list_image_avatar, BitmapFactory.decodeResource(getResources(), helper.getLogo(url.getName())));
                        }
                    }
                    Dataset dataset = new Dataset.Builder(remoteViews)
                            .setValue(emailID, AutofillValue.forText(login.getUsername()))
                            .setValue(passID, AutofillValue.forText(login.getPassword()))
                            .build();
                    builder.addDataset(dataset);
                }
            }else {
                RemoteViews rv = new RemoteViews(getPackageName(), R.layout.autofill_no_result);
                Dataset.Builder datasetBuilder = new Dataset.Builder(rv);
                datasetBuilder.setValue(emailID, AutofillValue.forText(""))
                        .setValue(passID, AutofillValue.forText(""));
                Dataset dataset = datasetBuilder.build();
                builder.addDataset(dataset);
            }
            realm.close();

            FillResponse response = builder.build();
            return response;
        }else {
            RemoteViews rv = new RemoteViews(getPackageName(), R.layout.autofill_no_result);
            Dataset.Builder datasetBuilder = new Dataset.Builder(rv);
            datasetBuilder.setValue(emailID, AutofillValue.forText(""))
                    .setValue(passID, AutofillValue.forText(""));
            Dataset dataset = datasetBuilder.build();
            builder.addDataset(dataset);

            FillResponse response = builder.build();
            return response;
        }
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        realm.close();
    }
}
