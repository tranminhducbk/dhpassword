package com.samsung.ductran.dhpassword.adapters;

import android.content.ClipData;
import android.content.ClipboardManager;
import android.content.Context;
import android.content.Intent;
import android.icu.text.SimpleDateFormat;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityOptionsCompat;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.chauthai.swipereveallayout.SwipeRevealLayout;
import com.chauthai.swipereveallayout.ViewBinderHelper;
import com.github.siyamed.shapeimageview.CircularImageView;
import com.pnikosis.materialishprogress.ProgressWheel;
import com.samsung.ductran.dhpassword.R;
import com.samsung.ductran.dhpassword.activities.main.MainActivity;
import com.samsung.ductran.dhpassword.activities.password.ViewAccountActivity;
import com.samsung.ductran.dhpassword.model.Login;
import com.samsung.ductran.dhpassword.model.URL;
import com.samsung.ductran.dhpassword.utils.URLHelper;

import java.util.ArrayList;

import io.realm.OrderedRealmCollection;
import io.realm.Realm;
import io.realm.RealmRecyclerViewAdapter;

/**
 * Created by sev_user on 4/12/2018.
 */

public class ListPasswordAdapter extends RealmRecyclerViewAdapter<Login, ListPasswordAdapter.ViewHolder> {

    private final ViewBinderHelper binderHelper = new ViewBinderHelper();
    private Context mContext;
    Realm realm;
    OrderedRealmCollection<Login> dataList;

    boolean clicked =false;

    public void setDataList(OrderedRealmCollection<Login> dataList) {
        this.dataList = dataList;
    }

    //    public ListPasswordAdapter(Context context, List<String> dataSet) {
//        mContext = context;
//        mDataSet = dataSet;
//        mInflater = LayoutInflater.from(context);
//        // uncomment if you want to open only one row at a time
//         binderHelper.setOpenOnlyOne(true);
//    }

    public ListPasswordAdapter(@Nullable OrderedRealmCollection<Login> data) {
        super(data, true);
//        setHasStableIds(true);
        binderHelper.setOpenOnlyOne(true);
        realm = Realm.getDefaultInstance();
        dataList = data;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.listview_item, parent, false);
        mContext = parent.getContext();
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
//        if (0 <= position && position < mDataSet.size()) {
        final Login item = getItem(position);
            // Use ViewBindHelper to restore and save the open/close state of the SwipeRevealView
            // put an unique string id as value, can be any string which uniquely define the data
        binderHelper.bind(holder.swipeLayout, String.valueOf(item.getId()));
            // Bind your data here
        holder.bind(item);
//        }
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public int getItemCount() {
        return dataList.size();
    }

    /**
     * Only if you need to restore open/close state when the orientation is changed. * Call this method in {@link android.app.Activity#onSaveInstanceState(Bundle)}
     */
    public void saveStates(Bundle outState) {
        binderHelper.saveStates(outState);
    }

    /**
     * Only if you need to restore open/close state when the orientation is changed. * Call this method in {@link android.app.Activity#onRestoreInstanceState(Bundle)}
     */
    public void restoreStates(Bundle inState) {
        binderHelper.restoreStates(inState);
    }

    class ViewHolder extends RecyclerView.ViewHolder {
        private SwipeRevealLayout swipeLayout;
        private View frontLayout;
        private View optionLayout;
        private TextView tvTitle;
        private TextView tvDate;
        private CircularImageView avatar;
        private ImageView avatarCard;
        private ImageButton copyButton;
        ProgressWheel progressWheel_loading_icon;
        ArrayList<URL> listURL;
        URLHelper helper;


        public ViewHolder(View itemView) {
            super(itemView);
            swipeLayout = (SwipeRevealLayout) itemView.findViewById(R.id.swipe_layout);
            frontLayout = itemView.findViewById(R.id.front_layout);
            optionLayout = itemView.findViewById(R.id.option_layout);
            tvTitle = (TextView) itemView.findViewById(R.id.tv_title);
            tvDate = itemView.findViewById(R.id.tv_date_created);
            avatar = itemView.findViewById(R.id.list_image_avatar);
            avatarCard = itemView.findViewById(R.id.list_image_card);
            copyButton = itemView.findViewById(R.id.list_btn_copy);
            progressWheel_loading_icon = itemView.findViewById(R.id.progressWheel_loading_icon);

            helper = new URLHelper(mContext);
            listURL = helper.initURL();

            avatarCard.setVisibility(View.VISIBLE);
            avatar.setVisibility(View.GONE);
        }

        public void bind(final Login data) {
            optionLayout.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    optionLayout.setClickable(false);
                    realm.executeTransaction(new Realm.Transaction() {
                        @Override
                        public void execute(Realm realm) {
//                            RealmResults results = realm.where(Login.class).equalTo("id", data.getId()).findAll();
//                            results.deleteAllFromRealm();
                            data.deleteFromRealm();
                            ((MainActivity)mContext).loadDrawerCount();
                            Toast.makeText(mContext, "Delete", Toast.LENGTH_LONG).show();
                        }
                    });

                }
            });
            tvTitle.setText(data.getTitle());
            frontLayout.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if (!clicked){
                        clicked = true;
                        Login login = new Login(data.getId(), data.getUrl(), data.getAutofillHint(), data.getUsername(), data.getPassword(), data.getTitle(), data.getDateCreated());
                        Intent intent = new Intent(mContext, ViewAccountActivity.class);
                        intent.putExtra("account", login);
                        ActivityOptionsCompat options = ActivityOptionsCompat.
                                makeSceneTransitionAnimation((MainActivity)mContext, avatarCard, "listview_item_imageView");
                        mContext.startActivity(intent, options.toBundle());
                        clicked = false;
                    }
                }
            });

            String date = new SimpleDateFormat("yyyy-MM-dd").format(data.getDateCreated());
            tvDate.setText(mContext.getString(R.string.created) + ": " + date);

            copyButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    ClipboardManager clipboard = (ClipboardManager) mContext.getSystemService(Context.CLIPBOARD_SERVICE);
                    ClipData clip = ClipData.newPlainText("Copied", data.getPassword());
                    clipboard.setPrimaryClip(clip);
                    Toast.makeText(mContext, "Copied", Toast.LENGTH_SHORT).show();
                }
            });

            progressWheel_loading_icon.spin();
            if(data.getUrl() != null){
                String dataURL = data.getUrl();
                for (URL url : listURL){
                    if((!dataURL.equals(""))
                            && (dataURL.equals(url.getUrl())
                            || dataURL.contains(url.getTitle().toLowerCase())
                            || url.getUrl().contains(dataURL.toLowerCase()))){
                        Glide.with(mContext).load(helper.getLogo(url.getName()))
                                .into(avatarCard);
                        return;
                    }
                }
            }
            Glide.with(mContext).load(helper.getLogo("browser"))
                    .into(avatarCard);
        }
    }
}