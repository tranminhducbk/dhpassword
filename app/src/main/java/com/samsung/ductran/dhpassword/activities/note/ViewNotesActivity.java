package com.samsung.ductran.dhpassword.activities.note;

import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.drawable.ColorDrawable;
import android.icu.text.SimpleDateFormat;
import android.support.v7.app.ActionBar;
import android.os.Bundle;
import android.text.Spannable;
import android.text.SpannableString;
import android.text.style.ForegroundColorSpan;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.github.siyamed.shapeimageview.CircularImageView;
import com.samsung.ductran.dhpassword.R;
import com.samsung.ductran.dhpassword.activities.main.BaseActivity;
import com.samsung.ductran.dhpassword.model.Note;
import com.samsung.ductran.dhpassword.utils.LetterTileProvider;

import java.io.ByteArrayOutputStream;

import io.realm.Realm;

public class ViewNotesActivity extends BaseActivity {
    LinearLayout view_note;
    TextView view_note_title,view_note_content,view_note_date;
    CircularImageView imageView;
    Note note;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_view_note);
        ActionBar actionBar = getSupportActionBar();
        actionBar.setDisplayOptions(ActionBar.DISPLAY_HOME_AS_UP | ActionBar.DISPLAY_SHOW_TITLE);
        actionBar.setTitle(R.string.view_note_title);
        actionBar.setBackgroundDrawable(new ColorDrawable(getColor(R.color.total_transparent)));
        actionBar.setHomeAsUpIndicator(R.drawable.ic_back);

        Spannable text = new SpannableString(actionBar.getTitle());
        text.setSpan(new ForegroundColorSpan(getColor(R.color.colorPrimary)), 0, text.length(), Spannable.SPAN_INCLUSIVE_INCLUSIVE);
        actionBar.setTitle(text);

        initView();
        Intent intent = getIntent();
        note = (Note) intent.getSerializableExtra("note");
        showNote();
    }

    private void showNote() {
        view_note_title.setText(note.getTitle());
        view_note_date.setText(new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(note.getDateCreated()));
        view_note_content.setText(note.getData());

        Bitmap bitmapAvatar;
        int widthDimens = (int) getResources().getDimension(R.dimen.image_avatar_width);
        LetterTileProvider provider = new LetterTileProvider(this);
        bitmapAvatar = provider.getLetterTile(note.getTitle(), note.getTitle(), widthDimens, widthDimens);

        ByteArrayOutputStream stream = new ByteArrayOutputStream();
        bitmapAvatar.compress(Bitmap.CompressFormat.PNG, 100, stream);
        Glide.with(this).load(stream.toByteArray()).asBitmap().into(imageView);
    }
    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return super.onSupportNavigateUp();
    }
    private void initView() {
        view_note_title = (TextView) findViewById(R.id.view_note_title);
        view_note_date = (TextView) findViewById(R.id.view_note_date);
        view_note_content = (TextView) findViewById(R.id.view_note_content);
        imageView = findViewById(R.id.viewnote_imageView);
    }

    @Override
    protected void onResume() {
        super.onResume();
        Realm realm = Realm.getDefaultInstance();
        note = realm.where(Note.class).equalTo("id", note.getId()).findFirst();
        showNote();
        realm.close();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_edit_item, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        switch (id){
            case R.id.user_info_edit:
                Intent intent = new Intent(ViewNotesActivity.this, AddOrUpdateNoteActivity.class);
                Note newNote = new Note(note.getId(), note.getTitle(), note.getData(), note.getDateCreated());
                intent.putExtra("note", newNote);
                startActivity(intent);
                break;
        }
        return super.onOptionsItemSelected(item);
    }
}
