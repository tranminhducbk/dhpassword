package com.samsung.ductran.dhpassword.activities.userinfo;

import android.app.DatePickerDialog;
import android.content.Intent;
import android.graphics.Bitmap;
import android.icu.text.SimpleDateFormat;
import android.icu.util.Calendar;
import android.net.Uri;
import android.provider.MediaStore;
import android.support.v7.app.ActionBar;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;

import com.github.siyamed.shapeimageview.CircularImageView;
import com.samsung.ductran.dhpassword.R;
import com.samsung.ductran.dhpassword.activities.main.BaseActivity;
import com.samsung.ductran.dhpassword.model.UserInfo;
import com.samsung.ductran.dhpassword.utils.BitmapHelper;
import com.samsung.ductran.dhpassword.utils.RoundImage;

import java.util.Date;

import io.realm.Realm;
import io.realm.RealmConfiguration;

public class EditUserInfoActivity extends BaseActivity {
    private static final int SELECT_PICTURE = 1;

    EditText ed_user_gender, ed_user_age, ed_user_birthday, ed_user_name, ed_user_mobile, ed_user_email, ed_user_address;
    ImageView choose_birthday;
    CircularImageView imageView_avatar;
    Bitmap currentImage;

    UserInfo userInfo;
    Realm realm;

    Date birthday;
    byte[] avatar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit_user_info);

        getWindow().setWindowAnimations(0);

        ActionBar actionBar = getSupportActionBar();
        actionBar.setDisplayOptions(ActionBar.DISPLAY_HOME_AS_UP | ActionBar.DISPLAY_SHOW_TITLE);
        actionBar.setTitle(getString(R.string.edit_user_info_title));

        RealmConfiguration configuration = new RealmConfiguration.Builder()
                .name("UserInformation.realm")
                .build();
        realm = Realm.getInstance(configuration);

        getUserInfo();

        initView();

        showInfo();

    }

    public void getUserInfo(){
        userInfo = (UserInfo) getIntent().getSerializableExtra("user");
        birthday = userInfo.getBirthday();
        avatar = userInfo.getAvatar();
    }

    public void showInfo(){
        if (userInfo != null){
            ed_user_name.setText(userInfo.getName());
            ed_user_gender.setText(userInfo.getGender());
            ed_user_age.setText(userInfo.getAge() == 0? "" : String.valueOf(userInfo.getAge()));
            if(birthday != null) ed_user_birthday.setText(new SimpleDateFormat("dd/MM/yyyy").format(birthday));

            imageView_avatar = (CircularImageView) findViewById(R.id.user_image_avatar);
            imageView_avatar.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent = new Intent();
                    intent.setType("image/*");
                    intent.setAction(Intent.ACTION_GET_CONTENT);
                    startActivityForResult(Intent.createChooser(intent,"Select Picture"), SELECT_PICTURE);
                }
            });
            if (avatar!= null){
                imageView_avatar.setImageBitmap(BitmapHelper.getBitmapFromBytes(avatar));
            }
            ed_user_address.setText(userInfo.getAddress());
            ed_user_email.setText(userInfo.getEmail());
            ed_user_mobile.setText(userInfo.getMobile());
        }
    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return super.onSupportNavigateUp();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.edit_done, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        switch (id){
            case R.id.option_menu_done:
                updateUserInfo();
                finishAfterTransition();
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == RESULT_OK) {
            Uri photoUri = data.getData();
            if (photoUri != null) {
                try {
                    currentImage = MediaStore.Images.Media.getBitmap(this.getContentResolver(), photoUri);
                    imageView_avatar.setImageBitmap(currentImage);
                    avatar = BitmapHelper.compressToBytes(RoundImage.getResizedBitmap(currentImage, 500));
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }
    }

    public void updateUserInfo(){
        String name = ed_user_name.getText().toString();
        String gender = ed_user_gender.getText().toString();
        int age;
        if(ed_user_age.getText().toString().equals(""))age = 0;
        else {
            age = Integer.parseInt(ed_user_age.getText().toString());
        }
        String mobile = ed_user_mobile.getText().toString();
        String email = ed_user_email.getText().toString();
        String address = ed_user_address.getText().toString();

        UserInfo user = new UserInfo(userInfo.getId(), userInfo.getUsername());
        user.setName(name);
        user.setGender(gender);
        user.setAge(age);
        user.setBirthday(birthday);
        user.setMobile(mobile);
        user.setEmail(email);
        user.setAddress(address);

        user.setEncryptedPassword(userInfo.getEncryptedPassword());
        user.setIv(userInfo.getIv());
        user.setAvatar(avatar);

        realm.executeTransaction(new Realm.Transaction() {
            @Override
            public void execute(Realm realm) {
                realm.copyToRealmOrUpdate(user);
            }
        });
    }

    public void initView(){
        ed_user_age = (EditText) findViewById(R.id.edit_user_info_age);
        ed_user_gender = (EditText) findViewById(R.id.edit_user_info_gender);
        ed_user_birthday = (EditText) findViewById(R.id.edit_user_info_birthday);
        ed_user_email = findViewById(R.id.edit_user_info_email);
        ed_user_mobile = findViewById(R.id.edit_user_info_mobile);
        ed_user_name = findViewById(R.id.edit_user_name);
        ed_user_address = findViewById(R.id.edit_user_info_address);
        choose_birthday = findViewById(R.id.choose_birthday);

        choose_birthday.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                showDatePicker();
            }
        });
    }

    public void showDatePicker(){
        final Calendar c = Calendar.getInstance();
        int mYear = c.get(Calendar.YEAR);
        int mMonth = c.get(Calendar.MONTH);
        int mDay = c.get(Calendar.DAY_OF_MONTH);
        DatePickerDialog datePickerDialog = new DatePickerDialog(EditUserInfoActivity.this,
                new DatePickerDialog.OnDateSetListener() {
                    @Override
                    public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
                        String date = dayOfMonth+ "/" + (monthOfYear + 1) + "/" + year;
                        Calendar calendar = Calendar.getInstance();
                        calendar.set(year, monthOfYear, dayOfMonth, 0, 0);
                        birthday = calendar.getTime();
                        ed_user_birthday.setText(date);
                        int age = mYear - year;
                        ed_user_age.setText(age <= 0? "" : String.valueOf(age));
                    }
                }, mYear, mMonth, mDay);
        datePickerDialog.show();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        realm.close();
    }
}
