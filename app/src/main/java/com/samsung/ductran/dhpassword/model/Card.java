package com.samsung.ductran.dhpassword.model;

import java.io.Serializable;
import java.util.Date;

import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;

public class Card extends RealmObject implements Serializable {
    @PrimaryKey
    int id;
    String title;
    String cardHolder;
    String type;
    String cardNumber;
    Date expirationDate;
    Date validFrom;
    Date dateCreated;
    String pin;
    String cvv;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getCardHolder() {
        return cardHolder;
    }

    public void setCardHolder(String cardHolder) {
        this.cardHolder = cardHolder;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getCardNumber() {
        return cardNumber;
    }

    public void setCardNumber(String cardNumber) {
        this.cardNumber = cardNumber;
    }

    public Date getExpirationDate() {
        return expirationDate;
    }

    public void setExpirationDate(Date expirationDate) {
        this.expirationDate = expirationDate;
    }

    public Date getDateCreated() {
        return dateCreated;
    }

    public void setDateCreated(Date dateCreated) {
        this.dateCreated = dateCreated;
    }

    public String getPin() {
        return pin;
    }

    public void setPin(String pin) {
        this.pin = pin;
    }

    public String getCvv() {
        return cvv;
    }

    public void setCvv(String cvv) {
        this.cvv = cvv;
    }

    public Date getValidFrom() {
        return validFrom;
    }

    public void setValidFrom(Date validFrom) {
        this.validFrom = validFrom;
    }

    public Card(int id, String title, String cardHolder, String type, String cardNumber, Date expirationDate, Date validFrom, Date dateCreated, String pin, String cvv) {
        this.id = id;
        this.title = title;
        this.cardHolder = cardHolder;
        this.type = type;
        this.cardNumber = cardNumber;
        this.expirationDate = expirationDate;
        this.validFrom = validFrom;
        this.dateCreated = dateCreated;
        this.pin = pin;
        this.cvv = cvv;
    }

    public Card(){

    }
}
