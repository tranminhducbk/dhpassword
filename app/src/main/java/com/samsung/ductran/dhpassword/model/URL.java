package com.samsung.ductran.dhpassword.model;

import com.google.gson.annotations.SerializedName;

import io.realm.RealmObject;

/**
 * Created by sev_user on 4/18/2018.
 */

public class URL {

    @SerializedName("name")
    private String name;

    @SerializedName("url")
    private String url;

    @SerializedName("title")
    private String title;

    @SerializedName("login_url")
    private String login_url;

    public URL() {
    }

    public URL(String name, String url, String title) {
        this.name = name;
        this.url = url;
        this.title = title;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getLogin_url() {
        return login_url;
    }

    public void setLogin_url(String login_url) {
        this.login_url = login_url;
    }
}
