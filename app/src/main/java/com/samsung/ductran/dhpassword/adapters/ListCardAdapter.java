package com.samsung.ductran.dhpassword.adapters;

import android.content.Context;
import android.content.Intent;
import android.content.res.Resources;
import android.icu.text.SimpleDateFormat;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityOptionsCompat;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.chauthai.swipereveallayout.SwipeRevealLayout;
import com.chauthai.swipereveallayout.ViewBinderHelper;
import com.github.siyamed.shapeimageview.CircularImageView;
import com.samsung.ductran.dhpassword.R;
import com.samsung.ductran.dhpassword.activities.main.MainActivity;
import com.samsung.ductran.dhpassword.activities.card.ViewCardActivity;
import com.samsung.ductran.dhpassword.model.Card;

import io.realm.OrderedRealmCollection;
import io.realm.Realm;
import io.realm.RealmRecyclerViewAdapter;

/**
 * Created by Duc Tran on 5/1/2018.
 */

public class ListCardAdapter extends RealmRecyclerViewAdapter<Card, ListCardAdapter.ViewHolder> {

    private final ViewBinderHelper binderHelper = new ViewBinderHelper();
    private Context mContext;
    Realm realm;
    OrderedRealmCollection<Card> dataList;

    public ListCardAdapter(@Nullable OrderedRealmCollection<Card> data) {
        super(data, true);
//        setHasStableIds(true);
        binderHelper.setOpenOnlyOne(true);
        realm = Realm.getDefaultInstance();
        dataList = data;
    }

    @Override
    public ListCardAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.listview_item, parent, false);
        mContext = parent.getContext();
        return new ListCardAdapter.ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ListCardAdapter.ViewHolder holder, int position) {
//        if (0 <= position && position < mDataSet.size()) {
        final Card item = getItem(position);
        // Use ViewBindHelper to restore and save the open/close state of the SwipeRevealView
        // put an unique string id as value, can be any string which uniquely define the data
        binderHelper.bind(holder.swipeLayout, String.valueOf(item.getId()));
        // Bind your data here
        holder.bind(item);
//        }
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public int getItemCount() {
        return dataList.size();
    }

    class ViewHolder extends RecyclerView.ViewHolder {
        private SwipeRevealLayout swipeLayout;
        private View frontLayout;
        private View optionLayout;
        private TextView tvTitle;
        private TextView tvDate;
        private CircularImageView avatar;
        private ImageView avatarCard;
        private ImageButton copyButton;


        public ViewHolder(View itemView) {
            super(itemView);
            swipeLayout = (SwipeRevealLayout) itemView.findViewById(R.id.swipe_layout);
            frontLayout = itemView.findViewById(R.id.front_layout);
            optionLayout = itemView.findViewById(R.id.option_layout);
            tvTitle = (TextView) itemView.findViewById(R.id.tv_title);
            tvDate = itemView.findViewById(R.id.tv_date_created);
            avatar = itemView.findViewById(R.id.list_image_avatar);
            avatarCard = itemView.findViewById(R.id.list_image_card);
            copyButton = itemView.findViewById(R.id.list_btn_copy);

            avatarCard.setVisibility(View.VISIBLE);
            avatar.setVisibility(View.GONE);
        }

        public void bind(final Card data) {
            optionLayout.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    realm.executeTransaction(new Realm.Transaction() {
                        @Override
                        public void execute(Realm realm) {
    //                            RealmResults results = realm.where(Login.class).equalTo("id", data.getId()).findAll();
    //                            results.deleteAllFromRealm();
                            data.deleteFromRealm();
                            Toast.makeText(mContext, "Delete", Toast.LENGTH_LONG).show();
                            ((MainActivity)mContext).loadDrawerCount();
                        }
                    });

                }
            });

            String date = new SimpleDateFormat("yyyy-MM-dd").format(data.getDateCreated());
            tvDate.setText(mContext.getString(R.string.created) + ": " + date);
            tvTitle.setText(data.getTitle());
            copyButton.setVisibility(View.GONE);

            String type = data.getType();
            String nameLogo = null;
            if (type.equals("Diners Club")
                    ||type.equals("JCB")
                    ||type.equals("Maestro")
                    ||type.equals("Solo")
                    ||type.equals("Switch")
                    ||type.equals("Visa Electron")){
                nameLogo = "credit_card";
            }else if(type.equals("American Express")){
                nameLogo = "amex";
            }else if(type.equals("Discover")){
                nameLogo = "discover";
            }else if ((type.equals("Mastercard"))){
                nameLogo = "mastercard";
            }else if(type.equals("Visa")){
                nameLogo = "visa";
            }else if(type.equals(mContext.getResources().getString(R.string.none))){
                nameLogo = "logo_card";
            }
            Glide.with(mContext).load(getLogo(nameLogo)).asBitmap().into(avatarCard);

            frontLayout.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Card card = new Card(data.getId(), data.getTitle(), data.getCardHolder(), data.getType(), data.getCardNumber(), data.getExpirationDate()
                                        , data.getValidFrom(),data.getDateCreated(), data.getPin(), data.getCvv());
                    Intent intent = new Intent(mContext, ViewCardActivity.class);
                    intent.putExtra("card", card);
                    ActivityOptionsCompat options = ActivityOptionsCompat.
                            makeSceneTransitionAnimation((MainActivity)mContext, avatarCard, "listview_item_imageView");
                    mContext.startActivity(intent, options.toBundle());
                }
            });
        }

        public int getLogo(String name){
            Resources resources = mContext.getResources();
            final int resourceId = resources.getIdentifier(name, "drawable",
                    mContext.getPackageName());
            return resourceId;
        }
    }
}
