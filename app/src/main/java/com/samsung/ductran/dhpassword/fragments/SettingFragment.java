package com.samsung.ductran.dhpassword.fragments;

import android.annotation.TargetApi;
import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.Build;
import android.provider.Settings;
import android.support.v4.app.Fragment;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.autofill.AutofillManager;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.Toast;

import com.samsung.ductran.dhpassword.DHpassword;
import com.samsung.ductran.dhpassword.R;
import com.samsung.ductran.dhpassword.activities.other.ChangeMasterPasswordActivity;
import com.samsung.ductran.dhpassword.activities.main.MainActivity;

import cn.pedant.SweetAlert.SweetAlertDialog;

import static android.app.Activity.RESULT_OK;

public class SettingFragment extends Fragment {
    Switch switch_fingerprint_unlock, switch_fingerprint_unlock_item, switch_auto_login_app, switch_auto_lock_app;
    TextView tv_Autofill_not_supported;
    LinearLayout autofill_setting_layout, fingerprint_setting_layout, applock_setting_layout;
    LinearLayout change_master_password_layout;
    LinearLayout delete_data_layout;

    AutofillManager mAutofillManager;

    private static int REQUEST_CODE_SET_DEFAULT = 1;

    Activity activity;
    String username;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            username = getArguments().getString("username");
        }
        activity = (MainActivity)getActivity();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_setting, container, false);
        switch_fingerprint_unlock = (Switch) view.findViewById(R.id.switch_fingerprint_unlock);
        switch_fingerprint_unlock_item = (Switch) view.findViewById(R.id.switch_fingerprint_unlock_item);
        switch_auto_login_app = (Switch) view.findViewById(R.id.switch_auto_login_app);
        switch_auto_lock_app = view.findViewById(R.id.switch_auto_lock_app);
        tv_Autofill_not_supported = view.findViewById(R.id.tv_autofill_not_supported);

        change_master_password_layout = view.findViewById(R.id.change_master_password_layout);
        change_master_password_layout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(getActivity(), ChangeMasterPasswordActivity.class);
                intent.putExtra("username", username);
                intent.putExtra("password", ((MainActivity)activity).getMasterPassword());
                startActivity(intent);
            }
        });

        fingerprint_setting_layout = view.findViewById(R.id.fingerprint_setting_layout);
        fingerprint_setting_layout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                switch_fingerprint_unlock.setChecked(!switch_fingerprint_unlock.isChecked());
            }
        });

        applock_setting_layout = view.findViewById(R.id.applock_setting_layout);
        applock_setting_layout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                switch_auto_lock_app.setChecked(!switch_auto_lock_app.isChecked());
            }
        });

        delete_data_layout = view.findViewById(R.id.delete_data_layout);
        delete_data_layout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                showDeleteWarningDialog();
            }
        });

        autofill_setting_layout = view.findViewById(R.id.autofill_setting_layout);
        autofill_setting_layout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                switch_auto_login_app.setChecked(!switch_auto_login_app.isChecked());
            }
        });

        switch_fingerprint_unlock.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                saveSetting(username+ "_" + getString(R.string.setting_fingerprint_unlock), isChecked);

//                View customView = getActivity().getLayoutInflater().inflate(R.layout.layout_input_restore_password, null);
//                EditText edt_pass_to_unlock = customView.findViewById(R.id.restore_edt_password);
//                AlertDialog dialog = new AlertDialog.Builder(getActivity())
//                        .setView(customView)
//                        .setTitle("Input your master password to verify")
//                        .setCancelable(false)
//                        .setPositiveButton("OK", new DialogInterface.OnClickListener() {
//                            @Override
//                            public void onClick(DialogInterface dialog, int which) {
//                                String passwordToUnlock = edt_pass_to_unlock.getText().toString();
//                                if (passwordToUnlock == null || passwordToUnlock.equals(""))edt_pass_to_unlock.setError(getString(R.string.empty));
//                                else if(passwordToUnlock.equals(((MainActivity)activity).getMasterPassword())) {
//                                    dialog.dismiss();
//                                    saveSetting(username+ "_" + getString(R.string.setting_fingerprint_unlock), isChecked);
//                                }else {
//                                    Toast.makeText(getActivity() ,getString(R.string.wrong_master_password_error), Toast.LENGTH_LONG).show();
//                                }
//                                switch_fingerprint_unlock.setChecked(getSetting(username+ "_" + getString(R.string.setting_fingerprint_unlock)));
//                            }
//                        })
//                        .setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
//                            @Override
//                            public void onClick(DialogInterface dialog, int which) {
//                                switch_fingerprint_unlock.setChecked(getSetting(username+ "_" + getString(R.string.setting_fingerprint_unlock)));
//                                dialog.dismiss();
//                            }
//                        })
//                        .create();
//                dialog.show();
            }
        });


        switch_fingerprint_unlock_item.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                saveSetting(username+ "_" + getString(R.string.setting_fingerprint_unlock_item), isChecked);
            }
        });


        switch_auto_login_app.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if(isChecked){
                    enableAutofillSetting();
                }else {
                    disableAutofillService();
                }
            }
        });


        switch_auto_lock_app.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                saveSetting(username+ "_" + getString(R.string.auto_lock_app), b);
            }
        });

        switch_fingerprint_unlock.setChecked(getSetting(username+ "_" + getString(R.string.setting_fingerprint_unlock)));
        switch_fingerprint_unlock_item.setChecked(getSetting(username+ "_" + getString(R.string.setting_fingerprint_unlock_item)));
        switch_auto_lock_app.setChecked(getSetting(username+ "_" + getString(R.string.auto_lock_app)));
        showhideAutoFillSetting();
        return view;
    }

    public void showDeleteWarningDialog(){
        SweetAlertDialog dialog = new SweetAlertDialog(activity, SweetAlertDialog.NORMAL_TYPE)
                .setTitleText(getString(R.string.warning_title))
                .setContentText(getString(R.string.delete_data_warning))
                .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                    @Override
                    public void onClick(SweetAlertDialog sweetAlertDialog) {
                        DHpassword.getInstance().clearApplicationData();
                        activity.finishAffinity();
                        System.exit(0);
                    }
                })
                .setCancelClickListener(new SweetAlertDialog.OnSweetClickListener() {
                    @Override
                    public void onClick(SweetAlertDialog sweetAlertDialog) {
                        sweetAlertDialog.dismiss();
                    }
                })
                .setConfirmText(getString(R.string.dialog_confirm_text))
                .setCancelText(getString(R.string.dialog_cancel_text))
                .showCancelButton(true);
        dialog.setCancelable(true);
        dialog.setCanceledOnTouchOutside(true);
        dialog.show();
    }

    public void showhideAutoFillSetting(){
        if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.O){
            mAutofillManager = getActivity().getSystemService(AutofillManager.class);
            autofill_setting_layout.setVisibility(View.VISIBLE);
            tv_Autofill_not_supported.setVisibility(View.GONE);
            switch_auto_login_app.setChecked(mAutofillManager.hasEnabledAutofillServices());
        }else {
            autofill_setting_layout.setVisibility(View.GONE);
            tv_Autofill_not_supported.setVisibility(View.VISIBLE);
        }
    }

    public SettingFragment() {
        // Required empty public constructor
    }

    public void saveSetting(String name, boolean value){
        Activity activity = getActivity();
        SharedPreferences.Editor editor = activity.getSharedPreferences(name, Context.MODE_PRIVATE).edit();
        editor.putBoolean(name, value);
        editor.commit();
    }

    public boolean getSetting(String name){
        Activity activity = getActivity();
        SharedPreferences sharedPreferences = activity.getSharedPreferences(name, Context.MODE_PRIVATE);
        return sharedPreferences.getBoolean(name, false);
    }

    @TargetApi(Build.VERSION_CODES.O)
    public void enableAutofillSetting(){
        if (mAutofillManager != null && !mAutofillManager.hasEnabledAutofillServices()) {
            Intent intent = new Intent(Settings.ACTION_REQUEST_SET_AUTOFILL_SERVICE);
            intent.setData(Uri.parse("package:com.samsung.ductran.dhpassword"));
            startActivityForResult(intent, REQUEST_CODE_SET_DEFAULT);
        } else {
            Log.d("TAG","Autofill service already enabled.");
        }
    }

    @TargetApi(Build.VERSION_CODES.O)
    private void disableAutofillService() {
        if (mAutofillManager != null && mAutofillManager.hasEnabledAutofillServices()) {
            mAutofillManager.disableAutofillServices();
        } else {
            Log.d("TAG", "Autofill service already disabled.");
        }
    }

    @TargetApi(Build.VERSION_CODES.O)
    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if(requestCode == REQUEST_CODE_SET_DEFAULT){
            if(resultCode == RESULT_OK && mAutofillManager.isEnabled()){
                switch_auto_login_app.setChecked(true);
            }else {
                switch_auto_login_app.setChecked(false);
            }
        }
    }
}
