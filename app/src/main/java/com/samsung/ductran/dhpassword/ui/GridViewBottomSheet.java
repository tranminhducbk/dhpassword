package com.samsung.ductran.dhpassword.ui;

import android.annotation.SuppressLint;
import android.app.Dialog;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomSheetBehavior;
import android.support.design.widget.BottomSheetDialogFragment;
import android.support.design.widget.CoordinatorLayout;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.GridView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.samsung.ductran.dhpassword.R;
import com.samsung.ductran.dhpassword.activities.password.AddOrUpdatePasswordActivity;
import com.samsung.ductran.dhpassword.adapters.GridViewSiteAdapter;
import com.samsung.ductran.dhpassword.utils.URLHelper;
import com.samsung.ductran.dhpassword.model.URL;

import java.util.ArrayList;

/**
 * Created by Duc Tran on 4/28/2018.
 */

public class GridViewBottomSheet extends BottomSheetDialogFragment {
    public BottomSheetBehavior.BottomSheetCallback callback;
    TextView tvCancel;
    GridView login_gridview;
    ArrayAdapter adapter;
    URLHelper helper;

    public GridViewBottomSheet(){
        callback = new BottomSheetBehavior.BottomSheetCallback() {
            @Override
            public void onStateChanged(@NonNull View bottomSheet, int newState) {
                if (newState == BottomSheetBehavior.STATE_HIDDEN){
                    dismiss();
                }
            }

            @Override
            public void onSlide(@NonNull View bottomSheet, float slideOffset) {

            }
        };
    }

    @SuppressLint("RestrictedApi")
    @Override
    public void setupDialog(Dialog dialog, int style) {
        super.setupDialog(dialog, style);
        View view = View.inflate(getContext(), R.layout.add_password_gridview_bottom_sheet, null);
        dialog.setContentView(view);

        helper = new URLHelper(getActivity());

        tvCancel = (TextView) view.findViewById(R.id.item_detail_btnCancel);

        tvCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dismiss();
            }
        });

        login_gridview =(GridView)view.findViewById(R.id.ed_login_gridview);
        initGridView();

        CoordinatorLayout.LayoutParams params = (CoordinatorLayout.LayoutParams) ((View) view.getParent()).getLayoutParams();
        CoordinatorLayout.Behavior behavior = params.getBehavior();

        if( behavior != null && behavior instanceof BottomSheetBehavior ) {
            ((BottomSheetBehavior) behavior).setBottomSheetCallback(callback);
            ((BottomSheetBehavior) behavior).setHideable(false);
        }
    }

    public void initGridView(){
        ArrayList<URL> urls = helper.initURL();

        adapter = new GridViewSiteAdapter(getActivity(), R.layout.item_login_gridview, urls);
        login_gridview.setAdapter(adapter);
        login_gridview.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                AddOrUpdatePasswordActivity activity = (AddOrUpdatePasswordActivity)getActivity();
                if(activity.getEd_title().getText().toString().equals(""))activity.getEd_title().setText(urls.get(i).getTitle());
                activity.getEd_url().setText(urls.get(i).getUrl());
                Glide.with(getActivity()).load(helper.getLogo(urls.get(i).getName())).into(activity.getSiteLogo());
                dismiss();
            }
        });
    }
}
