package com.samsung.ductran.dhpassword.autofill;

import android.annotation.TargetApi;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.drawable.Animatable2;
import android.graphics.drawable.AnimatedVectorDrawable;
import android.graphics.drawable.Drawable;
import android.hardware.fingerprint.FingerprintManager;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.service.autofill.Dataset;
import android.service.autofill.FillResponse;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.autofill.AutofillId;
import android.view.autofill.AutofillValue;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.RemoteViews;
import android.widget.TextView;

import com.samsung.ductran.dhpassword.R;
import com.samsung.ductran.dhpassword.model.Login;
import com.samsung.ductran.dhpassword.model.URL;
import com.samsung.ductran.dhpassword.model.UserInfo;
import com.samsung.ductran.dhpassword.utils.Decryptor;
import com.samsung.ductran.dhpassword.utils.Encryptor;
import com.samsung.ductran.dhpassword.utils.HashHelper;
import com.samsung.ductran.dhpassword.utils.URLHelper;

import java.io.IOException;
import java.security.InvalidAlgorithmParameterException;
import java.security.InvalidKeyException;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.UnrecoverableEntryException;
import java.security.cert.CertificateException;
import java.util.List;

import javax.crypto.NoSuchPaddingException;

import cn.pedant.SweetAlert.SweetAlertDialog;
import io.realm.Case;
import io.realm.Realm;
import io.realm.RealmConfiguration;
import io.realm.RealmQuery;
import io.realm.RealmResults;
import io.realm.exceptions.RealmFileException;

import static android.view.autofill.AutofillManager.EXTRA_AUTHENTICATION_RESULT;

/**
 * Created by Duc Tran on 5/11/2018.
 */

@TargetApi(Build.VERSION_CODES.O)
public class AutofillUnlockActivity extends AppCompatActivity {

    String username;
    String password;
    String packageName;
    String datasetName;
    AutofillId emailID;
    AutofillId passID;

    Realm realm;

    LinearLayout password_layout;
    RelativeLayout fingerprint_layout;
    ImageView login_fingerprint_button;
    TextView login_use_password;
    TextView tv_fingerprint_notice;
    ImageView autofill_password_login;
    EditText password_edittext;

    AnimatedVectorDrawable avd;

    boolean isFingerprintEnabled;

    FingerprintAutofillHandler helper;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.requestWindowFeature(Window.FEATURE_NO_TITLE);

        Intent intent = getIntent();

        // The data sent by the service and the structure are included in the intent.
        username = intent.getStringExtra("username");

        emailID = intent.getParcelableExtra("emailFieldID");
        passID = intent.getParcelableExtra("passwordFieldID");
        packageName = intent.getStringExtra("packageName");
        datasetName = intent.getStringExtra("MY_EXTRA_DATASET_NAME");

        setContentView(R.layout.autofill_dialog_unlock);

        Realm.init(this);
        RealmConfiguration configuration = new RealmConfiguration.Builder()
                .name("UserInformation.realm")
                .build();
        realm = Realm.getInstance(configuration);

        initView();
        showHideFingerprint();
    }

    protected void initView(){
        password_layout = findViewById(R.id.password_layout);
        fingerprint_layout = findViewById(R.id.fingerprint_layout);
        login_fingerprint_button = findViewById(R.id.button_login_fingerprint);
        tv_fingerprint_notice = findViewById(R.id.txt_fingerprint_notification);
        login_use_password = findViewById(R.id.txt_fingerprint_use_password);
        login_use_password.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                FingerprintAutofillHandler helper = new FingerprintAutofillHandler(AutofillUnlockActivity.this);
                helper.stopAuth();
                password_layout.setVisibility(View.VISIBLE);
                fingerprint_layout.setVisibility(View.GONE);
            }
        });
        autofill_password_login = findViewById(R.id.autofill_password_login);
        autofill_password_login.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                authenticateUsingPassword();
            }
        });
        password_edittext = findViewById(R.id.password_edittext);
    }

    protected void showHideFingerprint(){
        SharedPreferences preferences = getSharedPreferences(username + "_" + getString(R.string.setting_fingerprint_unlock), MODE_PRIVATE);
        isFingerprintEnabled = preferences.getBoolean(username + "_" + getString(R.string.setting_fingerprint_unlock), false);
        if(isFingerprintEnabled){
            fingerprint_layout.setVisibility(View.VISIBLE);
            password_layout.setVisibility(View.GONE);
            showLoginFingerPrint();
            showLoginFingerprintScanning();
            startFingerprintAuthentication();
        }else {
            fingerprint_layout.setVisibility(View.GONE);
            password_layout.setVisibility(View.VISIBLE);
        }
    }

    public void startFingerprintAuthentication(){
        showLoginFingerprintScanning();
        RealmResults<UserInfo> results = realm.where(UserInfo.class).equalTo("username", username).findAll();
        UserInfo userInfo = results.first();
        try {
            Decryptor decryptor = new Decryptor(userInfo.getUsername(), userInfo.getIv());
            FingerprintManager.CryptoObject cryptoObject = new FingerprintManager.CryptoObject(decryptor.getCipher());
            helper = new FingerprintAutofillHandler(AutofillUnlockActivity.this);
            helper.startAuth(cryptoObject);
        } catch (CertificateException e) {
            e.printStackTrace();
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        } catch (KeyStoreException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (NoSuchPaddingException e) {
            e.printStackTrace();
        } catch (UnrecoverableEntryException e) {
            e.printStackTrace();
        } catch (InvalidAlgorithmParameterException e) {
            e.printStackTrace();
        } catch (InvalidKeyException e) {
            e.printStackTrace();
        }
    }

    public void showLoginFingerprintFail(){
        tv_fingerprint_notice.setText(R.string.fingerprint_no_match);
        login_fingerprint_button.setImageDrawable(getDrawable(R.drawable.fingerprint_to_cross));
        Drawable don = login_fingerprint_button.getDrawable();
        if (don instanceof AnimatedVectorDrawable){
            avd = (AnimatedVectorDrawable)don;
            avd.start();
        }
    }

    public void showLoginFingerprintScanning(){
        tv_fingerprint_notice.setText(R.string.place_finger_on_the_sensor);
        login_fingerprint_button.setImageDrawable(getDrawable(R.drawable.scan_fingerprint));
        Drawable don = login_fingerprint_button.getDrawable();
        if (don instanceof AnimatedVectorDrawable){
            avd = (AnimatedVectorDrawable)don;
            avd.registerAnimationCallback(new Animatable2.AnimationCallback() {
                @Override
                public void onAnimationEnd(Drawable drawable) {
                    avd.start();
                }
            });
            avd.start();
        }
    }

    public void showLoginFingerPrint(){
        tv_fingerprint_notice.setText(R.string.place_finger_on_the_sensor);
        login_fingerprint_button.setImageDrawable(getDrawable(R.drawable.show_fingerprint));
        Drawable don = login_fingerprint_button.getDrawable();
        if (don instanceof AnimatedVectorDrawable){
            avd = (AnimatedVectorDrawable)don;
            avd.start();
        }

    }

    public void showLoginFingerprintPass(){
        tv_fingerprint_notice.setText(R.string.fingerprint_pass);
        login_fingerprint_button.setImageDrawable(getDrawable(R.drawable.fingerprint_to_tick));
        Drawable don = login_fingerprint_button.getDrawable();
        if (don instanceof AnimatedVectorDrawable){
            avd = (AnimatedVectorDrawable)don;
            avd.start();
        }
    }

    public void showWrongPasswordError(){
        new SweetAlertDialog(this, SweetAlertDialog.ERROR_TYPE)
                .setContentText(getString(R.string.wrong_master_password_error))
                .setTitleText(getString(R.string.error_dialog_title))
                .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                    @Override
                    public void onClick(SweetAlertDialog sweetAlertDialog) {
                        sweetAlertDialog.dismiss();
                    }
                })
                .show();
    }

    public void showSomeError(){
        new SweetAlertDialog(this, SweetAlertDialog.ERROR_TYPE)
                .setContentText("Some errors happend\nPlease log in using password")
                .setTitleText(getString(R.string.error_dialog_title))
                .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                    @Override
                    public void onClick(SweetAlertDialog sweetAlertDialog) {
                        sweetAlertDialog.dismiss();
                    }
                })
                .show();
    }

    public RealmResults<UserInfo> checkUser(Realm realm, String username){
        RealmResults<UserInfo> results = realm.where(UserInfo.class)
                .equalTo("username", username)
                .findAll();
        return results;
    }

    protected void authenticateUsingPassword(){
        final int[] isSuccess = new int[]{-1}; //0: user not exist, 1: wrong password, 2: success
        final String[] masterPassword = new String[1];
        String pass = password_edittext.getText().toString();
        Runnable signinRunnable = new Runnable() {
            @Override
            public void run() {
                realm.executeTransactionAsync(new Realm.Transaction() {
                    @Override
                    public void execute(Realm realm) {
                        RealmResults<UserInfo> results = checkUser(realm, username);
                        UserInfo userInfo = results.first();
                        try {
                            Decryptor decryptor = new Decryptor(userInfo.getUsername(), userInfo.getIv());
                            masterPassword[0] = decryptor.decryptData(userInfo.getEncryptedPassword());
                            if(!pass.equals(masterPassword[0])){
                                isSuccess[0] = 1;
                            }else {
                                isSuccess[0] = 2;
                            }
                        } catch (Exception e){
                            //Lost the key
                            Log.d("Exception signin", "execute: " + e.getMessage() + " " + e.getClass());
                            RealmConfiguration configuration = new RealmConfiguration.Builder()
                                    .name(username+".realm")
                                    .encryptionKey(HashHelper.hexStringToByteArray(HashHelper.get_SHA_512(pass, username)))
                                    .build();
                            try {
                                Realm recoverRealm = Realm.getInstance(configuration);
                                userInfo.setUsername(username);
                                try {
                                    Encryptor encryptor = new Encryptor(username);
                                    userInfo.setIv(encryptor.getIv());
                                    userInfo.setEncryptedPassword(encryptor.encryptText(username, pass));
                                } catch (Exception ex){
                                    Log.d("Exception signin", "execute: " + ex.getMessage());
                                }
                                realm.copyToRealm(userInfo);
                            }catch (RealmFileException re){
                                isSuccess[0] = 1;
                            }
                        }
                    }
                }, new Realm.Transaction.OnSuccess() {
                    @Override
                    public void onSuccess() {
                        if(isSuccess[0] == 1){
                            showWrongPasswordError();
//                                hideProgressbar();
                        }else {
                            password = masterPassword[0];
                            setResult();
                        }
                    }
                });
            }
        };
        new Handler().postDelayed(signinRunnable, 0);
    }



    protected void setResult(){
        if(isFingerprintEnabled){
            RealmResults<UserInfo> results = checkUser(realm, username);
            UserInfo userInfo = results.first();
            try {
                Decryptor decryptor = new Decryptor(userInfo.getUsername(), userInfo.getIv());
                password = decryptor.decryptData(userInfo.getEncryptedPassword());
            } catch (Exception e){
                //Lost the key
                Log.d("Exception signin", "execute: " + e.getMessage() + " " + e.getClass());
                RealmConfiguration configuration = new RealmConfiguration.Builder()
                        .name(username+".realm")
                        .encryptionKey(HashHelper.hexStringToByteArray(HashHelper.get_SHA_512(password, username)))
                        .build();
                try {
                    userInfo.setUsername(username);
                    try {
                        Encryptor encryptor = new Encryptor(username);
                        userInfo.setIv(encryptor.getIv());
                        userInfo.setEncryptedPassword(encryptor.encryptText(username, password));
                    } catch (Exception ex){
                        Log.d("Exception signin", "execute: " + ex.getMessage());
                    }
                    realm.copyToRealm(userInfo);
                }catch (RealmFileException re){
                    fingerprint_layout.setVisibility(View.GONE);
                    password_layout.setVisibility(View.VISIBLE);
                    helper.stopAuth();
                    showSomeError();
                }
            }
        }
        if (password != null){

            FillResponse.Builder builder = new FillResponse.Builder();

            RealmConfiguration configuration = new RealmConfiguration.Builder()
                    .name(username+".realm")
                    .encryptionKey(HashHelper.hexStringToByteArray(HashHelper.get_SHA_512(password, username)))
                    .build();
            Realm realm = Realm.getInstance(configuration);

            RealmQuery<Login> firstQuery = realm.where(Login.class).in("title", packageName.split("\\."), Case.INSENSITIVE)
                    .or()
                    .in("autofillHint", packageName.split("\\."), Case.INSENSITIVE);

            RealmResults<Login> tempResults = firstQuery.findAll();

            RealmResults<Login> results = tempResults.where()
                    .notEqualTo("username", "")
                    .or().notEqualTo("password", "").findAll();

            if (results.size() > 0){
                for (Login login : results){
                    RemoteViews remoteViews = new RemoteViews(getPackageName(), R.layout.autofill_item);
                    remoteViews.setTextViewText(R.id.tv_title, login.getUsername());
                    URLHelper helper = new URLHelper(this);
                    List<URL> urls = helper.initURL();
                    for (URL url : urls){
                        if(login.getUrl().equals(url.getUrl()) || login.getUrl().contains(url.getTitle().toLowerCase())){
                            remoteViews.setImageViewUri(R.id.list_image_avatar, Uri.parse("android.resource://com.samsung.ductran.dhpassword/" + helper.getLogo(url.getName())));
                            break;
                        }
                    }
                    Dataset dataset = new Dataset.Builder(remoteViews)
                            .setValue(emailID, AutofillValue.forText(login.getUsername()))
                            .setValue(passID, AutofillValue.forText(login.getPassword()))
                            .build();
                    builder.addDataset(dataset);
                }
            }else {
                RemoteViews rv = new RemoteViews(getPackageName(), R.layout.autofill_no_result);
                Dataset.Builder datasetBuilder = new Dataset.Builder(rv);
                datasetBuilder.setValue(emailID, AutofillValue.forText(""))
                        .setValue(passID, AutofillValue.forText(""));
                Dataset dataset = datasetBuilder.build();
                builder.addDataset(dataset);
            }

            FillResponse response = builder.build();
            Intent replyIntent = new Intent();
            replyIntent.putExtra(EXTRA_AUTHENTICATION_RESULT, response);

            setResult(RESULT_OK, replyIntent);

            realm.close();
        }
        finish();
    }
}
