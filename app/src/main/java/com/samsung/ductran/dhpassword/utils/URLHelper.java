package com.samsung.ductran.dhpassword.utils;

import android.app.Activity;
import android.content.Context;
import android.content.res.Resources;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.samsung.ductran.dhpassword.R;
import com.samsung.ductran.dhpassword.model.URL;
import com.samsung.ductran.dhpassword.utils.TextUtils;

import java.io.IOException;
import java.util.ArrayList;

public class URLHelper {

    private Context context;
    private ArrayList<URL> urls;

    public URLHelper(Context mContext){
        context = mContext;
        urls = initURL();
    }

    public ArrayList<URL> initURL() {
        ArrayList<URL> urls = new ArrayList<>();
        try {
            String urltext = TextUtils.readText(context, R.raw.logobrowser);
            urls = new Gson().fromJson(urltext, new TypeToken<ArrayList<URL>>() {
            }.getType());

        } catch (IOException e) {
            e.printStackTrace();
        }
        return urls;
    }

    public int getLogo(String name){
        Resources resources = context.getResources();
        int resourceId;
        if(context instanceof Activity){
            resourceId = resources.getIdentifier(name, "drawable", ((Activity)context).getPackageName());
        }else {
            resourceId = resources.getIdentifier(name, "drawable", "com.samsung.ductran.dhpassword");
        }
        return resourceId;
    }
}
