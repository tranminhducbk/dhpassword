package com.samsung.ductran.dhpassword.adapters;

import android.app.Activity;
import android.content.ClipData;
import android.content.ClipboardManager;
import android.content.Context;
import android.icu.text.SimpleDateFormat;
import android.support.annotation.LayoutRes;
import android.support.annotation.NonNull;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;
import android.widget.Toast;

import com.samsung.ductran.dhpassword.R;
import com.samsung.ductran.dhpassword.model.PasswordGenerated;

import java.util.Date;

import io.realm.RealmResults;

/**
 * Created by sev_user on 4/23/2018.
 */

public class PasswordGeneratedAdapter extends ArrayAdapter<PasswordGenerated> {
    RealmResults<PasswordGenerated> list= null;
    int layout;
    Activity activity;
    public PasswordGeneratedAdapter(@NonNull Activity activity, @LayoutRes int layout, @NonNull RealmResults<PasswordGenerated> list) {
        super(activity, layout, list);
        this.layout = layout;
        this.list = list;
        this.activity = activity;
    }
    public View getView(final int position, View view, ViewGroup parent) {
        LayoutInflater inflater = activity.getLayoutInflater();
        view = inflater.inflate(layout, null);
        TextView txt_password_generate = (TextView) view.findViewById(R.id.txt_password_generate);
        TextView txt_date_password_generate = (TextView) view.findViewById(R.id.txt_date_password_generate);

        txt_password_generate.setText(list.get(position).getPasswordgenerate());
        Date getdate = list.get(position).getDate();
        final String date = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(getdate);
        txt_date_password_generate.setText(date);
        TextView copy_generate = (TextView)view.findViewById(R.id.copy_generate);
            copy_generate.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    ClipboardManager clipboard = (ClipboardManager) activity.getSystemService(Context.CLIPBOARD_SERVICE);
                    ClipData clip = ClipData.newPlainText("Copied",list.get(position).getPasswordgenerate());
                    clipboard.setPrimaryClip(clip);
                    Toast.makeText(activity, "Copied", Toast.LENGTH_SHORT).show();
                }
            });
        return view;
    }
}
