package com.samsung.ductran.dhpassword.activities.card;

import android.app.DatePickerDialog;
import android.app.Dialog;
import android.content.Intent;
import android.content.res.Resources;
import android.icu.text.SimpleDateFormat;
import android.icu.util.Calendar;
import android.support.v7.app.ActionBar;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.bumptech.glide.Glide;
import com.samsung.ductran.dhpassword.R;
import com.samsung.ductran.dhpassword.activities.main.BaseActivity;
import com.samsung.ductran.dhpassword.model.Card;

import java.util.Date;
import java.util.UUID;

import cn.pedant.SweetAlert.SweetAlertDialog;
import io.realm.Realm;


public class AddOrUpdateCreditCardActivity extends BaseActivity implements View.OnClickListener {
    ImageView expiry_date,valid_from,type_more, cardLogo;
    EditText ed_card_expiry_date,ed_card_valid_from, ed_card_type,ed_card_title,ed_card_name,
            ed_card_number,ed_card_cvv,ed_card_pin;
    LinearLayout item_card_american,item_card_diners_club,item_card_discover,item_card_jcb,
            item_card_maestro,item_card_mastercard,item_card_solo,item_card_switch,
            item_card_visa,item_card_visa_electron;
    private int mYear, mMonth, mDay;
    Dialog dialog;
    String date;

    Date validFrom, validUtil;

    Card card;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_card);

        ActionBar actionBar = getSupportActionBar();
        actionBar.setDisplayOptions(ActionBar.DISPLAY_HOME_AS_UP | ActionBar.DISPLAY_SHOW_TITLE);
        actionBar.setTitle(R.string.add_credit_card_title);

        initView();
        //set date picker dialog
        expiry_date.setOnClickListener(this);
        valid_from.setOnClickListener(this);
        //show dialog
        type_more.setOnClickListener(this);

        Intent intent = getIntent();
        card = (Card) intent.getSerializableExtra("card");
        if (card!= null){
            showCard();
        }
    }

    public void showCard(){
        ed_card_title.setText(card.getTitle());
        ed_card_number.setText(card.getCardNumber());
        ed_card_type.setText(card.getType());
        ed_card_cvv.setText(card.getCvv());
        ed_card_name.setText(card.getCardHolder());
        ed_card_pin.setText(card.getPin());
        validFrom = card.getValidFrom();
        validUtil = card.getExpirationDate();
        if(validUtil!= null){
            ed_card_expiry_date.setText(new SimpleDateFormat("MM/yyyy").format(validUtil));
        }
        if(validFrom!=null){
            ed_card_valid_from.setText(new SimpleDateFormat("MM/yyyy").format(validFrom));
        }

        String type = card.getType();
        String nameLogo = null;
        if (type.equals("Diners Club")
                ||type.equals("JCB")
                ||type.equals("Maestro")
                ||type.equals("Solo")
                ||type.equals("Switch")
                ||type.equals("Visa Electron")){
            nameLogo = "credit_card";
        }else if(type.equals("American Express")){
            nameLogo = "amex";
        }else if(type.equals("Discover")){
            nameLogo = "discover";
        }else if ((type.equals("Mastercard"))){
            nameLogo = "mastercard";
        }else if(type.equals("Visa")){
            nameLogo = "visa";
        }else if(type.equals(getResources().getString(R.string.none))){
            nameLogo = "logo_card";
        }

        setLogoCard(nameLogo);
    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return super.onSupportNavigateUp();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.edit_done, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        switch (id){
            case R.id.option_menu_done:
                addOrUpdateCard();
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    public void addOrUpdateCard(){
        String title = ed_card_title.getText().toString();
        if(title.equals(""))title = getResources().getString(R.string.untitled);
        if (checkEmptyField())showEmptyError();
        else {
            int id;
            Date dateCreated;
            if (card != null){
                id = card.getId();
                dateCreated = card.getDateCreated();
            }
            else  {
                id = (int) UUID.randomUUID().getMostSignificantBits();
                dateCreated = new Date();
            }

            String type = ed_card_type.getText().toString();
            if (type.equals(""))type = getString(R.string.none);

            Card card = new Card(id, title, ed_card_name.getText().toString()
                                    , ed_card_type.getText().toString(), ed_card_number.getText().toString()
                                    , validUtil, validFrom, dateCreated, ed_card_pin.getText().toString()
                                    , ed_card_cvv.getText().toString());

            Realm realm = Realm.getDefaultInstance();
            realm.executeTransaction(new Realm.Transaction() {
                @Override
                public void execute(Realm realm) {
                    realm.copyToRealmOrUpdate(card);
                }
            });

            realm.close();
            finishAfterTransition();
        }
    }

    public void showDialog() {
        dialog = new Dialog(AddOrUpdateCreditCardActivity.this);
        dialog.setTitle("Date");
        dialog.setContentView(R.layout.card_type_dialog);
        dialog.show();

        dialog.findViewById(R.id.item_card_american).setOnClickListener(this);
        dialog.findViewById(R.id.item_card_diners_club).setOnClickListener(this);
        dialog.findViewById(R.id.item_card_discover).setOnClickListener(this);
        dialog.findViewById(R.id.item_card_jcb).setOnClickListener(this);
        dialog.findViewById(R.id.item_card_maestro).setOnClickListener(this);
        dialog.findViewById(R.id.item_card_mastercard).setOnClickListener(this);
        dialog.findViewById(R.id.item_card_solo).setOnClickListener(this);
        dialog.findViewById(R.id.item_card_switch).setOnClickListener(this);
        dialog.findViewById(R.id.item_card_visa).setOnClickListener(this);
        dialog.findViewById(R.id.item_card_visa_electron).setOnClickListener(this);

    }
    public void setDatePicker(final int dateint){
        final Calendar c = Calendar.getInstance();
        mYear = c.get(Calendar.YEAR);
        mMonth = c.get(Calendar.MONTH);
        mDay = c.get(Calendar.DAY_OF_MONTH);
        DatePickerDialog datePickerDialog = new DatePickerDialog(AddOrUpdateCreditCardActivity.this,
                new DatePickerDialog.OnDateSetListener() {
                    @Override
                    public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
                         date = (monthOfYear + 1) + "/" + year;
                        if (dateint==0) {
                            validUtil = new Date();
                            validUtil.setMonth(monthOfYear);
                            validUtil.setYear(year-1900);
                            ed_card_expiry_date.setText(date);
                        }
                        else if(dateint==1) {
                            validFrom = new Date();
                            validFrom.setMonth(monthOfYear);
                            validFrom.setYear(year- 1900);
                            ed_card_valid_from.setText(date);
                        }
                    }
                }, mYear, mMonth, mDay);
        datePickerDialog.show();
    }
    public void initView(){
        expiry_date = (ImageView)findViewById(R.id.date_expiry_date);
        valid_from = (ImageView)findViewById(R.id.date_valid_from);
        ed_card_expiry_date = (EditText)findViewById(R.id.ed_card_expiry_date);
        ed_card_valid_from = (EditText)findViewById(R.id.ed_card_valid_from);
        type_more = (ImageView)findViewById(R.id.type_more);

        ed_card_type =(EditText)findViewById(R.id.ed_card_type);
        ed_card_title =(EditText)findViewById(R.id.ed_card_title);
        ed_card_name =(EditText)findViewById(R.id.ed_card_name);
        ed_card_number =(EditText)findViewById(R.id.ed_card_number);
        ed_card_cvv =(EditText)findViewById(R.id.ed_card_cvv);
        ed_card_pin =(EditText)findViewById(R.id.ed_card_pin);

        item_card_american =(LinearLayout)findViewById(R.id.item_card_american);
        item_card_diners_club =(LinearLayout)findViewById(R.id.item_card_diners_club);
        item_card_discover =(LinearLayout)findViewById(R.id.item_card_discover);
        item_card_jcb =(LinearLayout)findViewById(R.id.item_card_jcb);
        item_card_maestro =(LinearLayout)findViewById(R.id.item_card_maestro);
        item_card_mastercard =(LinearLayout)findViewById(R.id.item_card_mastercard);
        item_card_solo =(LinearLayout)findViewById(R.id.item_card_solo);
        item_card_switch =(LinearLayout)findViewById(R.id.item_card_switch);
        item_card_visa =(LinearLayout)findViewById(R.id.item_card_visa);
        item_card_visa_electron =(LinearLayout)findViewById(R.id.item_card_visa_electron);

        cardLogo = findViewById(R.id.card_logo);
    }

    @Override
    public void onClick(View v) {
        int id = v.getId();
        switch (id){
            case R.id.date_expiry_date:
                setDatePicker(0);
                break;
            case R.id.date_valid_from:
                setDatePicker(1);
                break;
            case R.id.type_more:
                showDialog();
                break;
            case R.id.item_card_american:
                ed_card_type.setText("American Express");
                setLogoCard("amex");
                dialog.cancel();
                break;
            case R.id.item_card_diners_club:
                ed_card_type.setText("Diners Club");
                setLogoCard("credit_card");
                dialog.cancel();
                break;
            case R.id.item_card_discover:
                ed_card_type.setText("Discover");
                setLogoCard("discover");
                dialog.cancel();
                break;
            case R.id.item_card_jcb:
                ed_card_type.setText("JCB");
                setLogoCard("credit_card");
                dialog.cancel();
                break;
            case R.id.item_card_maestro:
                ed_card_type.setText("Maestro");
                setLogoCard("credit_card");
                dialog.cancel();
                break;
            case R.id.item_card_mastercard:
                ed_card_type.setText("Mastercard");
                setLogoCard("mastercard");
                dialog.cancel();
                break;
            case R.id.item_card_solo:
                ed_card_type.setText("Solo");
                setLogoCard("credit_card");
                dialog.cancel();
                break;
            case R.id.item_card_switch:
                ed_card_type.setText("Switch");
                setLogoCard("credit_card");
                dialog.cancel();
                break;
            case R.id.item_card_visa:
                ed_card_type.setText("Visa");
                setLogoCard("visa");
                dialog.cancel();
                break;
            case R.id.item_card_visa_electron:
                ed_card_type.setText("Visa Electron");
                setLogoCard("credit_card");
                dialog.cancel();
                break;
        }
    }

    public boolean checkEmptyField(){
        boolean check = false;
        if(ed_card_type.getText().toString().equals("")
                &&ed_card_number.getText().toString().equals(""))check = true;
        return check;
    }

    public void showEmptyError(){
        new SweetAlertDialog(this, SweetAlertDialog.ERROR_TYPE)
                .setContentText(getString(R.string.add_card_empty_field_error))
                .setTitleText(getString(R.string.error_dialog_title))
                .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                    @Override
                    public void onClick(SweetAlertDialog sweetAlertDialog) {
                        sweetAlertDialog.dismiss();
                    }
                })
                .show();
    }

    public void setLogoCard(String name){
        Glide.with(this).load(getLogo(name)).into(cardLogo);
    }

    public int getLogo(String name){
        Resources resources = getResources();
        final int resourceId = resources.getIdentifier(name, "drawable",
                getPackageName());
        return resourceId;

    }
}
